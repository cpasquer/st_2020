README
------
This repository contains the source codes for the Seen2Seen and Seen2Unseen tools, which perform automatic identification of verbal multiword expressions. These two systems ran in the [PARSEME shared task on Semi-supervised Identification of Verbal Multiword Expressions](http://multiword.sourceforge.net/sharedtask2020/).

Seen2Seen reads all MWEs annotated as such in the training corpus, and extracts all occurrences of the same MWEs (whether identical or subject to morpho-syntactic variation) in the test corpus.

Seen2Unseen relies on Seen2Seen for the extraction of the MWE seen in the training corpus, and adds several heuristics for the extraction of unseen MWEs. 

Installation and configuration (under Linux)
------
 * Install python (e.g. following [this guide](https://linuxize.com/post/how-to-install-python-3-9-on-ubuntu-20-04/))
 * Install pip (for easy installation of Python packages (e.g. following [this guide](https://itsfoss.com/install-pip-ubuntu/))
 * Install python libraries
     * `pip install tqdm progressbar`
 * Clone the Seen2(Un)Seen repositiory (to do once):
     * Create an account on [Gitlab](https://gitlab.com/)
     * Clone the Gitlab repo to a local directory (choose a name and use it as <tt>your-local-repo</tt>)
         * `git clone https://gitlab.com/cpasquer/st_2020.git your-local-repo`
 * Configure Seen2(Un)Seen:
     * Modify the `config.cfg` file to choose the languages and the names of the vorpus files, e.g.:
         * langues = ["FR"]          # Set of langauges to run the system on   
         * calculPerfoTEST = False   # change to True F-measure, R and P are to be calculated on the gold TEST (from INPUT/FR/test.cupt)
         * corpusDEV = "dev.cupt"    # Name of the DEV file
         * corpusTEST = "test.blind.cupt"    # Name of the TEST file
         * corpusTRAIN = "train.cupt"        # Name of the TRAIN file

Running Seen2Seen
------
 * Run Seen2Seen first, with the configuration as above, to select the optimal set of filters (out of 8) for each language:
     * cd `CODE/`
     * `python3 seen2seen.py`
     * cd `\..`
 * Examine the result file:
     * `CODE/infos_configs.tsv` - shows which selection of the 8 filters corresponds to which configuration 
     * `OUTPUT_seen` - newly created directory with files generated for training:
         * `OUTPUT_seen/INFOS/` - information about the MWEs seen in TRAIN
         * `OUTPUT_seen/DEBUG/CANDIDATES/LANG` - information about MWE candidates extracted from DEV and TEST
         * `OUTPUT_seen/EVAL_allLanguages/LANG/DEV/dev.system.config_XXX.cupt` - results of annotating DEV of language LANG with configuration XXX of the 8 filters
         * `OUTPUT_seen/EVAL_allLanguages/LANG/2020_configs2Fscore_onDEV.tsv` - F-measures on DEV of langauge LANG for each configuration of filters
         * `OUTPUT_seen/EVAL_allLanguages/2020_best_configs_Fscore_onDEV.tsv' - best F-measures per language (with optimal selection of filters per language), and the identifier of the best configuration, e.g. for French: 
         * Example: For French, on the shared task 1.2 data we have: `FR 0.7889  48`, i.e. the best combination of filters is 48 ({f1, f2, f5, f6, f7, f8})
 * Re-run the same code as many times as needed, for each new TEST file.
     * Decomment the last line of `seen2seen.py` (call to the `eval_TEST()` function)
     * Put your test file into `INPUT/LANG/`
     * If you have the gold (annotated) version of this file, put it also in `INPUT/LANG/`
     * Set `corpusTEST` to the name of the test file (this does not seem to work; instead, replace test.blind.cupt with your new file to annotate)
     * Re-run: `cd CODE; python3 seen2seen.py`
     * Examine the annotation results in `RESULT_SYSTEM_SEEN/LANG/test.system.cupt` (or any other .cupt file given in `config.cfg` for `corpusTEST`)
     

Contact
-------
[Seen2Seen](https://gitlab.com/cpasquer/st_2020) was developed by Caroline Pasquer at the University of Tours, France. 
The current maintainer is [Agata Savary](http://www.info.univ-tours.fr/~savary).

Bibliography
-------
 * Caroline Pasquer, Agata Savary, Carlos Ramisch, Jean-Yves Antoine (2020) [Verbal Multiword Expression Identification: Do We Need a Sledgehammer to Crack a Nut?](https://www.aclweb.org/anthology/2020.coling-main.296/) In the Proceedings of the 28th International Conference on Computational Linguistics (COLING'21), Barcelona, Spain (online)
 * Caroline Pasquer, Agata Savary, Carlos Ramisch, Jean-Yves Antoine (2020) [Seen2Unseen at PARSEME Shared Task 2020: All Roads do not Lead to Unseen Verb-Noun VMWEs](https://www.aclweb.org/anthology/2020.mwe-1.16/), in the Proceedings of the Joint Workshop on Multiword Expressions and Electronic Lexicons (MWE-LEX), at COLING 2021, Barcelona, Spain (online). 
 * Carlos Ramisch, Silvio Ricardo Cordeiro, Agata Savary, Veronika Vincze, Verginica Barbu Mititelu, Archna Bhatia, Maja Buljan, Marie Candito, Polona Gantar, Voula Giouli, Tunga Güngör, Abdelati Hawwari, Uxoa Iñurrieta, Jolanta Kovalevskaitė, Simon Krek, Timm Lichte, Chaya Liebeskind, Johanna Monti, Carla Parra Escartín, Behrang QasemiZadeh, Renata Ramisch, Nathan Schneider, Ivelina Stoyanova, Ashwini Vaidya, Abigail Walsh (2020) [Edition 1.1 of the PARSEME Shared Task on Automatic Identification of Verbal Multiword Expressions](https://www.aclweb.org/anthology/W18-4925/), in the Proceedings of the Joint Workshop on Linguistic Annotation, Multiword Expressions and Constructions (LAW-MWE-CxG-2018), Santa Fe, USA.
 * Carlos Ramisch, Agata Savary, Bruno Guillaume, Jakub Waszczuk, Marie Candito, Ashwini Vaidya, Verginica Barbu Mititelu, Archna Bhatia, Uxoa Iñurrieta, Voula Giouli, Tunga Güngör, Menghan Jiang, Timm Lichte, Chaya Liebeskind, Johanna Monti, Renata Ramisch, Sara Stymne, Abigail Walsh, Hongzhi Xu (2020) [Edition 1.2 of the PARSEME Shared Task on Semi-supervised Identification of Verbal Multiword Expressions](https://www.aclweb.org/anthology/2020.mwe-1.14/), in the Proceedings of the Joint Workshop on Multiword Expressions and Electronic Lexicons (MWE-LEX), at COLING 2021, Barcelona, Spain (online).
