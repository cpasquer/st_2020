Statistics FR
=============
### dev.cupt
* Sentences: 1573
* Tokens: 39502
* Total VMWEs: 425
  * `IRV`: 117
  * `LVC.cause`: 5
  * `LVC.full`: 144
  * `MVC`: 2
  * `VID`: 157
