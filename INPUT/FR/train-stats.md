Statistics FR
=============
### train.cupt
* Sentences: 14377
* Tokens: 360070
* Total VMWEs: 3870
  * `IRV`: 1037
  * `LVC.cause`: 70
  * `LVC.full`: 1253
  * `MVC`: 16
  * `VID`: 1494
