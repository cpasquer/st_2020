Statistics FR
=============
### test.cupt
* Sentences: 5011
* Tokens: 126420
* Total VMWEs: 1359
  * `IRV`: 347
  * `LVC.cause`: 22
  * `LVC.full`: 481
  * `MVC`: 4
  * `VID`: 505
