#!/usr/bin/python3
# -*- coding: utf-8 -*-


#==========================================
#             IMPORTS
#==========================================
import os
import sys
import time
import progressbar
import subprocess
import numpy as np
from collections import OrderedDict
from collections import Counter 
from tqdm import tqdm
from shutil import copyfile
import configparser
import ast
from threading import Thread
import math 
import shutil
import pickle
#-----------------------------
from util_utilitaires import *
from util_findPOScolumn import *
from util_cupt2Blind import *
from util_cupt2typo_POS_dep_morpho import * 
from util_distSyn import * 


             
#==========================================
#      FONCTIONS 
#==========================================
def listeStr_2int(l_str):
    l_out = []
    for elt in l_str:
        l_out.append(int(elt))
    return l_out

def loadpickle(infile):
    with open(infile, 'rb') as f :
        dico = pickle.load(f)   
    return dico

def savepickle(dico,outfile):
    with open(outfile, 'wb') as f :
        pickle.dump(dico, f,protocol=pickle.HIGHEST_PROTOCOL)  


def nb_LVC_VID_a_trouver(corpus, langue):
    if "test" in str(corpus):
        corpusGold = "test-stats.md"
    elif "dev" in str(corpus) :
        corpusGold = "dev-stats.md"

    #------------ Nb total (seen + unseen) à annoter comme LVC et VID ----------------
    chemin = os.path.join(repParent,"INPUT",langue,corpusGold)
    comptgold = 0
    with open(chemin,"r") as f_in :
        lignes = f_in.readlines()
        for l in lignes :
            if "LVC" in str(l) or "VID" in str(l) :
                comptgold += int(l.split(": ")[1].split("\n")[0])
    #print(comptgold)
    #------------ Nb déjà annoté comme LVC et VID seen -------
    config = configparser.ConfigParser()
    config.read(str(repParent)+'/config.cfg')
    nb_deja_annotes =  int(config.get('Parameters', str(langue) + "_nb_seen_annotes_par_sys"))

    return comptgold, nb_deja_annotes

def extractInfosDiscont(col6_positionEltsMwe, lignes,corpus) : 
       catMWEs = {}
       for MWE in col6_positionEltsMwe :           #{'1': '2|3', '2': '10|12'}   
              catMWE = ""
              idMWE = "MWE" + str(MWE)
              positionEltsMwe = col6_positionEltsMwe[MWE].split("|")  #['1', '2', '3', '4']
              positionDebut = positionEltsMwe[0]
              positionFin = positionEltsMwe[len(positionEltsMwe)-1]
              for ligne in lignes :                           
                     infos = ligne.split("\t")                       
                     if len(infos) > 10 and "-" not in infos[0]:  
                            for elt in range(int(positionDebut),int(positionFin) + 1) : 
                                   elt_lex = False 
                                   for num_eltLex in positionEltsMwe :	#['1', '2', '3', '4']
                                          if int(elt) == int(num_eltLex) :	
                                                 elt_lex = True     
                                   if int(elt) == int(infos[0]) : 
                                          if elt_lex :                                                              
                                                 #---- catégorie MWE ----  
                                                 if ";" not in infos[10] and ":" in infos[10] :
                                                        #e.g.  1:VID
                                                        idMWE2 = "MWE" + str(infos[10].split(":")[0])
                                                        if idMWE2 not in catMWEs :
                                                               catMWEs[idMWE2] = infos[10].split(":")[1]                                                               
                                                 elif  ";" in infos[10] :                                             
                                                        #e.g.  1:VID;2:LVC.full          1;2:LVC.full
                                                        alltypes = infos[10].split(";")
                                                        for typeMWE in alltypes :
                                                               if ":" in typeMWE :                                                
                                                                      idMWE2 = "MWE" + str(typeMWE.split(":")[0])
                                                                      catMWEs[idMWE2] = typeMWE.split(":")[1]   
              if idMWE in catMWEs :
                     catMWE = catMWEs[idMWE]        
              else :
                     
                     # catégorie non trouvée car c'est un signe de ponctuation qui la contient
                     num_mwe = idMWE.split("MWE")[1]
                     for ligne in lignes :                           
                            infos = ligne.split("\t")                       
                            if len(infos) > 10 :
                                   if ";" not in infos[10] and ":" in infos[10] :
                                          idMWE_encours = infos[10].split(":")[0]
                                          if num_mwe ==  idMWE_encours :
                                                 catMWEs[idMWE] = infos[10].split(":")[1]                                                               
                                   elif  ";" in infos[10] :                                             
                                          #e.g.  1:VID;2:LVC.full          1;2:LVC.full
                                          alltypes = infos[10].split(";")
                                          for typeMWE in alltypes :
                                                 if ":" in typeMWE :                                                
                                                        idMWE_encours = typeMWE.split(":")[0]
                                                        if num_mwe ==  idMWE_encours :
                                                               catMWEs[idMWE] = typeMWE.split(":")[1]   
                     catMWE = catMWEs[idMWE]        
       return catMWEs

def cupt2tableur(pathsCorpus,dico_NF_train,corpus,langue): 
       localisation_mwe_dev = [] 
       liste_NF_annot = []
       with open( str(repParent) + "/CODE/DEBUG/" + str(langue) + "_" + str(corpus) + "_annot.tsv","a") as f_out :
              f_out.write("numPhrase\tidMWE\tNF\tPOSnorm\tPOSseq\tNb elts lex\tcatMWE\tcorrespondace_lemme_pos\tPhrase\tNF Vue dans Train/Dev ?\n")
                                 
       #===================================================================
       # extract infos in cupt for annotated MWEs 
       #===================================================================       
       with open (pathsCorpus,"r") as f_cupt :
              infosCupt =  f_cupt.readlines()	
              corpusCupt = "".join(infosCupt[1:])					
              phrases = corpusCupt.split("\n\n")[:-1]
              comptPhrase = 1
              for phrase in phrases :     
                     mots = []  
                     col6_positionEltsMwe = {}		
                     col8_EltLexLemme = {}
                     col9_EltLexPOS = {}
                     col10_EltLexReldep = {}
                     lignes = phrase.split("\n")
                     for ligne in lignes :
                            if ligne[0] != "#" :              
                                   infos = ligne.split("\t")      
                                   # ce token fait-il partie de une ou plusieurs MWE => extarction du numéro de MWE + catégorie
                                   idMWEs = []   
                                   typeMWEs = []
                                   
                     
                                   if infos[10] not in ["*","_"]  :
                                          if "," not in infos[1] :
                                                 if "-" not in str(infos[0]):
                                                     mots.append("["+str(infos[1])+"]")
                                                 eltLex = True
                                                 #  Overlap => 1:VID;2:IAV /1;2  vs pas d'overlap =>  1:VID /1         
                                                 if ";" not in infos[10] and ":"  not in infos[10] :                                    
                                                        idMWEs.append(infos[10])
                                                 elif ";" not in infos[10] :
                                                        idMWEs.append(infos[10].split(":")[0])
                                                        typeMWEs.append(infos[10].split(":")[1])
                                                 elif ":" not in infos[10] :
                                                        idMWEs = infos[10].split(";")
                                                 else :                                    
                                                        temp_idMWEs = infos[10].split(";")
                                                        for temp in temp_idMWEs :
                                                               idMWEs.append(temp.split(":")[0])
                                                               typeMWEs.append(infos[10].split(":")[1])
                                          else :
                                                 eltLex = False 
                                                 mots.append(str(infos[1]))
                                   else:
                                          eltLex = False  
                                          mots.append(str(infos[1]))    
                                   # caractéristiques de l'Elt Lex   
                                   if eltLex and infos[3] != "PUNCT" :  
                                          for idMWE in idMWEs :  
                                                 newId = "MWE" + str(idMWE)
                                                 if idMWE not in col6_positionEltsMwe :
                                                        col6_positionEltsMwe[idMWE] = infos[0]
                                                        col8_EltLexLemme[newId] = infos[2].lower()
                                                        if infos[2].lower() == "er|es|sie" :
                                                            col8_EltLexLemme[newId] = "er-es-sie"
                                                        col9_EltLexPOS[newId] = infos[3]  
                                                        col10_EltLexReldep[newId] = infos[7]  
                                                 else :
                                                        col6_positionEltsMwe[idMWE] = col6_positionEltsMwe[idMWE] + "|" + infos[0] 
                                                        if infos[2].lower() == "er|es|sie" :
                                                            col8_EltLexLemme[newId] =  col8_EltLexLemme[newId] + "|er-es-sie"
                                                        else :
                                                            col8_EltLexLemme[newId] =  col8_EltLexLemme[newId] + "|" + str(infos[2].lower())
                                                        col9_EltLexPOS[newId] = col9_EltLexPOS[newId] + "|" + infos[3] 
                                                        col10_EltLexReldep[newId] = col10_EltLexReldep[newId] + "|" + infos[7] 

                     # ========================================================
                     #	OUTPUT (dans Fichier TSV si option debug)
                     # ========================================================

                     col4_catMWE = extractInfosDiscont(col6_positionEltsMwe, lignes,corpus)
                     col4_catMWE = sorted(col4_catMWE.items(), key=lambda t: t[0])    
                     col6_positionEltsMwe = sorted(col6_positionEltsMwe.items(), key=lambda t: t[0])     #{'1': '2|3', '2': '10|12'}
                     col8_EltLexLemme = sorted(col8_EltLexLemme.items(), key=lambda t: t[0])             #{'1': 'call|in', '2': 'pick|up'}
                     col9_EltLexPOS = sorted(col9_EltLexPOS.items(), key=lambda t: t[0])                 #{'1': 'VERB|ADP', '2': 'VERB|ADP'}                             
                     col10_EltLexReldep = sorted(col10_EltLexReldep.items(), key=lambda t: t[0])     
                     for MWE in range(0,len(col4_catMWE)): 
                            idMWE = col4_catMWE[MWE][0] 
                            catMWE = col4_catMWE[MWE][1]                     #VID          
                            lemmesEltLex = col8_EltLexLemme[MWE][1]          #a|little|birdie|tell  
                            lemmesEltLex_liste = lemmesEltLex.split("|") 
                            NF = ";".join(sorted(lemmesEltLex_liste))
                            POSseq = col9_EltLexPOS[MWE][1]                  #DET|ADJ|NOUN|VERB                               
                            POSnorm = ";".join(sorted(POSseq.split("|"))) 
                            relDep = col10_EltLexReldep[MWE][1]
                            if NF not in liste_NF_annot:
                                liste_NF_annot.append(NF)
                            
       
                            correspondace_lemme_pos = []
                            for elt in range(0,len(lemmesEltLex_liste)) :
                                   correspondace_lemme_pos += [str(POSseq[elt]) + "=" +str(lemmesEltLex_liste[elt]) ]
                            correspondace_lemme_pos = "||".join(correspondace_lemme_pos)
                            tok_annot = sorted(listeStr_2int(col6_positionEltsMwe[MWE][1].split("|")))    
                            #============================================================
                            # ecriture infos fichier cupt annoté & précise si NF seen in Train
                            #============================================================ 
                            train_seen = []
                            dev_seen = []

                            if os.path.exists(str(repParent) + "/OUTPUT_seen/INFOS/" + str(langue) + "/dev_gold_NFseen.pickle"):
                                dev_seen = loadpickle(str(repParent) + "/OUTPUT_seen/INFOS/" + str(langue) + "/dev_gold_NFseen.pickle")
                            if os.path.exists(str(repParent) + "/OUTPUT_seen/INFOS/" + str(langue) + "/train_NFseen.pickle"):
                                train_seen = loadpickle(str(repParent) + "/OUTPUT_seen/INFOS/" + str(langue) + "/train_NFseen.pickle")

                            deja_vue = ""
                            if NF in train_seen :
                                deja_vue = "seenTrain"
                                if NF in dev_seen :
                                   deja_vue += "-seenDev"
                            else :
                                if NF in dev_seen :
                                   deja_vue += "seenDev"

                            if deja_vue == "" :
                                deja_vue = "unseen"

                            with open( str(repParent) + "/CODE/DEBUG/" + str(langue) + "_" + str(corpus) + "_annot.tsv","a") as f_out :
                                 f_out.write(str(comptPhrase) + "\t" + str(idMWE) + "\t" + str(NF) + "\t" + str(POSnorm) + "\t" + str(POSseq) + "\t" + str(len(tok_annot)) + "\t" + str(catMWE)+ "\t" + str(correspondace_lemme_pos) + "\t"+ str(" ".join(mots))+"\t"+str(deja_vue) + "\n")
                            if corpus == "dev_gold" :
                                localisation_mwe_dev.append(str(comptPhrase) + ";" + "|".join(listeInt2Str(tok_annot)))

                                          
                     comptPhrase += 1

       


#================================ Paramètres ================================
langues = ["DE","EU","EL","FR","GA","HE","HI","IT","PL","PT","RO","SV","TR","ZH"]
#================================================================
repParent = "/".join(os.path.abspath(os.path.dirname(sys.argv[0])).split("/")[:-1]) 

for langue in langues :    
    print(langue)
    for corpus_annote in ["train","dev_gold","test_syst_seen2seen","test_syst_seen2unseen","dev_syst_seen2seen"] :
        if corpus_annote == "dev_syst_seen2seen" :
            rep = "/OUTPUT_seen/EVAL_allLanguages/" 
            filenames = os.listdir(str(repParent) + str(rep) + str(langue))
            for f in filenames : 
                print(f)
                if "test.system_" in str(f) :    
                    best_config = f.split(".cupt")[0]
                    print(best_config)
                    best_config = best_config.split("system_")[1]
                    nom_fdev = "dev.system.varide3.extractPlus_ok_config_config_" + str(best_config) + ".cupt"
                    pathsCorpus = str(repParent) + str(rep) + str(langue) + "/DEV/" + str(nom_fdev)
                    print(pathsCorpus)

        elif corpus_annote == "dev_gold" :
            rep = "/INPUT/"
            pathsCorpus = str(repParent) + str(rep) + str(langue) + "/dev.cupt" 
        elif corpus_annote == "test_syst_seen2seen" :
            rep = "/RESULT_SYSTEM_SEEN/"
            pathsCorpus = str(repParent) + str(rep) + str(langue) + "/test.system.cupt" 
        elif corpus_annote == "test_syst_seen2unseen" :
            rep = "/RESULT_SYSTEM_UNSEEN/"
            pathsCorpus = str(repParent) + str(rep) + str(langue) + "/test.system.cupt"            
            
        elif corpus_annote == "train" :
            rep = "/INPUT/"
            pathsCorpus = str(repParent) + str(rep) + str(langue) + "/train.cupt" 



        dico_NF_train = loadpickle(str(repParent) + "/OUTPUT_seen/INFOS/" + str(langue) + "/dicoFreqNF_inTrain.pickle")    
        cupt2tableur(pathsCorpus,dico_NF_train,corpus_annote,langue)
       
print("The end")
