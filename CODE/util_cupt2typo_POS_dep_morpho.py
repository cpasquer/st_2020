#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import os
from os.path import dirname	# remonter arborescence fichiers
import os.path
from util_findPOScolumn import *

#repParent = "/".join(os.path.abspath(os.path.dirname(sys.argv[0])).split("/")[:-1])

def cupt2POSdepMorpho(langue, pathsCorpus,colonnePOS) :
  """
  INPUT : language
  OUTPUT : for a given language, 3 text files called morpho.txt, POS.txt, relationDep.txt 
  """  
  dicoMorpho = {}
  listeMorpho = []	# pour éviter doublons
  listePOS = []
  listeRelDep = []
  pathsCorpusUtiles = []
  if pathsCorpus[1] == "noDEV" :
    pathsCorpusUtiles = [pathsCorpus[0]]
  else :
    pathsCorpusUtiles = pathsCorpus[:-1]
  for corpus in pathsCorpusUtiles :
    with open (corpus, "r") as f_corpus : 
      lignesCorpus = "".join(f_corpus.readlines()[1:])					
      phrases = lignesCorpus.split("\n\n")[:-1]
      compteurPhrase = 1
      for phrase in phrases :
        lignes = phrase.split("\n")
        for ligne in lignes :
          if "# source_sent_id" in ligne :
            sourcePhrase = ligne.split("# source_sent_id = ")[1]
            #idPhrase = sourcePhrase.split(" ")[2]     #e.g. email-enronsent34_01-0024   
            idPhrase = compteurPhrase           
          elif "# text = " in ligne :
            textPhrase =  ligne.split("# text = ")[1] #e.g. "A little birdie told me..."                
          elif ligne[0] != "#" :              
            infos = ligne.split("\t")		
            if len(infos) != 11 :
              print("WARNING: missing column in Sentence=" + str(idPhrase))
            else :                  
              POS = infos[colonnePOS]                
              morphos = infos[5].split("|")	#e.g. PronType=Art, PronType=Dem
              dep = infos[7]             
              if POS not in listePOS :
                listePOS.append(POS)                      
              for morpho in morphos :                        
                if "=" in morpho :                                         
                  morphoType = morpho.split("=")[0]
                  morphoValeur = morpho.split("=")[1]
                  if morphoValeur not in listeMorpho : 
                    listeMorpho.append(morphoValeur)                      
                  if  morphoType not in dicoMorpho :
                    dicoMorpho[morphoType] = morphoValeur
                  else :
                    old = dicoMorpho[morphoType].split("|")
                    if morphoValeur not in old : 
                      dicoMorpho[morphoType] = dicoMorpho[morphoType] + "|" + morphoValeur
                else:  
                  # morpho = ["_"] ou comme en turc ["A3sg","Loc","P3sg"]                         
                  if morpho == ['_'] or morpho == "_":                       
                    dicoMorpho["_"] = morpho                                             
                  else :
                    if morpho not in listeMorpho : 
                      listeMorpho.append(morpho)                        
                      if "unknown" not in dicoMorpho :
                        dicoMorpho["unknown"] = morpho
                      else :
                        dicoMorpho["unknown"] = dicoMorpho["unknown"] + "|" + morpho                                                                                                                                        
              if dep not in listeRelDep:
                listeRelDep.append(dep) 

        compteurPhrase += 1               
    #====== synthese sur corpus Train (+ Dev) ====
    listePOS = sorted(listePOS)
    listeRelDep = sorted(listeRelDep)
  return dicoMorpho,listePOS,listeRelDep
