#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys
import configparser
import ast
from pathlib import Path
import shutil

"""
Supprime la mention de la 11e colonne (annotation MWE)  du fichier CUPT et la remplace par _
"""
#-------------------- INPUT --------------------------
#Parameters in configFile
repParent = "/".join(os.path.abspath(os.path.dirname(sys.argv[0])).split("/")[:-1])
config = configparser.ConfigParser()
config.read(str(repParent)+'/config.cfg')
languesAtraiter = ast.literal_eval(config.get('Parameters', 'langues'))
corpusDEV = config.get('Parameters', 'corpusDEV')
#-----------------------------------------------------      
repParent = repParent
if os.path.exists(str(repParent) + "/INPUT_blind/") :
      shutil.rmtree(str(repParent)  + "/INPUT_blind")      
for langue in languesAtraiter : 
      os.makedirs(str(repParent) + "/INPUT_blind/" + str(langue) + "/")      
      #-------------------------  blind DEV if DEV exists -------------------------
      fichierIN_Dev = str(repParent) + "/INPUT/" + str(langue) + "/" + str(corpusDEV)
      fichierIN_Dev = fichierIN_Dev.replace('"','')
      if os.path.exists(fichierIN_Dev) :
            fichierOUT_Dev = str(repParent) + "/INPUT_blind/" + str(langue) + "/" + "dev.blind.cupt"
            with open(fichierIN_Dev,"r") as f_cupt:
                  with open(fichierOUT_Dev,"a") as f_blind:
                        lignes = f_cupt.readlines()		
                        for ligne in lignes :				
                              infos = ligne.split("\t") 
                              if "#" in str(infos[0]) or infos == ['\n'] :
                                    f_blind.write(ligne)
                              else :	
                                    newligne = ""
                                    for i in range(0,10) :			
                                          newligne += str(infos[i]) + "\t"
                                    newligne += "_\n"		
                                    f_blind.write(newligne)
                                    
            

