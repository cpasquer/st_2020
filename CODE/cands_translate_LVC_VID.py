#!/usr/bin/python3
# -*- coding: utf-8 -*-

#==========================================
#             IMPORTS
#==========================================
import os
import sys
import pickle
from tqdm import tqdm 							#pip install tqdm
from googletrans import Translator 	#pip install googletrans https://pypi.org/project/googletrans/
import codecs

#==========================================
#      			FONCTIONS 
#==========================================
def loadpickle(infile):
    with open(infile, 'rb') as f :
        iterable = pickle.load(f)   
    return iterable
#===========================================
def do_traduction(source_language,target_language,liste_cands,fichierOUT) :
    """
    Traduit via l'API Google uniquement les LVC et VID.
    Ne tient pas compte des traductions monomots (rares en FR)
    """
    translator = Translator() 
    fichier_avancement = os.path.join(repParent,"OUTPUT_unseen",rep_trad,source_language,"avancement_mwe.tsv")  
    trad_complet = False
    numligneDebut = 0    
    compt_EP_atraduire = 0
    if os.path.exists(fichier_avancement) :
        # ie si déjà commencé à traduire
        with open(fichier_avancement,"r") as f_in :
            lignes = f_in.readlines()
            for l in lignes : 
               infos = l.split("\t")
               if len(infos) > 1:
                   avancement = infos[1].split("\n")[0]
                   if avancement == "DONE" :
                       trad_complet = True   
        if not trad_complet and len(lignes) != 0: # fichier non vide
            avant_derniere_ligne = lignes[len(lignes)-2].split("\t")
            derniere_ligne = lignes[len(lignes)-1].split("\t")
            if len(derniere_ligne) > 1 :
                numligneDebut = int(derniere_ligne[1].split("\n")[0])        
            else :
                numligneDebut = int(avant_derniere_ligne[1].split("\n")[0]) 
        elif trad_complet  :
            print(str(source_language) + ": translation=100%")
    if not trad_complet:     
        with codecs.open(fichierOUT,"a","utf-8") as f_out, codecs.open(fichier_avancement,"a","utf-8") as f_etat :  
            compt_l = numligneDebut 
            message = str(source_language) + ": translation"                  
            pbar = tqdm(liste_cands[numligneDebut:],desc=message)      
            f_out.write("langue source\tcat\tmwe à traduire\tmwe traduite\tNbmots traduction\n")
            for cand in pbar :
                nf = ";".join(sorted(cand.split(" ")))
                if source_language == "ZH" :
                    source_language = "ZH-CN" #chinois de Chine et pas de Taïwan
                compt_EP_atraduire += 1
                cand_traduit = translator.translate(cand, src=source_language, dest = target_language).text 
                nb_mots = len(cand_traduit.split(" "))
                mots_separes = "|".join(cand_traduit.split(" "))
                if nb_mots > 1 : 
                    if nb_mots == 3 :
                        if cand_traduit.split(" ")[1] in ["le","la","l'","de","un","une","les","des"] :
                            nb_mots = 2
                            f_out.write(str(source_language) +  "\t" + str(nf) + "\t" + str(cand) + "\t" + str(cand_traduit.split(" ")[0].lower()) + "|" + str(cand_traduit.split(" ")[2].lower()) + "\t" + str(nb_mots) +"\n")    
                        else :
                            f_out.write(str(source_language) + "\t" + str(nf) + "\t"  + str(cand) + "\t"+ str(mots_separes.lower())  + "\t" + str(nb_mots) + "\n") 
                    else :
                        f_out.write(str(source_language) + "\t" + str(nf) + "\t"  + str(cand) + "\t"+ str(mots_separes.lower())  + "\t" + str(nb_mots) + "\n")                
                if compt_l == len(liste_cands) - 1: 
                    compt_l = "DONE"
                else :
                    compt_l += 1
                f_etat.write("cand\t"+ str(compt_l) + "\n")

def tri_dico(dico, topX):
    """
    input = dictionnaire clé-valeur 
    output = liste des topX clés par valeur décroissante
    """
    dicoTrie = {} 
    listeTrie = []
    dicoTrie =  {key: value for key, value in dico.items() if value in sorted(set(dico.values()), reverse=True)[:topX]}
    while len(listeTrie) <= topX :
        for k in dicoTrie :        
            listeTrie.append(k)
    return listeTrie

def obtention_EP_cands(langue,nb_cand):    
    filenames = os.listdir(os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",langue))
    for f in filenames :
        if "test_extraction_V-N_IRVoption" in str(f) and "lock" not in str(f):
            dico_cand = {}
            chemin_f_cand = os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",langue,f)  
            with codecs.open(chemin_f_cand,"r","utf-8") as f :
                lignes = f.readlines()
                for l in lignes[1:] :
                    infos = l.split("\t")
                    if len(infos) > 0 :
                        statut = infos[0]
                        action = infos[5]
                        if "unseen" in str(statut)  and "faut_ranker" in str(action) and "IRV" not in str(infos[7]) :
                            cand = infos[4]
                            proba_dep_dist = float(infos[10])
                            if cand not in dico_cand:
                                dico_cand[cand] = float(proba_dep_dist)
                            else :
                                if proba_dep_dist > float(dico_cand[cand]) :
                                    dico_cand[cand] = float(proba_dep_dist)  
            return tri_dico(dico_cand,nb_cand)
 
#========================================================================
#                             MAIN
#========================================================================
repParent = "/".join(os.path.abspath(os.path.dirname(sys.argv[0])).split("/")[:-1])
target_language = "FR"
nb_cand = 800 # 800 EP cands à traduire maximum
langues = ["IT"] 
						
#----------------------------------------------------------------   
rep_trad = os.path.join(repParent,"OUTPUT_unseen","CANDIDATES")
for langue in langues : 
    cands_a_traduire = obtention_EP_cands(langue,nb_cand)
    f_trad =os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",langue,str(langue) + "_cands_traduits.tsv") 
    do_traduction(langue,target_language,cands_a_traduire,f_trad)
print("The end")   
    


