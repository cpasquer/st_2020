#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re
from itertools import product
import os
import sys
import ast
import itertools
from itertools import permutations
import time
from shutil import copyfile
from util_cupt2typo_POS_dep_morpho import * 
from util_distSyn import * 
from tqdm import tqdm 
from util_utilitaires import *

# infos corpus
def listeTokens2dico(liste):
  """
  INPUT : ['DET', 'NOUN', 'ADP', 'NOUN', 'AUX', 'ADJ', 'PUNCT', 'PRON', 'PRON', 'VERB', '_', 'ADP', 'DET', 'NOUN', 'CCONJ', 'ADP', 'DET', 'NUM', 'NOUN', 'PUNCT']
  OUTPUT2 : {0: DET , 1:NOUN, ....} en excluant les underscores (qui correspondent aux amalgames)
  """
  dicoOUT = {}
  compt = 1
  for tok in liste :
    if tok != "_" :
      dicoOUT[compt] = tok
      compt += 1
  return dicoOUT

def infos_dependances(dicoPhrases, numPhrase) :
    """
    OUTPUT2 = connexions_dependance = {'1': '6', '2': '4', '3': '4', '4': '6', '5': '6', '6': '21', '7': '9', '8': '9', '9': '6', '10': '11', '11': '9', '12': '11', '13': '15', '14': '15', '15': '11', '16': '15', '17': '6', '18': '20', '19': '20', '20': '21', '21': '0', '22': '23', '23': '21', '24': '26', '25': '26', '26': '21', '27': '28', '28': '21', '29': '28', '30': '28', '31': '21'}
    """
    connexions_dependance = {}
    lignes = dicoPhrases[numPhrase].split("\n")
    for l in lignes :
      infos = l.split("\t")
      if "-" not in str(infos[0]) : 
          connexions_dependance[infos[0]] = infos[6]
    return connexions_dependance

def phrase2lemmeAndPOSphrase(phrase, langue) :
  """
  OUTPUT2 = 
   - liste des lemmes de la phrase (minuscules) avec correction des IRV mal lemmatisés si spécifié pour une langue donnée
   - liste des POS de la phrase
   - liste des tokens de la phrase
  """
  lemmesPhrase = []
  numToksPhrase = []
  POSPhrase = []
  for ligne in phrase :
    infos = ligne.split("\t")
    if len(infos) > 2 :
      if "-" not in infos[0] : # gestion des amalgames
        numToksPhrase.append(infos[0])
        if "|" in infos[2] and langue == "DE":
            new_infos = infos[2].replace("|","-")
        else: 
            if "_" in str(infos[2]) :
              new_infos = infos[1]
            else : 
              new_infos = infos[2]
        if langue != "SL" :
          if infos[3] != "_" :
            POSPhrase.append(infos[3])
          else :
            POSPhrase.append("X")
        else :
          POSPhrase.append(infos[4])
        if correctionLemmeIRV(langue)[0] == [] : 
          lemmesPhrase.append(new_infos.lower())  
        else :
          if new_infos in correctionLemmeIRV(langue)[0] and infos[9] != "*":
            # ie si expression annotee et pronom mal lemmatisé :
            lemmesPhrase.append(correctionLemmeIRV(langue)[1]) 
          else :
            lemmesPhrase.append(new_infos.lower())    
  return lemmesPhrase,numToksPhrase, POSPhrase
     
def verif_languecas(dico_NF2_case_morphoNoun):
  """
  considère si une langue est à cas si plus de 50% NF annotées comportent une info de cas sur le nom
  """
  comptNF = 0
  comptNF_casVide = 0 
  for NF in dico_NF2_case_morphoNoun  :
    if dico_NF2_case_morphoNoun[NF]["caseNoun"] == [''] :
      comptNF_casVide += 1
    comptNF += 1
  if comptNF_casVide > comptNF /2 :
    return False
  else :
    return True

def correctionLemmeIRV(langue):
  """
  detail des corrections d'IRV à apporter
  """
  if langue == "FR" :
    return (['me','te','nous','vous'],'se')
  else :
    return ([],'')

def NEW_tokens2_POSseq_POSnorm(dicoCand,dicoPOS,langue):
  """
  INPUT :  dictionnaire de candidats ie toutes les combinaisons possibles de lemmes 
           e.g. {7206 : {"de;la;partie;être" : ['1|10|11|14'...,}
  OUTPUT2 : dictionnaire avec séquences de POS par candidat
           e.g. {7206 : {"de;la;partie;être" :[['ADP', 'DET', 'NOUN', 'VERB'], ['ADP', 'DET', 'NOUN', 'VERB'], ['ADP', 'DET', 'NOUN', 'VERB']] ..}
  """   
  dicoPOSseq = {}
  dicoPOSnorm = {}
  POSseqs = []
  for numPhrase in dicoCand :
    dicoTok2POS = listeTokens2dico(dicoPOS[numPhrase]) 
    for NF in dicoCand[numPhrase] : 
      dicoPOSseq = updateDico2keys_liste(dicoPOSseq,numPhrase,NF) 
      dicoPOSnorm = updateDico2keys_liste(dicoPOSnorm,numPhrase,NF)  
      for combi in dicoCand[numPhrase][NF] :
        tokens = combi.split("|")
        POS = []
        for tok in tokens :    
          if int(tok) not in dicoTok2POS :
            POS += ["X"]
          else :
            POS += [dicoTok2POS[int(tok)]]
        POSnorm = ";".join(sorted(POS))
        POSseq_str = "|".join(POS)
        dicoPOSseq[numPhrase][NF] += [POSseq_str]
        dicoPOSnorm[numPhrase][NF] += [POSnorm]
  return dicoPOSseq, dicoPOSnorm

def POSseq2POSnorm(POSseq) :
  return ";".join(sorted(POSseq.split("|")))

def NEW_calcul_distLin(str_cand) :
  composants = listeStr2int(str_cand.split("|"))
  nb_composants = len(composants)
  return (max(composants) - min(composants)) - (nb_composants - 1 )

# search candidates

def search_LemmaSeq(langue,corpus,NFs,identifiant_corpus) :
  """
  Fonction qui recherche dans toutes les phrases du corpus 
  les co-occurrences de lemmes identiques à celles déjà vues (liste NF) 
  Input : 
       - langue (pour gérer le cas de correction de certaines POS, eg "vous souvenir" au lieu de "se souvenir")
       - liste des corpus pour l'extraction de candidats
       - liste des NF à rechercher
  OUTPUT2 : dictionnaire de candidats sous la forme suivante :
       - clé = numéro de phrase ; valeurs = localisation des lemmes 
       " 42: {'dificuldade;ter': {'dificuldade': [12], 'ter': [10, 21]}}, "   
  """
  dicoCand = {}
  dicoPOS = {}
  dicoPhrases,dicoPhrasesAvecEntete = cupt2phrases(corpus)
  comptPhrase = 1    
  message = str(langue) + "-Search candidates"
  pbar = tqdm(dicoPhrases,desc=message)      
  for phrase in pbar:  
    listelemmePhrase,numToksPhrase, POSPhrase = phrase2lemmeAndPOSphrase(dicoPhrases[phrase], langue) 
    dicoCand[comptPhrase] = {}             
    dicoCand[comptPhrase] = searchLemmaSeqInPhrase(dicoCand,comptPhrase,listelemmePhrase,numToksPhrase,POSPhrase, NFs, langue)
    dicoPOS[comptPhrase] = POSPhrase
    comptPhrase += 1         
  return dicoCand, dicoPOS
    
def searchLemmaSeqInPhrase(dicoCand,comptPhrase,listelemmePhrase,numToksPhrase,POSPhrase, NFs, langue) :  
  """
  Fonction qui recherche dans CHAQUE phrase du corpus 
  les co-occurrences de lemmes identiques à celles déjà vues (liste NF) 
  Input : 
       - langue (pour gérer le cas de correction de certaines POS, eg "vous souvenir" au lieu de "se souvenir")
       - liste des corpus pour l'extraction de candidats
       - liste des NF à rechercher
  OUTPUT2 : dictionnaire de candidats sous la forme suivante :
       - clé = numéro de phrase ; valeurs = localisation des lemmes 
       " 42: {'dificuldade;ter': {'dificuldade': [12], 'ter': [10, 21]}}, "   

  """  
  if langue == "FR":
    if "faire;il" in NFs :
      NFs.remove("faire;il") # cf Dev surreprésentation de cette NF qd analyse erreur
  if langue == "RO":
        if "putea;sine" in NFs :
            NFs.remove("putea;sine") # idem surreprésentation p/p train
  if langue == "GA":
        if "ar;bí" in NFs :
            NFs.remove("ar;bí") # idem surreprésentation p/p train
        if "bí;ó" in NFs :
            NFs.remove("bí;ó") # idem surreprésentation p/p train

  for NF in NFs :         
    lemmesAtrouver = NF.split(";")        
    for lemmeRef in lemmesAtrouver :
      token = 1    
      for lemmeCand in listelemmePhrase :    
        if lemmeCand == lemmeRef :    
          if NF not in dicoCand[comptPhrase] :    
            dicoCand[comptPhrase][NF] = {} 
            dicoCand[comptPhrase][NF][lemmeRef] = [token]  
          else :
            if lemmeRef not in dicoCand[comptPhrase][NF] :           
              dicoCand[comptPhrase][NF][lemmeRef] = []            
              dicoCand[comptPhrase][NF][lemmeRef] = [token]                  
            else :
              dicoCand[comptPhrase][NF][lemmeRef] += [token]
        token += 1  
    if comptPhrase in dicoCand and NF in dicoCand[comptPhrase] :
      if len(dicoCand[comptPhrase][NF]) != len(NF.split(";") ) :
        removeKey(dicoCand[comptPhrase],NF)
  return dicoCand[comptPhrase]

# filtres  

def cand2FiltrePosnorm(dicoCand):
  new_dicoCand = {}
  tokens = []  
  for comptPhrase in dicoCand :
    if len(dicoCand[comptPhrase]) != 0 :
      for NF in dicoCand[comptPhrase] : #{'de;le;partie;être': {'de': [11], 'le': [1, 8, 19], 'partie': [14], 'être': [10]}}
        new_dicoCand = updateDico2keys_liste(new_dicoCand, comptPhrase,NF)     
        new_dicoCand[comptPhrase][NF] = produit_cartesien(dicoCand[comptPhrase][NF])
  return new_dicoCand

def f1(dicoPOSnorm,dicoCand,NF2POSnorm) :  
  new_dicoCand = {}
  for numPhrase in dicoCand :
    for NF in dicoCand[numPhrase] :   
      for cand in range(0,len(dicoCand[numPhrase][NF])) :
        POSnorm_cand = dicoPOSnorm[numPhrase][NF][cand]
        # Verif si la POSnorm est tolérée par la NF        
        if POSnorm_cand in NF2POSnorm[NF] : 
            new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF)
            new_dicoCand[numPhrase][NF] += [dicoCand[numPhrase][NF][cand]]    
  return new_dicoCand

def f2(dicoCand,dicoPOSseq,POSnorm2POSseq) :  
  new_dicoCand = {}
  for numPhrase in dicoCand :
    for NF in dicoCand[numPhrase] :  
      for cand in range(0,len(dicoCand[numPhrase][NF])) :
        POSseq_cand = dicoPOSseq[numPhrase][NF][cand]
        # Conversion de la POSseq du candidat en POSnorm        
        POSnorm_cand = POSseq2POSnorm(POSseq_cand)
        # Verif si la POSseq observée fait bien partie de celles compatibles (observées) pour cette POSnorm
        if POSnorm_cand in POSnorm2POSseq : 
          allPOSseqTolerees = POSnorm2POSseq[POSnorm_cand]
          if POSseq_cand in allPOSseqTolerees :
            new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF)
            new_dicoCand[numPhrase][NF] += [dicoCand[numPhrase][NF][cand]]    
              
          
  return new_dicoCand

def f3(dicoCand,dicoPOS,NF2categ,NF2POSnorm,dico_seqPOS_perCat_POSnorm) :
  new_dicoCand = {}
  for numPhrase in dicoCand :
    dicoTok2POS = listeTokens2dico(dicoPOS[numPhrase])
    for NF in dicoCand[numPhrase] :
      cat = NF2categ[NF] 
      posNorm = list(NF2POSnorm[NF].keys())[0]
      for cand in range(0,len(dicoCand[numPhrase][NF])) :
        posSeq_cand = []
        tokens_cand = dicoCand[numPhrase][NF][cand]
        tok_debut = min(listeStr2int(tokens_cand.split("|")))
        tok_fin = max(listeStr2int(tokens_cand.split("|")))
        for t in range(tok_debut,tok_fin + 1) :
          posSeq_cand += [dicoTok2POS[t]]
        posSeq_cand_str = "|".join(posSeq_cand)
        if posSeq_cand_str in dico_seqPOS_perCat_POSnorm[cat][posNorm] :
          new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF)          
          new_dicoCand[numPhrase][NF] += [dicoCand[numPhrase][NF][cand]]     
  return new_dicoCand

#def NEW_distLinPerCat(dicoCands,NF2categ,stats_insertions)  :   
def f4(dicoCands,NF2categ,stats_insertions)  :   
  new_dicoCand = {}
  for numPhrase in dicoCands :
    for NF in dicoCands[numPhrase] :  
      cat = NF2categ[NF]
      max_insert_autorise = stats_insertions[cat]
      cands = dicoCands[numPhrase][NF]  
      for cand in cands :
        distanceLin_cand = NEW_calcul_distLin(cand)
        if distanceLin_cand <= max_insert_autorise :
          new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF)
          new_dicoCand[numPhrase][NF] += [cand]  
  return new_dicoCand

def f5(dicoCand):
  """
  On se restreint à un seul candidat par NF dans une phrase (celui qui a la plus petite distance linéaire)
  """
  new_dicoCand = {}
  min1 = -1
  min2 = -1
  for numPhrase in dicoCand :
    for NF in dicoCand[numPhrase] :  
      cands = dicoCand[numPhrase][NF]
      if len(cands) == 1 :
        # ici pas de sélection
        cand = dicoCand[numPhrase][NF]
        new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF)
        new_dicoCand[numPhrase][NF] = cands    
      else :
        # on sélectionne par nb d'insertions minimales
        # recherche de la valeur minimale :
        allDist = []
        for cand in cands : 
          distanceLin = NEW_calcul_distLin(cand)
          if distanceLin not in allDist :
            allDist.append(distanceLin)
          min1 = min(allDist)
        # filtrage des cands :
        nbCandconserve = 0
        for cand in cands : 
          distanceLin = NEW_calcul_distLin(cand)
          if distanceLin == min1 and nbCandconserve == 0:
            new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF)
            new_dicoCand[numPhrase][NF] += [cand]   
            nbCandconserve += 1
  return new_dicoCand
  
def f6(allCorpusPaths, chemins, dicoCand,dicoPhrases, dicoPOS,langue,corpus) :
  new_dicoCand = {}
  for numPhrase in dicoCand :    
    for NF in dicoCand[numPhrase] :  
      # filtre par distance syntaxique :
      for cand in dicoCand[numPhrase][NF] : 
        tokens = cand.split("|")
        tokens_ordered = sorted(listeStr2int(tokens))
        if len(tokens_ordered) == 1 :
            new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF)
            new_dicoCand[numPhrase][NF] += [cand] 
        elif len(tokens_ordered) == 2 :
          (dist2elts,typeDist2elts) = distanceSyntaxique(tokens_ordered[0],tokens_ordered[1],dicoPhrases[numPhrase])
          if dist2elts == 0 or (dist2elts ==1 and typeDist2elts == "serie") :
            new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF)
            new_dicoCand[numPhrase][NF] += [cand] 
        else :     
          connexions_dependance =  infos_dependances(dicoPhrases, numPhrase)
          # si >= 3 elts => cherche si distance syntaxique nulle
          premier_elt = tokens_ordered[0]
          dernier_elt = tokens_ordered[len(tokens_ordered)-1]
          nb_elts_connectes_syntax =  0
          couple_traite = []
          for elt in range(premier_elt, dernier_elt + 1) :
              if elt in tokens_ordered : 													# ie elt lex
                  for elt2 in range(premier_elt, dernier_elt + 1) :
                      #if elt2 in tokens_ordered and elt2 != elt and elt not in elts_traites and elt2 not in elts_traites :
                      if elt2 in tokens_ordered and (elt2,elt) not in couple_traite and (elt,elt2) not in couple_traite :
                          if connexions_dependance[str(elt2)] == str(elt) or connexions_dependance[str(elt)] == str(elt2) :
                            nb_elts_connectes_syntax += 1
                            couple_traite.append((elt,elt2))
          proportion_elts_connectes = nb_elts_connectes_syntax / (len(tokens_ordered) - 1)
          if proportion_elts_connectes == 1 :
            new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF)
            new_dicoCand[numPhrase][NF] += [cand] 
  return new_dicoCand

def f7(dicoCands,dicoPhrases, NF2categ,dico_NF2_case_morphoNoun,islanguecas) :
  new_dicoCand = {}
  if islanguecas :
    categs_prises_en_compte = ["VID","LVC.full","LVC,cause"]
    info_prise_en_compte = "caseNoun"
  else :
    categs_prises_en_compte = ["VID"]
    info_prise_en_compte = "numberNoun"    
  for numPhrase in dicoCands :
    for NF in dicoCands[numPhrase] :  
      cands = dicoCands[numPhrase][NF]
      if NF in dico_NF2_case_morphoNoun : 
        # ie NF comportant 1 seul V et un seul N => on cherche si morpho NOUN déjà vue dan train (cas pour langues à cas et nombre sinon)        
        cat = NF2categ[NF]
        if cat in categs_prises_en_compte :
          lignes = dicoPhrases[numPhrase].split("\n")
          for cand in cands :
            tokens_cand = cand.split("|")
            valeur_morpho_NOUN_cand = "" 
            for l in lignes :
              infos = l.split("\t")
              for t in tokens_cand :
                if str(infos[0]) == str(t) and infos[3] == "NOUN" :
                  infos_morpho = infos[5].split("|")  #Gender=Fem|Number=Sing
                  for i in infos_morpho :
                    trait = i.split("=")[0]
                    if trait == "Case" and islanguecas:
                      valeur_morpho_NOUN_cand = i.split("=")[1]                      
                    if trait == "Number" and not islanguecas :
                      valeur_morpho_NOUN_cand = i.split("=")[1]                      
            if valeur_morpho_NOUN_cand != "" :              
              if valeur_morpho_NOUN_cand in dico_NF2_case_morphoNoun[NF][info_prise_en_compte] :
                new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF)
                new_dicoCand[numPhrase][NF] += [cand]  
            else :      
                # ie si pas d'info => on garde le candidat       
                new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF)
                new_dicoCand[numPhrase][NF] += [cand]   
        else :
          # si c'est une MWE ne contenant pas 1 seul Noun et un seul Verb          
          for cand in cands :
              new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF)
              new_dicoCand[numPhrase][NF] += [cand]                
      else :
        # pour autres catégories 
        for cand in cands :
            new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF)
            new_dicoCand[numPhrase][NF] += [cand]  
  return new_dicoCand
  
def f8(dicoCands,dico_ImbricationsMWEs): 
  nbCandidats_conserves = 0
  new_dicoCand = {}
  for numPhrase in dicoCands :
    if numPhrase > -1 :
      candsInPhrase = []
      NFsinPhrase = []      
      for NF in dicoCands[numPhrase] :  
        cands = dicoCands[numPhrase][NF]
        for cand in cands :   
          candsInPhrase.append(cand.split("|"))
          NFsinPhrase.append(NF)    
      cands_imbriquants2imbriques = cherche_listes_imbriquees(candsInPhrase)
      dicoImbriquant2imbrique = {}
      NF_imbriquanteOUimbriquees = []
      NF_imbriquante_liste = []
      for cand_imbriquant in cands_imbriquants2imbriques :
        cand_imbriquant_tokens = cand_imbriquant.split("|")
        for indiceCand in range(0,len(candsInPhrase)) :
          if candsInPhrase[indiceCand] == cand_imbriquant_tokens :
            NF_imbriquante = NFsinPhrase[indiceCand]
            NF_imbriquanteOUimbriquees.append(NF_imbriquante)
            NF_imbriquante_liste.append(NF_imbriquante)
            for cand_imbrique in cands_imbriquants2imbriques[cand_imbriquant] :
              for indiceCand2 in range(0,len(candsInPhrase)) :
                if candsInPhrase[indiceCand2] == cand_imbrique :
                  NF_imbriquee = NFsinPhrase[indiceCand2]
                  NF_imbriquanteOUimbriquees.append(NF_imbriquee)
                  if NF_imbriquante not in dicoImbriquant2imbrique :
                    dicoImbriquant2imbrique[NF_imbriquante] = []
                    dicoImbriquant2imbrique[NF_imbriquante] = [(NF_imbriquante, candsInPhrase[indiceCand]),(NF_imbriquee, candsInPhrase[indiceCand2])]
                  else :
                    dicoImbriquant2imbrique[NF_imbriquante] += [(NF_imbriquante, candsInPhrase[indiceCand]),(NF_imbriquee, candsInPhrase[indiceCand2])]    
      NF_traitees = []
      NF_conservees = []
      liste_NF_imbriquees = []
      for NF in dicoCands[numPhrase] :
        if NF in dicoImbriquant2imbrique :
          for tuple_imbriquant_imbrique in range(0,len(dicoImbriquant2imbrique[NF])) :
                if tuple_imbriquant_imbrique%2 != 0 :
                  liste_NF_imbriquees.append(dicoImbriquant2imbrique[NF][tuple_imbriquant_imbrique][0])
      for NF in dicoCands[numPhrase] :
        if NF not in NF_imbriquanteOUimbriquees :
          cands = dicoCands[numPhrase][NF]
          for cand in cands :  
            new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF)
            new_dicoCand[numPhrase][NF] += [cand]    
            nbCandidats_conserves += 1
          NF_conservees.append(NF)
          NF_traitees.append(NF) 
        else :          
          # on annote toujours l'imbriquante et on vérifie s'il faut annoter les imbriquées
          if NF in dico_ImbricationsMWEs :       
            # ie c'est une NF qui peut en imbriquer d'autres
            NF_imbriquees_autorisees =  dico_ImbricationsMWEs[NF]
            NF_imbriquante = NF
            cands = dicoCands[numPhrase][NF_imbriquante]
            for cand in cands :  
              new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF_imbriquante)
              new_dicoCand[numPhrase][NF_imbriquante] += [cand]    
              nbCandidats_conserves += 1
            NF_traitees.append(NF_imbriquante)
            NF_conservees.append(NF_imbriquante)
            #---------- on vérifie s'il faut conserver les NF imbriquées -----------            
            if NF in dicoImbriquant2imbrique :             
              NF_imbriquees_cands = dicoImbriquant2imbrique[NF]       
              for NF_imbriquee_cand in NF_imbriquees_cands : 
                nom_NF = NF_imbriquee_cand[0]
                toks = NF_imbriquee_cand[1]
                if nom_NF in NF_imbriquees_autorisees :
                  new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,nom_NF)
                  new_dicoCand[numPhrase][nom_NF] += ["|".join(toks)]    
                  nbCandidats_conserves += 1
                  NF_conservees.append(nom_NF)
                NF_traitees.append(nom_NF)         
          elif NF in NF_imbriquante_liste and NF not in liste_NF_imbriquees and NF not in dico_ImbricationsMWEs :
            # c'est la plus longue séquence que l'on choisit d'annoter
            cands = dicoCands[numPhrase][NF]
            for cand in cands :  
              new_dicoCand = updateDico2keys_liste(new_dicoCand,numPhrase,NF)
              new_dicoCand[numPhrase][NF] += [cand]    
              nbCandidats_conserves += 1
              NF_traitees.append(NF)
  return new_dicoCand
