#!/usr/bin/python3
# -*- coding: utf-8 -*-


#==========================================
#             IMPORTS
#==========================================
import os
import sys
import numpy as np
from collections import Counter 
import math 
from scipy import spatial


#==========================================
#      FONCTIONS 
#==========================================

def lists_cosine_similarity(l1, l2): 
    """
    calcule la similarité cosinus entre 2 listes, renvoie un nombre entre -1 et 1.
    plus la valeur est proche de 1, plus les listes sont similaires
    """    
    c1 = Counter(l1) 
    c2 = Counter(l2) 
    terms = set(c1).union(c2) 
    dotprod = sum(c1.get(k, 0) * c2.get(k, 0) for k in terms) 
    magA = math.sqrt(sum(c1.get(k, 0)**2 for k in terms)) 
    magB = math.sqrt(sum(c2.get(k, 0)**2 for k in terms)) 
    return dotprod/(magA * magB) 

def whichPOScolumn(pathTRAIN):
    """
    Vérifie si la colonne UPOS contient des POS au format UD, sinon choisit le meilleur compromis entre les colonnes UPOS et XPOS
    Renvoie la colonne à prendre en compte pour les POS
    """
    UDPOS = ["ADJ", "ADP","ADV","AUX","CCONJ","DET", "INTJ", "NOUN", "NUM","PART", "PRON", "PROPN", "PUNCT", "SCONJ", "SYM", "VERB", "X"]  # cf site web UD
    listeUPOS = []
    listeXPOS = []
    with open (pathTRAIN, "r") as f_corpus :
        lignesCorpus = "".join(f_corpus.readlines()[1:])					
        phrases = lignesCorpus.split("\n\n")[:-1]
        for phrase in phrases :
            lignes = phrase.split("\n")
            for ligne in lignes :
                infos = ligne.split("\t")	
                if len(infos) > 3 : 
                    UPOS = infos[3]   
                    if UPOS not in listeUPOS :
                        listeUPOS.append(UPOS) 
                    XPOS = infos[4]   
                    if XPOS not in listeXPOS :
                        listeXPOS.append(XPOS)          
    if len(listeXPOS) == 1 :    #["_"]
        return (3,len(listeXPOS))                
    elif len(listeUPOS) == 1 :
        return (4,len(listeXPOS))   
    elif lists_cosine_similarity(listeUPOS, UDPOS) > 0.5 :    # calcul de similiarité avec liste des POS format UD
        return (3,len(listeXPOS))   
    elif len(listeUPOS) > len(listeXPOS) :
        return (3,len(listeXPOS))   
    else :
        return (4,len(listeXPOS))   
    

def modifColonne4(pathsCorpus) :
    """
    Si colonne 4 est utilisée pour les POS, on ne conserve que la 1ère lettre de la POS : 
    4	analizo	analiza	_	Sozet	_	...
    devient : 
    4	analizo	analiza	_	S	_ ...
    """      

    for pathCorpus in pathsCorpus :
        splitNomcorpus = str(pathCorpus).split("/")
        oldnomCorpus = str("/".join(splitNomcorpus[0:-1])) + "/initial_" +  str(splitNomcorpus[len(splitNomcorpus)-1])
        if not os.path.exists(oldnomCorpus) : 
            os.rename(pathCorpus, oldnomCorpus)   
            with open(oldnomCorpus,"r") as f_old:
                with open(pathCorpus,"a") as f_new:
                    lignes = f_old.readlines()		
                    for ligne in lignes :				
                        infos = ligne.split("\t") 
                        if "#" in str(infos[0]) or infos == ['\n'] :
                            f_new.write(ligne)
                        else :	
                            newligne = ""
                            for i in range(0,4) :	                            		
                                newligne += str(infos[i]) + "\t"
                            newligne += infos[4][0] + "\t"
                            for i in range(5,10) :
                                newligne += str(infos[i]) + "\t"
                            newligne += infos[10] 
                            f_new.write(newligne)