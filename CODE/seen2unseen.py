#!/usr/bin/python3
# -*- coding: utf-8 -*-


#==========================================
#             IMPORTS
#==========================================

import os
import sys
import time
import progressbar
import subprocess
import numpy as np
import collections
from collections import OrderedDict
from collections import Counter 
from tqdm import tqdm
from shutil import copyfile
import configparser
import ast
from threading import Thread
import math 
import shutil
import pickle
import statistics
import unicodedata
#-----------------------------
from util_utilitaires import *
from util_findPOScolumn import *
from util_cupt2Blind import *
from util_cupt2typo_POS_dep_morpho import * 
from util_distSyn import * 
from util_verifGlaff import *



def cupt2pickle(cupt,langue):   
    dico_mwe_lemmes = {}
    NFs_seen_in_train = []
    with open(cupt,"r") as f_in :
        contenu = f_in.read()	
        phrases = contenu.split("\n\n")        
        compt = 0
        for p in phrases :
            lignes = p.split("\n")
            for l in lignes :
                if len(l) > 0 :
                    if l[0] != "#" :
                        infos = l.split("\t")
                        lemme = infos[2].lower()
                        if "_" in str(lemme) : 
                            lemme = infos[1].lower()
                        else :
                            if "|" in str(lemme) : 
                                lemme = lemme.replace("|","-")                        
                        if infos[10] not in ["*","_"] :
                            annots = infos[10].split(";")
                            for a in annots :
                                infos_mwe = a.split(":")
                                num_mwe = infos_mwe[0]
                                if num_mwe not in dico_mwe_lemmes :   
                                    dico_mwe_lemmes[num_mwe] = [lemme]
                                else :
                                    dico_mwe_lemmes[num_mwe] += [lemme]
            compt += 1
            for num_mwe in dico_mwe_lemmes :
                NF = ";".join(sorted(dico_mwe_lemmes[num_mwe]))
                if NF not in NFs_seen_in_train :
                    NFs_seen_in_train.append(NF)    
    return NFs_seen_in_train

def extractInfosDiscont(col6_positionEltsMwe, lignes,corpus) : 
       catMWEs = {}
       for MWE in col6_positionEltsMwe :           #{'1': '2|3', '2': '10|12'}   
              catMWE = ""
              idMWE = "MWE" + str(MWE)
              positionEltsMwe = col6_positionEltsMwe[MWE].split("|")  #['1', '2', '3', '4']
              positionDebut = positionEltsMwe[0]
              positionFin = positionEltsMwe[len(positionEltsMwe)-1]
              for ligne in lignes :                           
                     infos = ligne.split("\t")                       
                     if len(infos) > 10 and "-" not in infos[0]:  
                            for elt in range(int(positionDebut),int(positionFin) + 1) : 
                                   elt_lex = False 
                                   for num_eltLex in positionEltsMwe :	#['1', '2', '3', '4']
                                          if int(elt) == int(num_eltLex) :	
                                                 elt_lex = True     
                                   if int(elt) == int(infos[0]) : 
                                          if elt_lex :                                                              
                                                 #---- catégorie MWE ----  
                                                 if ";" not in infos[10] and ":" in infos[10] :
                                                        #e.g.  1:VID
                                                        idMWE2 = "MWE" + str(infos[10].split(":")[0])
                                                        if idMWE2 not in catMWEs :
                                                               catMWEs[idMWE2] = infos[10].split(":")[1]                                                               
                                                 elif  ";" in infos[10] :                                             
                                                        #e.g.  1:VID;2:LVC.full          1;2:LVC.full
                                                        alltypes = infos[10].split(";")
                                                        for typeMWE in alltypes :
                                                               if ":" in typeMWE :                                                
                                                                      idMWE2 = "MWE" + str(typeMWE.split(":")[0])
                                                                      catMWEs[idMWE2] = typeMWE.split(":")[1]   
              if idMWE in catMWEs :
                     catMWE = catMWEs[idMWE]        
              else :
                     
                     # catégorie non trouvée car c'est un signe de ponctuation qui la contient
                     num_mwe = idMWE.split("MWE")[1]
                     for ligne in lignes :                           
                            infos = ligne.split("\t")                       
                            if len(infos) > 10 :
                                   if ";" not in infos[10] and ":" in infos[10] :
                                          idMWE_encours = infos[10].split(":")[0]
                                          if num_mwe ==  idMWE_encours :
                                                 catMWEs[idMWE] = infos[10].split(":")[1]                                                               
                                   elif  ";" in infos[10] :                                             
                                          #e.g.  1:VID;2:LVC.full          1;2:LVC.full
                                          alltypes = infos[10].split(";")
                                          for typeMWE in alltypes :
                                                 if ":" in typeMWE :                                                
                                                        idMWE_encours = typeMWE.split(":")[0]
                                                        if num_mwe ==  idMWE_encours :
                                                               catMWEs[idMWE] = typeMWE.split(":")[1]   
                     catMWE = catMWEs[idMWE]        
       return catMWEs

def cupt2gold_transformation_V_N(pathsCorpus,corpus,langue): 
       #localisation_mwe_dev = [] 
       liste_NF_annot = []
       nf_V_N = ""               
       #===================================================================
       # extract infos in cupt for annotated MWEs 
       #===================================================================       
       with open (pathsCorpus,"r") as f_cupt :
              infosCupt =  f_cupt.readlines()	
              corpusCupt = "".join(infosCupt[1:])					
              phrases = corpusCupt.split("\n\n")[:-1]
              comptPhrase = 1
              for phrase in phrases :     
                     mots = []  
                     col6_positionEltsMwe = {}		
                     col8_EltLexLemme = {}
                     col9_EltLexPOS = {}
                     lignes = phrase.split("\n")
                     for ligne in lignes :
                            if ligne[0] != "#" :              
                                   infos = ligne.split("\t")      
                                   # ce token fait-il partie de une ou plusieurs MWE => extarction du numéro de MWE + catégorie
                                   idMWEs = []   
                                   typeMWEs = []
                                   if infos[10] not in ["*","_"]  :
                                          if "," not in infos[1] :
                                                 if "-" not in str(infos[0]):
                                                     mots.append("["+str(infos[1])+"]")
                                                 eltLex = True
                                                 #  Overlap => 1:VID;2:IAV /1;2  vs pas d'overlap =>  1:VID /1         
                                                 if ";" not in infos[10] and ":"  not in infos[10] :                                    
                                                        idMWEs.append(infos[10])
                                                 elif ";" not in infos[10] :
                                                        idMWEs.append(infos[10].split(":")[0])
                                                        typeMWEs.append(infos[10].split(":")[1])
                                                 elif ":" not in infos[10] :
                                                        idMWEs = infos[10].split(";")
                                                 else :                                    
                                                        temp_idMWEs = infos[10].split(";")
                                                        for temp in temp_idMWEs :
                                                               idMWEs.append(temp.split(":")[0])
                                                               typeMWEs.append(infos[10].split(":")[1])
                                          else :
                                                 eltLex = False 
                                                 mots.append(str(infos[1]))
                                   else:
                                          eltLex = False  
                                          mots.append(str(infos[1]))    
                                   # caractéristiques de l'Elt Lex   
                                   if eltLex and infos[3] != "PUNCT" :  
                                          for idMWE in idMWEs :  
                                                 newId = "MWE" + str(idMWE)
                                                 if idMWE not in col6_positionEltsMwe :
                                                        col6_positionEltsMwe[idMWE] = infos[0]
                                                        col8_EltLexLemme[newId] = infos[2].lower()
                                                        if infos[2].lower() == "er|es|sie" :
                                                            col8_EltLexLemme[newId] = "er-es-sie"
                                                        col9_EltLexPOS[newId] = infos[3]  
                                                 else :
                                                        col6_positionEltsMwe[idMWE] = col6_positionEltsMwe[idMWE] + "|" + infos[0] 
                                                        if infos[2].lower() == "er|es|sie" :
                                                            col8_EltLexLemme[newId] =  col8_EltLexLemme[newId] + "|er-es-sie"
                                                        else :
                                                            col8_EltLexLemme[newId] =  col8_EltLexLemme[newId] + "|" + str(infos[2].lower())
                                                        col9_EltLexPOS[newId] = col9_EltLexPOS[newId] + "|" + infos[3] 

                     # ========================================================
                     #	OUTPUT (dans Fichier TSV si option debug)
                     # ========================================================

                     col4_catMWE = extractInfosDiscont(col6_positionEltsMwe, lignes,corpus)
                     col4_catMWE = sorted(col4_catMWE.items(), key=lambda t: t[0])    
                     col6_positionEltsMwe = sorted(col6_positionEltsMwe.items(), key=lambda t: t[0])     #{'1': '2|3', '2': '10|12'}
                     col8_EltLexLemme = sorted(col8_EltLexLemme.items(), key=lambda t: t[0])             #{'1': 'call|in', '2': 'pick|up'}
                     col9_EltLexPOS = sorted(col9_EltLexPOS.items(), key=lambda t: t[0])                 #{'1': 'VERB|ADP', '2': 'VERB|ADP'}                             
                     for MWE in range(0,len(col4_catMWE)): 
                            idMWE = col4_catMWE[MWE][0] 
                            catMWE = col4_catMWE[MWE][1]                     #VID          
                            lemmesEltLex = col8_EltLexLemme[MWE][1]          #a|little|birdie|tell  
                            lemmesEltLex_liste = lemmesEltLex.split("|") 
                            NF = ";".join(sorted(lemmesEltLex_liste))
                            POSseq = col9_EltLexPOS[MWE][1]                  #DET|ADJ|NOUN|VERB                               
                            POSnorm = ";".join(sorted(POSseq.split("|"))) 
                            
                            if POSnorm in ["NOUN;VERB","AUX;NOUN","DET;NOUN;VERB","AUX;NOUN;VERB","ADP;NOUN;VERB","ADP;AUX;NOUN","ADP;DET;NOUN;VERB","ADP;AUX;DET;NOUN"] :
                                lemmes = []
                                for i in range(0,len(POSseq.split("|"))) :    
                                    if POSseq.split("|")[i] in ["VERB","AUX"] :                                        
                                        lemmes.append(lemmesEltLex_liste[i])
                                    elif POSseq.split("|")[i] in ["NOUN"] :                                        
                                        lemmes.append(lemmesEltLex_liste[i])
                                nf_V_N = ";".join(sorted(lemmes))
                            if nf_V_N not in liste_NF_annot:
                                liste_NF_annot.append(nf_V_N)        
                     comptPhrase += 1

       savepickle(liste_NF_annot, str(repParent) + "/OUTPUT_seen/INFOS/FR/" + str(corpus) + "_mweV_N.pickle")

def cupt2gold(pathsCorpus,corpus,langue): 
        localisation_mwe = [] 
        liste_NF_annot = []
        #===================================================================
        # extract infos in cupt for annotated MWEs 
        #===================================================================       
        with open (pathsCorpus,"r") as f_cupt :
              infosCupt =  f_cupt.readlines()	
              corpusCupt = "".join(infosCupt[1:])					
              phrases = corpusCupt.split("\n\n")[:-1]
              comptPhrase = 1
              for phrase in phrases :     
                     mots = []  
                     col6_positionEltsMwe = {}		
                     col8_EltLexLemme = {}
                     lignes = phrase.split("\n")
                     for ligne in lignes :
                            if ligne[0] != "#" :              
                                   infos = ligne.split("\t")      
                                   # ce token fait-il partie de une ou plusieurs MWE => extarction du numéro de MWE + catégorie
                                   idMWEs = []   
                                   typeMWEs = []
                                   if infos[10] not in ["*","_"]  :
                                          if "," not in infos[1] :
                                                 if "-" not in str(infos[0]):
                                                     mots.append("["+str(infos[1])+"]")
                                                 eltLex = True
                                                 #  Overlap => 1:VID;2:IAV /1;2  vs pas d'overlap =>  1:VID /1         
                                                 if ";" not in infos[10] and ":"  not in infos[10] :                                    
                                                        idMWEs.append(infos[10])
                                                 elif ";" not in infos[10] :
                                                        idMWEs.append(infos[10].split(":")[0])
                                                        typeMWEs.append(infos[10].split(":")[1])
                                                 elif ":" not in infos[10] :
                                                        idMWEs = infos[10].split(";")
                                                 else :                                    
                                                        temp_idMWEs = infos[10].split(";")
                                                        for temp in temp_idMWEs :
                                                               idMWEs.append(temp.split(":")[0])
                                                               typeMWEs.append(infos[10].split(":")[1])
                                          else :
                                                 eltLex = False 
                                                 mots.append(str(infos[1]))
                                   else:
                                          eltLex = False  
                                          mots.append(str(infos[1]))    
                                   # caractéristiques de l'Elt Lex   
                                   if eltLex and infos[3] != "PUNCT" :  
                                          for idMWE in idMWEs :  
                                                 newId = "MWE" + str(idMWE)
                                                 if idMWE not in col6_positionEltsMwe :
                                                        col6_positionEltsMwe[idMWE] = infos[0]
                                                        col8_EltLexLemme[newId] = infos[2].lower()
                                                        if infos[2].lower() == "er|es|sie" :
                                                            col8_EltLexLemme[newId] = "er-es-sie"
                                                 else :
                                                        col6_positionEltsMwe[idMWE] = col6_positionEltsMwe[idMWE] + "|" + infos[0] 
                                                        if infos[2].lower() == "er|es|sie" :
                                                            col8_EltLexLemme[newId] =  col8_EltLexLemme[newId] + "|er-es-sie"
                                                        else :
                                                            col8_EltLexLemme[newId] =  col8_EltLexLemme[newId] + "|" + str(infos[2].lower())
                                                       
                     # ============
                     #	OUTPUT
                     # ============
                     col4_catMWE = extractInfosDiscont(col6_positionEltsMwe, lignes,corpus)
                     col4_catMWE = sorted(col4_catMWE.items(), key=lambda t: t[0])    
                     col6_positionEltsMwe = sorted(col6_positionEltsMwe.items(), key=lambda t: t[0])     #{'1': '2|3', '2': '10|12'}
                     col8_EltLexLemme = sorted(col8_EltLexLemme.items(), key=lambda t: t[0])             #{'1': 'call|in', '2': 'pick|up'}
                     for MWE in range(0,len(col4_catMWE)): 
                            idMWE = col4_catMWE[MWE][0] 
                            catMWE = col4_catMWE[MWE][1]                     #VID          
                            lemmesEltLex = col8_EltLexLemme[MWE][1]          #a|little|birdie|tell  
                            lemmesEltLex_liste = lemmesEltLex.split("|") 
                            NF = ";".join(sorted(lemmesEltLex_liste))
                            if NF not in liste_NF_annot:
                                liste_NF_annot.append(NF)
                            tok_annot = sorted(listeStr_2int(col6_positionEltsMwe[MWE][1].split("|")))               
                            localisation_mwe.append(str(comptPhrase) + ";" + "|".join(listeInt2Str(tok_annot)))
                     comptPhrase += 1
        if not os.path.exists(str(repParent) + "/OUTPUT_seen/INFOS/" + str(langue) + "/" + str(corpus) + "_NFseen.pickle"):
           savepickle(liste_NF_annot, str(repParent) + "/OUTPUT_seen/INFOS/" + str(langue) + "/" + str(corpus) + "_NFseen.pickle")
        if corpus == "dev_gold" and not os.path.exists(str(repParent) + "/OUTPUT_seen/INFOS/" + str(langue) + "/" + str(corpus) + "_localisationSeen.pickle"):
           savepickle(localisation_mwe, str(repParent) + "/OUTPUT_seen/INFOS/" + str(langue) + "/" + str(corpus) + "_localisationSeen.pickle")
        return  liste_NF_annot, localisation_mwe         

def debug_estimationQteUnseen(langue):
    train_seen = []
    val_precision = 0
    #=================================================
    # Seen : quelle est la proportion de gold parmi les LVC et VID trouvés ?
    # ie précision
    #=== cherche la précision obtenue pour la config retenue pour DEV 
    with open(str(repParent) + "/OUTPUT_seen/EVAL_allLanguages/2020_best_configs_Fscore_onDEV.tsv","r") as f_in :
        lignes = f_in.readlines()
        for l in lignes :
            infos = l.split("\t")
            if len(infos) > 2 :
                lang = infos[0]
                if lang == langue :
                    bestconfig = infos[2].split("\n")[0]
    with open(str(repParent) + "/OUTPUT_seen/EVAL_allLanguages/" + str(langue) + "/DEV/eval_DEV_config" + str(bestconfig) + ".txt","r") as f_in :
        lignes = f_in.readlines()
        for l in lignes :
            if "* Seen-in-train: MWE-based:" in str(l) :
                infos = l.split("* Seen-in-train: MWE-based: P=")
                if len(infos) > 0 :
                    perfo = infos[1] 
                    precision = perfo.split(" ")[0]
                    val_precision = float(precision.split("=")[1])
    # Unseen : combien de V-N extraits dans DEV sont Gold ?
    with open(str(repParent) + "/OUTPUT_unseen/CANDIDATES/" + str(langue) + "/" + str(langue) + "_dev_extraction_V-N_IRVoption.tsv","r") as f_in :
        compt_V_N_dev_Gold = 0
        dev_not_mweVN = []
        nb_V_N_extraits_inDev = 0
        lignes = f_in.readlines()
        for l in lignes[1:] :              
            infos = l.split("\t")
            idphrase = infos[1]
            toks = infos[2]   
            nf = infos[3]         
            if infos[11] == "annot_mwe\n" and infos[4] != "n_a":
                nb_V_N_extraits_inDev += 1
            if infos[0] == "nf unseen" and infos[11] == "not_annot_mwe\n" and infos[4] != "n_a":
                if nf not in dev_not_mweVN :
                    dev_not_mweVN.append(nf)
    with open (str(repParent) + "/Unseen_infos/estimation_qteLVC_VID_a_annoter.tsv","a") as f_out :
        f_out.write("Langue\tTest: nb gold LVC_VID\tTest : nb LVC_VID annotes par Seen2Seen\tDev: nb gold LVC_VID\t        Dev : precision Seen\tDev : nb V_N extraits qui sont MWE\tTest : Nb restant estimé à annoter\n")
        #==================== combien de LVC-vid restent à annoter dans Test ? ===========
        comptGoldTest,annoteSeenTest = nb_LVC_VID_a_trouver("test",langue)
        comptGoldDev,annoteSeenTest = nb_LVC_VID_a_trouver("dev",langue)
        nb_seen_annote_corriges = annoteSeenTest * val_precision
        # ratio de V-N extaits GOLD parmi tous les VID et LVC gold à annoter dans Dev
        ratio_V_Nextraits_parrapport_a_LVC_VID = nb_V_N_extraits_inDev / comptGoldDev
        estimation_LVC_VID_V_N_extractibles = int((comptGoldTest-nb_seen_annote_corriges)*ratio_V_Nextraits_parrapport_a_LVC_VID)
        f_out.write(str(langue) + "\t" + str(comptGoldTest) + "\t" + str(annoteSeenTest) + "\t" + str(comptGoldDev) + "\t" + str(val_precision) +"\t" + str(nb_V_N_extraits_inDev) + "\t" + str(estimation_LVC_VID_V_N_extractibles) + "\n")
        if not os.path.exists(str(repParent) + "/OUTPUT_unseen/CANDIDATES/"+ str(langue) + "/" + str(langue) + "_dev_not_mweVN.pickle") :
            savepickle(dev_not_mweVN,str(repParent) + "/OUTPUT_unseen/CANDIDATES/"+ str(langue) + "/" + str(langue) + "_dev_not_mweVN.pickle")

def langue2maxDist():
    """
    Distance linéaire max hors hapax pour LVC et VID
    """
    d = {}
    d["EL"] = 21
    d["TR"] = 11
    d["ZH"] = 13
    d["SV"] = 4
    d["RO"] = 3
    d["PT"] = 17
    d["PL"] = 11
    d["IT"] = 9
    d["HI"] = 2
    d["HE"] = 9
    d["GA"] = 7
    d["FR"] = 13
    d["EU"] = 7
    d["DE"] = 23
    return d

def savepickle(dico,outfile):
    with open(outfile, 'wb') as f :
        pickle.dump(dico, f,protocol=pickle.HIGHEST_PROTOCOL)  

def dico2stat(d):
    s = {}
    for k in d:
        tot = 0
        mwe = 0
        for k1 in d[k]:
            tot += d[k][k1]
            if k1 == "annot_mwe":
                mwe += d[k][k1]
        s[k] = mwe/tot
    return s

def extr_V_PRON(corpus,langue, cupt, dico_insert, dico_stat_insert,dicoNumPhrase2maxAnnot):    
    symboles = ["°","'","!","@","#","&","(",")","_","–","[","{","}","]",":",";",",","?","/","|","*","`","~","$","^","+","=","<",">","“",".","«","»"]
    #================================================
    #  IRV cranberry  unseen ET proba cranberry > 90
    #================================================
    nom_f_IRV = str(langue) + "_liste4_IRV.tsv"  
    with open(os.path.join(repParent,"CODE", "listes_generees",nom_f_IRV), "r") as f2:
        liste4_IRV = []
        lignes = f2.readlines() 
        for l in lignes: 
            infos = l.split("\t")
            if len(infos) > 1: 
                nf = infos[0]
                deja_annot = infos[1]
                proba_se = infos[5].split("\n")[0]
                total = infos[3]
                if nf not in liste4_IRV and deja_annot == "unseen" and float(proba_se) > 80 and int(total) > 1: 
                    liste4_IRV.append(nf) 

    #================================================
    #  Extraction de SE + verbes dans cupt
    #================================================
    with open(cupt,"r") as f_in :            
            contenu = f_in.read()	
            phrases = contenu.split("\n\n")        
            compt = 1
            non_IRV = []
            for p in phrases :
                if compt in dicoNumPhrase2maxAnnot : 
                    num_annot_possible = dicoNumPhrase2maxAnnot[compt]
                else :
                    num_annot_possible = 1 
                lignes = p.split("\n")
                lemmes_MWE = []
                for l in range(0,len(lignes)) :                    
                    if len(lignes[l]) > 0  :
                        if lignes[l][0] != "#" :
                            infos = lignes[l].split("\t")
                            if "-" not in str(infos[0]) :
                                annot = infos[10]
                                num_mwes = []
                                if infos[10] not in ["*","_"] :
                                        annots = infos[10].split(";")
                                        for a in annots :
                                            infos_mwe = a.split(":")
                                            num_mwe = infos_mwe[0]
                                            num_mwes.append(int(num_mwe))     
                                            if int(num_mwe) >= num_annot_possible: 
                                                num_annot_possible = int(num_mwe) + 1
                for l in range(0,len(lignes)) :                    
                    if len(lignes[l]) > 0  :
                        if lignes[l][0] != "#" :
                            infos = lignes[l].split("\t")
                            if "-" not in str(infos[0]) :
                                num_tok = infos[0]
                                annot = infos[10]
                                POS = infos[3]
                                lemme = infos[2].lower()                                
                                dep_cand = ""
                                tok_cand = ""
                                dep = int(infos[6])
                                dep1 =infos[7]
                                num_mwes = []
                                is_seen_train = ""
                                if POS == "VERB" or (POS == "PRON" and lemme in ["se","s'"]):                                    
                                    intersect = False
                                    if infos[10] not in ["*","_"] :
                                        intersect = True 
                                        annots = infos[10].split(";")
                                        for a in annots :
                                            infos_mwe = a.split(":")
                                            num_mwe = infos_mwe[0]
                                            num_mwes.append(int(num_mwe))     
                                        
                                    for l2 in range(0,len(lignes)) :
                                        if len(lignes[l2]) > 0 and l2 != l: 
                                            if lignes[l2][0] != "#" :
                                                infos2 = lignes[l2].split("\t")
                                                deja_annot ="not_annot_mwe"
                                                if "-" not in str(infos2[0]) : 
                                                    cand_valide = True
                                                    num_tok2 = infos2[0]
                                                    annot2 = infos2[10]
                                                    POS2 = infos2[3]
                                                    lemme2 = infos2[2].lower()   
                                                    
                                                    for char in lemme2 :
                                                        if char in symboles :
                                                            cand_valide = False           
                                                    for char in lemme :
                                                        if char in symboles :
                                                            cand_valide = False                                     
                                                    num_mwes2 = []
                                                    if int(num_tok2) == dep and cand_valide :
                                                        if POS2 == "VERB"and POS == "PRON" and lemme in ["se","s'"] or POS2 == "PRON" and POS == "VERB" and lemme2 in ["se","s'"]: 
                                                            bilan = ""
                                                            bilan_test = ""
                                                            dep2 = infos2[7]
                                                            cand_string = ""
                                                            tok_cand = str(num_tok)+"|"+str(num_tok2)
                                                            dist = abs(int(num_tok) - int(num_tok2)) - 1
                                                            if POS == "VERB": 
                                                                cand_string = str(lemme) + " " + str(lemme2)
                                                            else: 
                                                                cand_string = str(lemme2) + " " + str(lemme)                                                                
                                                            if infos2[10] not in ["*","_"] :
                                                                intersect = True
                                                                annots2 = infos2[10].split(";")
                                                                for a in annots2 :
                                                                    infos2_mwe = a.split(":")
                                                                    num_mwe2 = int(infos2_mwe[0])
                                                                    num_mwes2.append(num_mwe2)
                                                                if len(num_mwes)>0:
                                                                    for n in num_mwes:
                                                                        for n2 in num_mwes2:
                                                                            if n == n2:
                                                                                deja_annot = "annot_mwe"                                                                
                                                            nf =";".join(sorted([lemme,lemme2]))       
                                                            if corpus !=  "train": 
                                                                if nf in liste4_IRV :                                                 
                                                                    bilan = "faut_annoter :['l4']"
                                                                else :                                                 
                                                                    bilan = "faut_pas_annoter : pas cranberry dans conlluST"
                                                                non_IRV = loadpickle(os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_train_non_IRV.pickle"))
                                                                if nf in non_IRV :  
                                                                    bilan = "faut_pas_annoter : nf non annot Train"
                                                                if intersect: 
                                                                    bilan = "faut_pas_annoter : overlap"
                                                                if nf in NFs_seen_in_train:
                                                                    is_seen_train = "nf seen"
                                                                else:
                                                                    is_seen_train = "nf unseen"
                                                            else :
                                                                if deja_annot != "annot_mwe" and nf not in non_IRV: 
                                                                    non_IRV.append(nf)   
                                                                #=========== stats distance linéaire =======                                                                                                                             
                                                                if dist not in dico_insert:
                                                                    dico_insert[dist] = {}  
                                                                    dico_insert[dist][deja_annot] = 1 
                                                                else : 
                                                                    if deja_annot not in dico_insert[dist]:
                                                                        dico_insert[dist][deja_annot] = 1                                                                  
                                                                    else :
                                                                        dico_insert[dist][deja_annot] += 1

                                                            with open(os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue), str(langue) + "_" + str(corpus) +  "_extraction_V-N_IRVoption.tsv"),"a") as f_out:                                                                
                                                                proba_insert = "n_a"   # pas utilsé pour IRV
                                                                f_out.write(str(is_seen_train)+"\t" +  str(compt)+"\t"+str(tok_cand)+"\t" + str(nf) + "\tn_a\t" + str(bilan) + "\t" + str(bilan_test) + "\t" + str(num_annot_possible) + ":IRV" + "\t" + str(dist) + "\tn_a\t" + str(proba_insert) + "\t" + str(deja_annot)+"\n")
                                                            num_annot_possible += 1    

                compt += 1
    if corpus == "train":
        savepickle(non_IRV,os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_" + str(corpus) + "_non_IRV.pickle"))
        savepickle(dico_insert,os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_" + str(corpus) + "_dico_insert_IRV.pickle"))   
    return dico_insert

def fichier2liste_generee(f) :
    with open(f, "r") as f1:
        liste = []
        lignes = f1.readlines()
        for l in lignes: 
            infos = l.split("\t")
            if len(infos) > 1: 
                nf = infos[0]
                deja_annot = infos[1]
                if deja_annot == "unseen" and nf not in liste: 
                    liste.append(nf) 
    return liste

def fichier2liste3_AMI_generee(f,liste3_sans_ami) :
    # restriction aux NF de valeur d'AMI > 0
    with open(f, "r") as f1:
        liste = []
        lignes = f1.readlines()
        for l in lignes[1:]: 
            infos = l.split("\t")            
            if len(infos) > 1: 
                if not "invalide" in str(infos[len(infos)-2]) and not "n_a" in str(infos[len(infos)-1]) :
                    cand = ast.literal_eval(infos[len(infos)-2])
                    nf = ";".join(cand[0].split("|"))
                    nb_cooc = int(infos[len(infos)-4]) 
                    if nf in liste3_sans_ami : 
                        valeur_ami = float(infos[len(infos)-1].split("\n")[0])                        
                        if valeur_ami > 0 and nb_cooc >= 10 : 
                            #if valeur_ami > 0 : 
                            liste.append(nf)
    return liste
   
def nb_LVC_VID_a_trouver(corpus, langue):
    if "test" in str(corpus):
        corpusGold = "test-stats.md"
    elif "dev" in str(corpus) :
        corpusGold = "dev-stats.md"
    #------------ Nb total (seen + unseen) à annoter comme LVC et VID ----------------
    chemin = os.path.join(repParent,"INPUT",langue,corpusGold)
    comptgold = 0
    with open(chemin,"r") as f_in :
        lignes = f_in.readlines()
        for l in lignes :
            if "LVC" in str(l) or "VID" in str(l) :
                comptgold += int(l.split(": ")[1].split("\n")[0])
    #------------ Nb déjà annoté comme LVC et VID seen -------
    config = configparser.ConfigParser()
    config.read(str(repParent)+'/config.cfg')
    nb_deja_annotes =  int(config.get('Parameters', str(langue) + "_nb_seen_annotes_par_sys"))
    return comptgold, nb_deja_annotes

def extr_V_N(corpus,langue, cupt, dico_dep, dico_insert, dico_stat_dep, dico_stat_insert,dist_seen_train):
    compt_annot_sys_V_N = 0
    symboles = ["°","'","!","@","#","&","(",")","_","–","[","{","}","]",":",";",",","?","/","|","*","`","~","$","^","+","=","<",">","“",".","«","»"]
    list1  =[]
    list2  =[]
    list3  =[]
    list3_ami = []  
    dicoNumPhrase2maxAnnot = {}
    list1 = fichier2liste_generee(os.path.join(repParent,"CODE", "listes_generees",str(langue) + "_liste1.tsv"))
    list2 = fichier2liste_generee(os.path.join(repParent,"CODE", "listes_generees",str(langue) + "_liste2.tsv"))   

    if langue == "FR" or langue == "IT":
        nom_l3 = str(langue) + "_liste3_brute.tsv"  
        nom_l3_AMI = str(langue) + "_liste3_AMI.tsv"  
        list3 = fichier2liste_generee(os.path.join(repParent,"CODE", "listes_generees",nom_l3)) 
        list3_ami = fichier2liste3_AMI_generee(os.path.join(repParent,"CODE", "listes_generees",nom_l3_AMI),list3)
    
    with open(cupt,"r") as f_in :
            with open(os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_" + str(corpus) + "_extraction_V-N_IRVoption.tsv"),"a") as f_out:
                f_out.write("seen in Train?\tNumPhrase\tTokens cand\tNF cand\tOrdre Verb-Noun\tAction\tAction complémentaire\tAnnotation proposée\tDistLin\tDeps\tP(distLin) pour IRV ; P(distLin)x P(Deps) pour LVC/VID \tTokens annotés (manuel pour Dev, auto pour Test\tChoix final\n")            
            contenu = f_in.read()	
            phrases = contenu.split("\n\n")        
            compt = 1
            not_mwe_train = []
            not_mwe_dev = []
            for p in phrases :
                num_annot_possible = 1
                lignes = p.split("\n")
                lemmes_MWE = []
                for l in range(0,len(lignes)) :                    
                    if len(lignes[l]) > 0:
                        if lignes[l][0] != "#" :
                            infos = lignes[l].split("\t")
                            if "-" not in str(infos[0]) :
                                annot = infos[10]
                                num_mwes = []                    
                                if infos[10] not in ["*","_"] :
                                    annots = infos[10].split(";")
                                    for a in annots :
                                        infos_mwe = a.split(":")
                                        num_mwe = infos_mwe[0]
                                        num_mwes.append(int(num_mwe))        
                                        if int(num_mwe) >= num_annot_possible: 
                                            num_annot_possible = int(num_mwe) + 1  
                for l in range(0,len(lignes)) :                    
                    if len(lignes[l]) > 0:
                        if lignes[l][0] != "#" :
                            infos = lignes[l].split("\t")
                            if "-" not in str(infos[0]) :
                                is_seen_train = ""
                                num_tok = infos[0]
                                annot = infos[10]
                                POS = infos[3]
                                lemme = infos[2].lower()
                                dep_cand = ""
                                tok_cand = ""
                                dep = int(infos[6])
                                dep1 =infos[7]
                                num_mwes = []                                
                                if POS == "VERB" or POS == "NOUN":                                    
                                    intersect = False
                                    if infos[10] not in ["*","_"] :
                                        intersect = True 
                                        annots = infos[10].split(";")
                                        for a in annots :
                                            infos_mwe = a.split(":")
                                            num_mwe = infos_mwe[0]
                                            num_mwes.append(int(num_mwe))                               
                                    for l2 in range(0,len(lignes)) :
                                        if len(lignes[l2]) > 0 and l2 != l: 
                                            if lignes[l2][0] != "#" :
                                                infos2 = lignes[l2].split("\t")
                                                deja_annot ="not_annot_mwe"
                                                if "-" not in str(infos2[0]) : 
                                                    cand_valide = True
                                                    num_tok2 = infos2[0]
                                                    annot2 = infos2[10]
                                                    POS2 = infos2[3]
                                                    lemme2 = infos2[2].lower()                                               
                                                    num_mwes2 = []
                                                    for char in lemme2 :
                                                        if char in symboles :
                                                            cand_valide = False           
                                                    for char in lemme :
                                                        if char in symboles :
                                                            cand_valide = False 
                                                    if int(num_tok2) == dep and cand_valide:
                                                        if POS2 == "VERB"and POS == "NOUN" or POS2 == "NOUN" and POS == "VERB": 
                                                            bilan = ""
                                                            bilan_test = ""
                                                            dep2 = infos2[7]
                                                            cand_string = ""
                                                            dep_cand = str(dep1)+"|"+str(dep2)
                                                            tok_cand = str(num_tok)+"|"+str(num_tok2)
                                                            dist = abs(int(num_tok) - int(num_tok2)) - 1  

                                                            nf =";".join(sorted([lemme,lemme2]))
                                                            
                                                            
                                                            if nf in NFs_seen_in_train:
                                                                is_seen_train = "nf seen"
                                                            else:
                                                                is_seen_train = "nf unseen"
                                                            if POS == "NOUN": 
                                                                dep_n = infos[7]
                                                            else: 
                                                                dep_n = infos2[7]
                                                            if POS == "VERB": 
                                                                dep_v = infos[7]
                                                                cand_string = str(lemme) + " " + str(lemme2)
                                                            else: 
                                                                dep_v = infos2[7]
                                                                cand_string = str(lemme2) + " " + str(lemme)       
                                                            if infos2[10] not in ["*","_"] :
                                                                intersect = True
                                                                annots2 = infos2[10].split(";")
                                                                for a in annots2 :
                                                                    infos2_mwe = a.split(":")
                                                                    num_mwe2 = int(infos2_mwe[0])
                                                                    num_mwes2.append(num_mwe2)
                                                                if len(num_mwes)>0:
                                                                    for n in num_mwes:
                                                                        for n2 in num_mwes2:
                                                                            if n == n2:
                                                                                deja_annot = "annot_mwe"                                                              
                                                            
                                                            if corpus == "train" :
                                                                if is_seen_train == "nf seen" and deja_annot == "annot_mwe" :
                                                                    if dist not in dist_seen_train :
                                                                        dist_seen_train.append(dist)

                                                                if is_seen_train != "nf seen" and deja_annot != "annot_mwe" and nf not in not_mwe_train: 
                                                                    not_mwe_train.append(nf)                                                                
                                                                if dep_cand not in dico_dep:
                                                                    dico_dep[dep_cand] = {}  
                                                                    dico_dep[dep_cand][deja_annot] = 1 
                                                                else : 
                                                                    if deja_annot not in dico_dep[dep_cand]:
                                                                        dico_dep[dep_cand][deja_annot] = 1                                                                  
                                                                    else :
                                                                        dico_dep[dep_cand][deja_annot] += 1                                                                
                                                                if dist not in dico_insert:
                                                                    dico_insert[dist] = {}  
                                                                    dico_insert[dist][deja_annot] = 1 
                                                                else : 
                                                                    if deja_annot not in dico_insert[dist]:
                                                                        dico_insert[dist][deja_annot] = 1                                                                  
                                                                    else :
                                                                        dico_insert[dist][deja_annot] += 1
                                                            else :   
                                                                dist_max = langue2maxDist()
                                                                if int(dist) > int(dist_max[langue]) :
                                                                    bilan = "faut_pas_annoter : valeur distLin"
                                                                else :
                                                                    not_mwe_train = loadpickle(os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_train_not_mweVN.pickle"))
                                                                    not_mwe_dev = loadpickle(os.path.join(repParent,"Unseen_infos",str(langue),str(langue) + "_dev_not_mweVN.pickle"))
                                                                    if nf in not_mwe_train :  
                                                                        bilan = "faut_pas_annoter : nf non annot Train"
                                                                    elif nf in not_mwe_dev :  
                                                                        bilan = "faut_pas_annoter : nf non annot Dev"
                                                                    else :
                                                                        intersection_liste_generees = []
                                                                         #------------------------------------------------
                                                                        if nf in list1 :           
                                                                            intersection_liste_generees.append("l1") 
                                                                        if nf in list2:                
                                                                            intersection_liste_generees.append("l2")  
                                                                           
                                                                        if nf in list3_ami:       
                                                                            intersection_liste_generees.append("l3")                         
                                                                        #------------------------------------------------
                                                                        liste_mwe_dev = loadpickle(os.path.join(repParent,"OUTPUT_seen","INFOS", str(langue),"dev_gold_NFseen.pickle"))
                                                                        liste_mwe_train = loadpickle(os.path.join(repParent,"OUTPUT_seen","INFOS", str(langue),"train_NFseen.pickle"))
                                                                        liste5_mwes_train_dev =  liste_mwe_dev + liste_mwe_train
                                                                        if nf in liste5_mwes_train_dev:       
                                                                            intersection_liste_generees.append("dev")
                                                                        #------------------------------------------------
                                                                        if len(intersection_liste_generees) == 0 :
                                                                            bilan = "faut_ranker"    
                                                                        if intersect: 
                                                                            
                                                                            bilan = "faut_pas_annoter : overlap annot existante"     
                                                                    if bilan == "" and len(intersection_liste_generees) > 0 :                                                 
                                                                        bilan = "faut_annoter :" + str(intersection_liste_generees)
                                                                        compt_annot_sys_V_N += 1
                                                            with open(os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_" + str(corpus) +  "_extraction_V-N_IRVoption.tsv"),"a") as f_out:
                                                                if corpus != "train": 
                                                                    dicoNumPhrase2maxAnnot[compt] = num_annot_possible
                                                                    if dep_cand in dico_stat_dep:
                                                                        proba_dep = dico_stat_dep[dep_cand]
                                                                    else:
                                                                        proba_dep = 0
                                                                    if dist in dico_stat_insert:
                                                                        proba_insert = dico_stat_insert[dist]
                                                                    else:
                                                                        # on interpole en utilisant la moyenne entre la proba de la distance inférieure existante et de la supérieure
                                                                        if dist <  dist_seen_train[len(dist_seen_train)-1]  :
                                                                            for i in range(0,len(dist_seen_train)) :
                                                                                if dist > dist_seen_train[i] and dist < dist_seen_train[i + 1] :
                                                                                    proba_insert = statistics.mean([dico_stat_insert[dist_seen_train[i]],dico_stat_insert[dist_seen_train[i+1]]])
                                                                                   
                                                                        else :
                                                                            proba_insert = 0
                                                                if corpus != "train":
                                                                    proba_cand = proba_dep * proba_insert
                                                                    if proba_cand == 0 :
                                                                        bilan = "faut_pas_annoter : proba distOudep nulle"
                                                                else:
                                                                    proba_cand = "n_a"
                                                                f_out.write(str(is_seen_train)+ "\t" +  str(compt)+"\t"+str(tok_cand)+ "\t" +str(nf) +  "\t" + str(cand_string) + "\t" + str(bilan) + "\t" + str(bilan_test) + "\t"  +str(num_annot_possible) + ":LVC.full" + "\t" + str(dist) + "\t" + str(dep_cand) + "\t" + str(proba_cand) + "\t" +  str(deja_annot)+ "\n")
                                                                num_annot_possible += 1
                compt += 1
    if corpus == "train":
        savepickle(sorted(dist_seen_train), os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_" + str(corpus) + "_liste_dist_seen.pickle"))
        savepickle(not_mwe_train,os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_" + str(corpus) + "_not_mweVN.pickle"))
        savepickle(dico_dep,os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_" + str(corpus) + "_dico_dep.pickle"))
        savepickle(dico_insert,os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_" + str(corpus) + "_dico_insert.pickle"))
    return sorted(dist_seen_train), dico_dep,dico_insert, dicoNumPhrase2maxAnnot, compt_annot_sys_V_N

def annot_unseen(langue,corpus2annot, valeurs_rank_R3_utiles) :
    if not os.path.exists(os.path.join(repParent,"RESULT_SYSTEM_UNSEEN")) :
        os.makedirs(os.path.join(repParent,"RESULT_SYSTEM_UNSEEN"))
    if not os.path.exists(os.path.join(repParent,"RESULT_SYSTEM_UNSEEN",langue)) :
        os.makedirs(os.path.join(repParent,"RESULT_SYSTEM_UNSEEN",langue))
    chemin_tableur_cands_a_annoter = os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",langue, str(langue) + "_" + str(corpus2annot) + "_extraction_V-N_IRVoption_FINAL.tsv")
    chemin_f_a_annoter = os.path.join(repParent,"RESULT_SYSTEM_SEEN",langue,str(corpus2annot) + ".system.cupt")
    chemin_f_annote = os.path.join(repParent,"RESULT_SYSTEM_UNSEEN",langue,str(corpus2annot) + ".system.cupt")
    with open(chemin_f_a_annoter,"r") as f_blind , open(chemin_f_annote,"a") as f_out, open(chemin_tableur_cands_a_annoter,"r") as f_rankfinal:
        dico_annot = {}
        lignes = f_rankfinal.readlines()
        for l in lignes[1:] :            
            infos = l.split("\t")
            if len(infos) > 1 :
                numPhrase = infos[1]
                tokens = infos[2]                
                if len(infos) == 16 :
                    action = infos[15]                        
                    if "FINAL:faut_annoter" in str(action):
                        tokens_liste = tokens.split("|")
                        premier_token = str(min(listeStr2int(tokens_liste)))
                        annot_proposee = infos[7]
                        if numPhrase not in dico_annot :
                            dico_annot[numPhrase] = {}    
                            for t in tokens_liste :
                                if t != premier_token :
                                    dico_annot[numPhrase][t] = annot_proposee.split(":")[0]
                                else :
                                    dico_annot[numPhrase][t] = annot_proposee
                        else :
                            for t in tokens_liste :
                                if t in dico_annot[numPhrase] :
                                    if t != premier_token :
                                        dico_annot[numPhrase][t] += ";" + annot_proposee.split(":")[0]
                                    else :
                                        dico_annot[numPhrase][t] += ";" + str(annot_proposee)
                                else :
                                    if t != premier_token :
                                        dico_annot[numPhrase][t] = annot_proposee.split(":")[0]
                                    else :
                                        dico_annot[numPhrase][t] = annot_proposee        
        infosCupt =  f_blind.readlines()	
        corpusCupt = "".join(infosCupt[1:])					
        phrases = corpusCupt.split("\n\n")[:-1]
        comptPhrase = 1
        f_out.write(str(infosCupt[0]))
        for numPhrase in range(0,len(phrases)) : 
            lignes = phrases[numPhrase].split("\n")
            if str(comptPhrase) not in dico_annot :
                for ligne in lignes :
                    f_out.write(str(ligne) + "\n")
            else :
                for ligne in lignes :
                    infos = ligne.split("\t")
                    if ligne[0] == "#" :
                        f_out.write(str(ligne) + "\n")
                    else :                        
                        numTok = infos[0]
                        infos_conservees = "\t".join(infos[0:10])
                        annot_actuelle = infos[10]
                        if numTok in dico_annot[str(comptPhrase)] :    
                            if annot_actuelle == "*" :
                                f_out.write(str(infos_conservees) + "\t" + str(dico_annot[str(comptPhrase)][numTok] + "\n"))
                            else :
                                f_out.write(str(infos_conservees) + "\t" + str(annot_actuelle) + ";" + str(dico_annot[str(comptPhrase)][numTok]) + "\n")
                        else :
                            f_out.write(str(ligne) + "\n")
            
            f_out.write("\n")
            comptPhrase += 1
      
def nettoyage_candidat(langue, cand_traduit) :   
    symboles_hors_tiret = ["'","°","!","@","#","&","(",")","_","–","[","{","}","]",":",";",",","?","/","|","*","`","~","$","^","+","=","<",">","“",".","«","»"]
    sequencesPOS_frequentes = ["V|A","V|N","N|V","V|D|N","V|S|N","V|S|D|N","V|D|N|N","V|D|N|S|D|N","V|P","P|P|V","P|V"]
    pronoms_sujets = ["je","tu","il","elle","nous","vous","ils","elles","on"]
    pronoms_reflechis = ["toi","nous","vous"]
    preps_simples = ["à", "après", "avant", "avec", "chez", "contre", "dans", "de", "depuis", "derrière", "dès", "devant", "en", "entre", "envers", "hors", "jusque", "par", "parmi", "pendant", "pour", "sans", "selon", "sous","sur", "vers","comme"]      # NB "comme" est considéré à la frontière entre prep et adv
    possessifs = ["ma","mon","mes","ta","ton","tes","son","sa","ses","notre","nos","votre","vos","leur","leurs"]
    det = ["le","les","la","un","une","des"]
    aux = ["être","avoir"]
    
    #===========================================================
    #   [FR] gestions amalgamés : de le / de la / aux (supprimés)
    #===========================================================
    if "|de|la|" in str(cand_traduit) :
        cand_traduit =cand_traduit.replace("|de|la|","|")
    elif "|de|le|" in str(cand_traduit) :
        cand_traduit = cand_traduit.replace("|de|le|","|")  
    if "|aux|" in str(cand_traduit) :
        cand_traduit =cand_traduit.replace("|aux|","|à|")
    if "|au|" in str(cand_traduit) :
        cand_traduit =cand_traduit.replace("|au|","|à|")
    if "|du|" in str(cand_traduit) :
        cand_traduit =cand_traduit.replace("|du|","|")
    #======================================
    mots_separes = cand_traduit.split("|")
    new_mots_separes = []      
    existe_mot_vide = False  
    cand_valide = True
    for m in mots_separes :                     
        new_m =unicodedata.normalize('NFKD', m)  # normalise ligatures
        #========================================================================
        #    [FR] : suppression des pronoms et articles élidés 
        #========================================================================
        if "j'" in str(new_m) or "d'" in str(new_m) or "l'" in str(new_m)  :
            new_m = m.split("'")[1]  
        # lemmatisation du s' en SE
        if "s'" in str(new_m)  :
            new_mots_separes += ["se",m.split("'")[1]] 
        #========================================================================
        #  Si un mot est vide  => EP candidate supprimé
        #=========================================================================
        if len(m) == 0 :
            existe_mot_vide = True     
        #========================================================================
        # Génération de ligatures (car entrées du Glaff le nécessitent)  
        #========================================================================
        if "oe" in str(new_m)  :
            new_m = new_m.replace("oe","œ")  
        if "ae" in str(new_m)  :
            new_m = new_m.replace("ae","æ")
        #========================================================================
        #  suppression des éléments non pertinents : 
        # pronoms sujets / ponctuation  OU mis en option (possessif)        
        #========================================================================
        if new_m not in symboles_hors_tiret and new_m != "-" and new_m not in pronoms_sujets :
            new_mots_separes += [new_m]                      

    if len(new_mots_separes) == 2 :        
        for m in new_mots_separes :
            if m in preps_simples or m in det :
                cand_valide = False

        if new_mots_separes[1] in pronoms_reflechis :
            #=====================================
            # [FR] rappelle toi => rappelle se
            #=====================================
            new_mots_separes = ["se",new_mots_separes[0]]
    
    if len(new_mots_separes) > 2 :  
        #===========================================================
        # faire ses/des début => faire débuts
        #===========================================================
        new = []
        for m in new_mots_separes :
            if m not in possessifs and m not in det:
                new += [m]
        new_mots_separes = new
 
    if not existe_mot_vide and len(new_mots_separes) > 1 and cand_valide :   
        #==========================================================================================
        # Lemmatisation et POS avec le Glaff                
        # on ne garde pas les monotokens 
        # conserve combinaisons de lemmes correspondant patrons de POS les  plus fréquents en FR
        #==========================================================================================
        lemmes_possibles, ordre_nom_verbe_autorise = ep2motscherches(langue, aux,"|".join(new_mots_separes))
        if "[]" in str(lemmes_possibles):
            return new_mots_separes, "invalide (mot inconnu)"

        elif "invalide" not in str(lemmes_possibles) :
            candidat_valide = filtre_ep_lemmatisee_pos_LVCorVID(lemmes_possibles,aux,sequencesPOS_frequentes)    
            if candidat_valide == "faire seconde passe en supprimant auxiliaire" :
                new_cand_traduit = "|".join(cand_traduit.split("|")[1:]) 
                new_mots_separes, candidat_valide = nettoyage_candidat(langue, new_cand_traduit) 
                return new_mots_separes, candidat_valide
            
            elif len(new_mots_separes) == 2 and  ordre_nom_verbe_autorise == False :
                    new_candidat_valide = []
                    for cand in ast.literal_eval(candidat_valide) :
                        ordre = cand[1]
                        if ordre != "N|V" :
                            new_candidat_valide += [cand]
                    if len(new_candidat_valide) > 0 :
                        return new_mots_separes, str(new_candidat_valide)
                    else :
                        return new_mots_separes, "invalide (ordre N-V sans passif)"
            elif len(new_mots_separes) == 2 and  ordre_nom_verbe_autorise == True :
                    return new_mots_separes, str(candidat_valide)
                
            else :
                return new_mots_separes, candidat_valide
        else :
            return new_mots_separes, "invalide (1)"    
    else :
        return new_mots_separes, "invalide (2)"

#=============================================
#  Extraction d'infos dans Train et candidats
#=============================================
# Paramètres
#Parameters already in configFile :
config = configparser.ConfigParser()
config.read(str(repParent)+'/config.cfg')
corpusTRAIN = config.get('Parameters', 'corpusTRAIN')
corpusDEV = config.get('Parameters', 'corpusDEV')
corpusTEST = config.get('Parameters', 'corpusTEST')
langues = ast.literal_eval(config.get('Parameters', 'langues'))
do_eval_test = config.get('Parameters', 'calculPerfoTEST')   # à mettre à valuer False pour ST
#=================================================================
do_eval_test = False
#=================================================================
for langue in langues :     

    #=========================================================
    #   DEV et TRAIN gold => liste des NF seen
    #==========================================================
    if os.path.exists(str(repParent) + "/OUTPUT_seen/INFOS/" + str(langue) + "/dev_gold_NFseen.pickle"):
        dev_seen = loadpickle(str(repParent) + "/OUTPUT_seen/INFOS/" + str(langue) + "/dev_gold_NFseen.pickle")
        localisation_mwe_dev = loadpickle(str(repParent) + "/OUTPUT_seen/INFOS/" + str(langue) + "/dev_gold_localisationSeen.pickle")
        train_seen = loadpickle(str(repParent) + "/OUTPUT_seen/INFOS/" + str(langue) + "/train_NFseen.pickle")
        
    else :
        dev_seen, localisation_mwe_dev = cupt2gold(str(repParent) + "/INPUT/" + str(langue) + "/dev.cupt" ,"dev_gold" ,langue)
        train_seen, localisation_mwe_train = cupt2gold(str(repParent) +  "/INPUT/" + str(langue) + "/train.cupt" ,"train" ,langue)
    
    #----------------------------------------- FR-------------------------------
    # pour comparaison des listes traduites avec NF annotées en FR
    # Toutes NF, toutes catégories
    if not os.path.exists(str(repParent) + "/OUTPUT_seen/INFOS/FR/dev_gold_NFseen.pickle"):
        cupt2gold(str(repParent) + "/INPUT/FR/dev.cupt" ,"dev_gold" ,"FR")
        cupt2gold(str(repParent) +  "/INPUT/FR/train.cupt" ,"train" ,"FR")  
    # Uniquement LVC et VID simplifiées sous la forme V-N e.g. prendre la fuite => prendre|fuite
    if not os.path.exists(str(repParent) + "/OUTPUT_seen/INFOS/FR/dev_gold_mweV_N.pickle") : 
        cupt2gold_transformation_V_N(str(repParent) + "/INPUT/FR/dev.cupt", "dev_gold" ,"FR") 
        cupt2gold_transformation_V_N(str(repParent) + "/INPUT/FR/train.cupt","train","FR") 
    #----------------------------------------------------------------------------
    
    train_cupt = os.path.join(repParent,"INPUT",langue,"train.cupt")
    chemin_seen = os.path.join(repParent,"OUTPUT_seen","INFOS",langue,"train.cupt_NFs_seen_in_train.pickle")
    chemin_seen2 = os.path.join(repParent,"OUTPUT_seen","INFOS",langue,"NF2categ.pickle")
    NFs_seen_in_train = loadpickle(chemin_seen2)   
    #===============================================
    #   Extraction candidats Unseen
    #===============================================
    
    print("Extract cands : " + str(langue))    
    if not os.path.exists(os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_" + "test_extraction_V-N_IRVoption.tsv")):
             
        #==================================================================        
        #          STEP1 :  extrait cands Unseen 
        #   ces candidats sont stockés dans tableur TSV
        # soit faut_annoter : dans L1, L2, L3 (pour FR et IT), L4 (pour FR et PT), ou bien dans L5 (DEV)
        # soit faut_pas_annoter 
        # soit faut ranker (ie les autres)
        #==================================================================   
        test_fourni = False
        chemin_cands = os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",langue)
        if not os.path.exists(chemin_cands) :
            os.makedirs(chemin_cands)
        dist_seen = []
        dico_dep = {}
        dico_stat_dep ={}
        dico_insert = {}
        dico_insert_IRV = {}
        dico_stat_insert = {}
        dico_stat_insert_IRV = {}
        
        dev_cupt = os.path.join(repParent,"INPUT",langue,"dev.cupt")
        test_cupt = os.path.join(repParent,"RESULT_SYSTEM_SEEN",langue,"test.system.cupt") # avec annotations préliminaires en SEEN 
        if os.path.exists(test_cupt) :
            test_fourni = True
        if not os.path.exists(os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_train_not_mweVN.pickle")):    
            # 1/ extract cands V-N connectés dans TRAIN => stats dist_linéaire, dependances, cand NON annotés
            #print("extract Cands in Train")
            dist_seen_train, dico_dep, dico_insert,dicoNumPhrase2maxAnnot, compt_annot_sys_V_N = extr_V_N("train",langue, train_cupt,dico_dep,  dico_insert, dico_stat_dep, dico_stat_insert,dist_seen)
            dico_stat_dep = dico2stat(dico_dep)
            dico_stat_insert = dico2stat(dico_insert)        
            if langue in ["FR","PT"]:
                dico_insert_IRV = extr_V_PRON("train",langue, train_cupt, dico_insert_IRV, dico_stat_insert_IRV,dicoNumPhrase2maxAnnot)
                dico_stat_insert_IRV = dico2stat(dico_insert_IRV)
            #======================== DEV SANS annot préalable des seen ========================     
            # 2/ extract cands V-N connectés dans DEV => exploite stats + cand NON annotés + cand générés listes 1 à 4
            #print("extract Cands in Dev")
            dist_seen, dico_dep, dico_insert, dicoNumPhrase2maxAnnot_DEV,compt_annot_sys_V_N = extr_V_N("dev",langue, dev_cupt,dico_dep, dico_insert, dico_stat_dep, dico_stat_insert,dist_seen_train)
            if langue in ["FR","PT"]:
                extr_V_PRON("dev",langue, dev_cupt, dico_insert_IRV, dico_stat_insert_IRV,dicoNumPhrase2maxAnnot_DEV)   
            
            #======================== TEST  avec annot préalable des seen ======================== 
            if test_fourni : 
                # 2/ extract cands V-N connectés dans DEV => exploite stats + cand NON annotés + cand générés listes 1 à 4
                dist_seen, dico_dep, dico_insert, dicoNumPhrase2maxAnnot_TEST,compt_annot_sys_V_N_TEST = extr_V_N("test",langue, test_cupt,dico_dep, dico_insert, dico_stat_dep, dico_stat_insert,dist_seen_train)
                if langue in ["FR","PT"]:
                    extr_V_PRON("test",langue, test_cupt, dico_insert_IRV, dico_stat_insert_IRV,dicoNumPhrase2maxAnnot_TEST)   
                
        else:
            # 2 ou 3/ extract cands V-N connectés dans DEV/TEST => exploite stats + cand NON annotés + cand générés listes 1 à 4
            dico_dep= loadpickle(os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue), str(langue) + "_train_dico_dep.pickle"))
            dico_insert =  loadpickle(os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue), str(langue) + "_train_dico_insert.pickle"))
            dist_seen_train = loadpickle (os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_train_liste_dist_seen.pickle"))
            dico_stat_dep = dico2stat(dico_dep)
            dico_stat_insert = dico2stat(dico_insert)
            #======================== DEV  SANS annot préalable des seen ========================     
            dist_seen, dico_dep, dico_insert, dicoNumPhrase2maxAnnot_DEV,compt_annot_sys_V_N =  extr_V_N("dev",langue, dev_cupt,dico_dep, dico_insert, dico_stat_dep, dico_stat_insert,dist_seen_train) 
            if langue in ["FR","PT"]:
                dico_insert_IRV =  loadpickle(os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue), str(langue) + "_train_dico_insert_IRV.pickle"))
                dico_stat_insert_IRV = dico2stat(dico_insert_IRV)
                extr_V_PRON("dev",langue, dev_cupt, dico_insert_IRV, dico_stat_insert_IRV,dicoNumPhrase2maxAnnot_DEV)
           
    
    if langue not in ["FR"] :
        if os.path.exists(os.path.join(repParent,"Unseen_infos",str(langue),str(langue) + "_cands_traduits.tsv"))  :
            #===============================================================================        
            #          STEP2 :  Nouveau filtre des cands Unseen 
            #   langues != FR et IT : 
            #   1/ on traduit les cands 
            #   2/ on nettoie et filtre les traductions
            #   3/ chaque trad référencée dans les listes FR est conservée => candidat annoté "faut_annoter : trad seen-FR"
            #===============================================================================  
            
            cands_annotes_l1 = 0
            cands_annotes_l2 = 0
            cands_annotes_l3 = 0
            cands_annotes_l5 = 0
            tableur_cand = os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_" + "test_extraction_V-N_IRVoption.tsv")
            tableur_cand_new = os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_" + "test_extraction_V-N_IRVoption_TRAD.tsv")
            cand_traduits = os.path.join(repParent,"Unseen_infos",str(langue),str(langue) + "_cands_traduits.tsv")
            cand_traduits_corr  = os.path.join(repParent,"Unseen_infos",str(langue),str(langue) + "_cands_traduits_nettoyes.tsv")
            with open(tableur_cand,"r") as f_in, open(tableur_cand_new,"a") as f_out, open(cand_traduits,"r") as f_trad, open(cand_traduits_corr,"a") as f_trad_corr :
                # 1 / on récupère les traductions faites vers FR
                f_trad_corr.write("Candidat traduit\tCorrections possibles\tcandidat nettoyé\n")
                if not os.path.exists(os.path.join(repParent,"Unseen_infos",str(langue),str(langue) + "_dico_NF2trad.pickle")) :
                    dico_NF2trad = {}
                    lignes = f_trad.readlines()
                    message = str(langue) + ": nettoie trad"
                    pbar = tqdm(lignes[1:],desc=message) 
                    for l in pbar :
                        infos = l.split("\t")
                        if len(infos) > 1 :
                            nf  = infos[1]
                            trad = infos[3]
                            #2/ on les nettoie par POS, POS seq, lemmatisation => NF        
                            new_mots_separes, candidat_valide  = nettoyage_candidat(langue,trad)
                            nf_corrigee = "n_a"
                            if "invalide" not in str(candidat_valide) :
                                lemmes_possibles = ast.literal_eval(nettoyage_candidat(langue,trad)[1])
                                for t in lemmes_possibles :
                                    pos = t[1] 
                                    if t[1] == "V|N" :
                                        nf_corrigee = ";".join(sorted(t[0].split("|")))
                            if nf not in dico_NF2trad :
                                dico_NF2trad[nf] = nf_corrigee
                            f_trad_corr.write(str(trad) + "\t" +str(new_mots_separes) + "\t" + str(candidat_valide) + "\t"  + str(nf_corrigee) + "\n")
                    savepickle(dico_NF2trad,os.path.join(repParent,"Unseen_infos",str(langue),str(langue) + "_dico_NF2trad.pickle"))
                else :
                    dico_NF2trad = loadpickle(os.path.join(repParent,"Unseen_infos",str(langue),str(langue) + "_dico_NF2trad.pickle"))
                #3/ on cherche intersection avec listes 1; 2; 3FR ; liste 5FR (EP vues dans Train-FR et Dev-FR)
                if not os.path.exists(os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_dico_NF2action.pickle")) :
                
                    dico_NF2ami_l3 = {}
                    dico_NF2action = {}
                    liste1_FR = os.path.join(repParent,"CODE", "listes_generees","FR_liste1.tsv")
                    liste2_FR = os.path.join(repParent,"CODE", "listes_generees","FR_liste2.tsv")
                    liste3_FR_AMI = os.path.join(repParent,"CODE", "listes_generees","FR_liste3_AMI.tsv")
                    #------------------------- EP attestées en FR ---------------------------------------
                    liste_mwe_dev_FR = loadpickle(os.path.join(repParent,"OUTPUT_seen","INFOS", "FR","dev_gold_NFseen.pickle"))
                    liste_mwe_train_FR = loadpickle(os.path.join(repParent,"OUTPUT_seen","INFOS", "FR","train_NFseen.pickle"))
                    liste5_mwes_train_dev_FR =  liste_mwe_dev_FR + liste_mwe_train_FR
                    #------------------------- EP attestées en FR mais modifiées pour ne conserver que V-N sur quelques POSnorm ciblées (VDN...) ---------------------------------------
                    liste5bis_mwe_dev_V_N_FR = loadpickle(os.path.join(repParent,"OUTPUT_seen","INFOS", "FR","dev_gold_mweV_N.pickle"))
                    liste5bis_mwe_train_V_N_FR = loadpickle(os.path.join(repParent,"OUTPUT_seen","INFOS", "FR","train_mweV_N.pickle"))
                    liste5bis_mwe_train_dev_V_N_FR = liste5bis_mwe_dev_V_N_FR + liste5bis_mwe_train_V_N_FR                    

                    #-----------------------------------------------------------------------------------------------
                    with open(liste3_FR_AMI,"r") as f_l3_ami,  open(liste1_FR,"r") as f_l1,  open(liste2_FR,"r") as f_l2  :
                        dico_alternances = loadpickle(os.path.join(repParent,"Unseen_infos","FR_mot2alternances_LVC.pickle"))
                        for nf_init in dico_NF2trad :                        
                            nf_init = dico_NF2trad[nf_init]  # NF corrigée                        
                            if nf_init != "n_a" :
                                #================ ajout tolérance variabilité du verbe parmi alternances =====
                                lemmes = nf_init.split(";")
                                all_nfs_alternances = []
                                lemmes1 = []
                                lemme2 = [] 
                                for l in lemmes :                  
                                    if l in dico_alternances["VERB"] :
                                        lemmes1 = dico_alternances["VERB"][l]
                                    else :
                                        lemme2.append(l)
                                lemmes_modif = prod_cartesien(lemmes1,lemme2)
                                for l in lemmes_modif :
                                    all_nfs_alternances.append(";".join(sorted(l)))
                                #============================================================================
                                for nf in all_nfs_alternances :
                                    if nf not in dico_NF2action :                                                                         
                                        # cherche dans L5 = EP annotées manuellement en FR
                                        if nf in liste5_mwes_train_dev_FR or nf in liste5bis_mwe_train_dev_V_N_FR:
                                            dico_NF2action[nf] = "faut_annoter : l5"
                                            cands_annotes_l5 += 1
                                    if nf not in dico_NF2action :                                     
                                        # cherche dans L1
                                        lignes1 = f_l1.readlines()
                                        for l1 in lignes1 :
                                            infos = l1.split("\t")
                                            if len(infos) > 1 :
                                                nf_l1 = infos[0]
                                                statut = infos[1]
                                                if nf_l1 == nf and nf not in dico_NF2action :
                                                    dico_NF2action[nf] = "faut_annoter : l1"
                                                    cands_annotes_l1 += 1
                                    if nf not in dico_NF2action :     
                                        # cherche dans L2                                    
                                        lignes2 = f_l2.readlines()
                                        for l2 in lignes2 :
                                            infos = l2.split("\t")
                                            if len(infos) > 1 :
                                                nf_l2 = infos[0]
                                                statut = infos[1]
                                                if nf_l2 == nf and nf not in dico_NF2action :
                                                    dico_NF2action[nf] = "faut_annoter : l2"
                                                    cands_annotes_l2 += 1
                                    if nf not in dico_NF2action : 
                                        # cherche dans L3 ET valeur AMI > 0 ET nbcocc > 10
                                        lignes3 = f_l3_ami.readlines()
                                        for l3 in lignes3 :
                                            infos = l3.split("\t")                                         
                                            if not "invalide" in str(infos[len(infos)-2]) : 
                                                expression = ast.literal_eval(infos[len(infos)-2])
                                                nf_l3 = ";".join(sorted(expression[0].split("|")))
                                                val_ami = infos[len(infos)-1]  
                                                nf_choisie = infos[len(infos)-2] 
                                                for i in range(0,len(infos)-2) :
                                                    if infos[i] == nf_choisie :
                                                        nb_coocc = int(infos[i-1])
                                                if nf == nf_l3 and val_ami > 0 and nb_coocc >= 10 :
                                                    dico_NF2ami_l3[nf] = val_ami
                                                    dico_NF2action[nf] = "faut_annoter : l3"   
                                                    cands_annotes_l3 += 1  
                            savepickle(dico_NF2action,os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_dico_NF2action.pickle"))
                    
                    with open (os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_2020-07-02_intersection_l1_l2_l3_FR.tsv"),"a") as f_info :
                        f_info.write("Nb cands annotés car intersect L1_FR = " + str(cands_annotes_l1) + "\n")
                        f_info.write("Nb cands annotés car intersect L2_FR = " + str(cands_annotes_l2) + "\n")
                        f_info.write("Nb cands annotés car intersect L3_FR = " + str(cands_annotes_l3) + "\n")
                        f_info.write("Nb cands annotés car intersect L5_FR = " + str(cands_annotes_l5) + "\n")
                
                    #==================================================================================
                    # Nouveau fichier candidat avec mention "faut_annoter" pour intersections
                    # NB : ecrite fichier _TRAD à supprimer car en fait on utilise seulement 
                    #  par la suite le NF2action.pickle généré
                    #==================================================================================
                    lignes = f_in.readlines()
                    f_out.write(lignes[0])
                    for l in lignes[1:] :
                        infos = l.split("\t")
                        if len(infos) > 1 :
                            nf  = infos[3]   
                            if nf in  dico_NF2action :
                                infos_new_debut = infos[0:5]
                                infos_new_fin = infos[6:len(infos)]
                                f_out.write("\t".join(infos_new_debut + [dico_NF2action[nf]] + infos_new_fin))
                            else :
                                f_out.write(l)                
                else :
                    dico_NF2action =  loadpickle(os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_dico_NF2action.pickle"))
                    
                
                      
    #===============================================================================        
    #          STEP3 :  nouveau niveau de filtrage
    #   Candidats restants : classés par AMI et annotés comme EP 
    #   jusqu'à atteindre le quota d'EP indiqué dans test.md
    #  avec un prorata établi d'après DEV sur le nb de V-N extraits p/p au nb de LVC -VID gold
    #===============================================================================  

    dico_cand2rang_Pdist_dep = {}
    dico_cand2rang_AMI = {}
    nb_cands_faut_annoter = 0

    fichier_cand = os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_" + "test_extraction_V-N_IRVoption.tsv")
    fichier_cand_R1 = os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_" + "test_extraction_V-N_IRVoption_temp.tsv")
    fichier_cand_R2 = os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_" + "test_extraction_V-N_IRVoption_FINAL.tsv")
    
    # 1/ Obtention ranking R1 : Pdist x Pdep
    with open(fichier_cand,"r") as f_cand, open(fichier_cand_R1,"a") as f_out:
        lignes = f_cand.readlines()
        infos_entete = lignes[0].split("\t")
        infos_new = infos_entete[0:len(infos_entete) -1]
        derniere_cell = [infos_entete[len(infos_entete)-1].split("\n")[0]]
        f_out.write("\t".join(infos_new + ["ranking R1", "ranking R2","ranking R3"] + derniere_cell) + "\n")
        all_probas = []
        for l in lignes[1:] : 
            infos = l.split("\t")
            action = infos[5]
            if action == "faut_ranker" :            
                proba = float(infos[10])
                if proba not in all_probas and proba > 0 :
                    all_probas.append(proba)
        all_probas = sorted(all_probas,reverse=True) # par  proba décroissante
        for l in lignes[1:] : 
            infos = l.split("\t")
            action = infos[5]
            infos_new = infos[0:len(infos) -1]
            derniere_cell = [infos[len(infos)-1].split("\n")[0]]
            if action == "faut_ranker" :     
                numP = infos[1]
                toks = infos[2]
                id_cand = str(numP) + ";" + str(toks)
                proba = float(infos[10])    
                if proba == 0 :
                    rank_R1 = "n_a"
                else :                
                    rank_R1 =  all_probas.index(proba) + 1 # car indice démarre à zéro 
                f_out.write("\t".join(infos_new + [str(rank_R1), "n_a","n_a"] + derniere_cell) + "\n")
            elif "faut_annoter" in str(action):
                nb_cands_faut_annoter += 1
                f_out.write("\t".join(infos_new + ["n_a", "n_a","n_a"] + derniere_cell) + "\n")
        
     

    # 2/ Obtention ranking R2 : NF2AMI
    #=========================== parcourt fichier AMI pour obtenir rang des candidats ====================
    fichier_ami = os.path.join(repParent,"Unseen_infos",str(langue),"2020-07-01_"+str(langue)+"_cand2AMI.tsv")    
    with open(fichier_cand_R1,"r") as f_cand, open(fichier_ami,"r") as f_ami:
        dico_Rank_final = {}     
        valeurs_ami = []
        dicoNF2AMI = {}
        dicoNF2nbcooc = {}
        lignes2 = f_ami.readlines()
        for l in lignes2[1:]:
            infos = l.split("\t")
            lemmes = []   
            dic = ast.literal_eval(infos[0])
            for k in dic :
                lemmes += [k.split("=")[0]]      
                nf = ";".join(sorted(lemmes))
            nb_coocc = infos[1]
            val_ami = infos[3].split("\n")[0]
            if "n_a" not in str(val_ami):        
                if float(val_ami) > 0 and int(nb_coocc) >= 10: 
                    dicoNF2AMI[nf] = val_ami     
                    dicoNF2nbcooc[nf] = nb_coocc 
                    if val_ami not in valeurs_ami :                    
                        valeurs_ami.append(float(val_ami))    
        all_val_ami = sorted(valeurs_ami,reverse=True) # par AMI décroissante
        #-----------------------------------------------
        lignes = f_cand.readlines()
        for l in lignes[1:] : 
            infos = l.split("\t")
            action = infos[5]
            if action == "faut_ranker" :
                numP = infos[1]
                toks = infos[2]
                id_cand = str(numP) + ";" + str(toks)
                nf = infos[3]
                if nf in dicoNF2AMI :
                    ami = dicoNF2AMI[nf]
                    nb_cooc = dicoNF2nbcooc[nf]
                else : 
                    ami = "n_a"
                    nb_cooc = "n_a"
                infos_new = infos[0:len(infos) -3]
                derniere_cell = [infos[len(infos)-1].split("\n")[0]]
                rank_R1 = infos[len(infos) -4]
                if "n_a" in str(ami) or "n_a" in str(rank_R1) or "n_a" in str(nb_cooc) :
                    rank_R2 = "n_a"
                    rank_R3 = "n_a"
                elif float(ami) <= 0 or int(nb_cooc) < 10  :
                    rank_R2 = "n_a"
                    rank_R3 = "n_a"
                else :                         
                    rank_R2 =  int(all_val_ami.index(float(ami))) + 1 # car indice démarre à zéro 
                    rank_R3 =  (1/int(rank_R1)) + 1/int(rank_R2)
                    if rank_R3 not in dico_Rank_final :
                        dico_Rank_final[rank_R3] = 1
                    else :
                        dico_Rank_final[rank_R3] += 1

    #================ cherche la valeur R3 minimale permettant d'avoir le topx candidats avec topx = nb de candidats estimés =========   
    valeurs_rank_Rfinal_utiles = [] # ie plus élevées en accord aec le nb d'EP à trouver
    debug_estimationQteUnseen(langue)    
    estimation_nb_annot = 0
    with open(os.path.join(repParent,"Unseen_infos","estimation_qteLVC_VID_a_annoter.tsv"),"r") as f_estim :
        lignes = f_estim.readlines()
        for l in lignes[1:] :
            infos = l.split("\t")
            lang = infos[0]
            if lang == langue :
                estimation_nb_annot = int(infos[6].split("\n")[0])

    
    nb_restant_LVC_VID_a_annoter = estimation_nb_annot - nb_cands_faut_annoter
    if nb_restant_LVC_VID_a_annoter < 0 :
        nb_restant_LVC_VID_a_annoter = 0    
    nb_cand_conserves = 0
    sort_dico_Rank_final = sorted(dico_Rank_final.items(),reverse = True)
    for k,v in sort_dico_Rank_final:
        val = float(k)
        nb_cand = int(v)
        if nb_cand + nb_cand_conserves <= nb_restant_LVC_VID_a_annoter :
            valeurs_rank_Rfinal_utiles.append(val)
            nb_cand_conserves += nb_cand

    if len(valeurs_rank_Rfinal_utiles) > 0 :
        # ie on n'a pas atteint le quota estimé de candidats unseen
        rank_min_pour_select = min(valeurs_rank_Rfinal_utiles)
    else :
        rank_min_pour_select = "n_a"
    #============ on indique dans le fichier en dernière colonne les Ep à annoter =======
    with open(fichier_cand_R1,"r") as f_cand, open(fichier_ami,"r") as f_ami, open(fichier_cand_R2,"a") as f_out:
        dico_Rank_final = {}                
        valeurs_ami = []
        dicoNF2AMI = {}
        dicoNF2nbcooc = {}        
        #========= obtention de dico NF -> nb cooc  et NF -> AMI ======
        lignes2 = f_ami.readlines()
        for l in lignes2[1:]:
            infos = l.split("\t")
            lemmes = []   
            dic = ast.literal_eval(infos[0])
            for k in dic :
                lemmes += [k.split("=")[0]]      
                nf = ";".join(sorted(lemmes))
            nb_coocc = infos[1]
            val_ami = infos[3].split("\n")[0]
            if "n_a" not in str(val_ami):        
                if float(val_ami) > 0 and int(nb_coocc) >= 10: 
                    dicoNF2AMI[nf] = val_ami     
                    dicoNF2nbcooc[nf] = nb_coocc 
        #======= ajout de la mesure R3 qui se base sur AMI dans fichier =====
        lignes = f_cand.readlines()
        f_out.write("seen in Train?\tNumPhrase\tTokens cand\tNF cand\tOrdre Verb-Noun\tAction\tAction complémentaire\tAnnotation proposée\tDistLin\tDeps\tP(distLin) pour IRV ; P(distLin)x P(Deps) pour LVC/VID \tRanking R1\tRanking R2\tRanking combiné R3\tAnnotation Gold\tChoix final\n")  
        for l in lignes[1:] : 
            infos = l.split("\t")  
            infos_new = infos[0:len(infos) -3]
            derniere_cell = [infos[len(infos)-1].split("\n")[0]]          
            action = infos[5]
            if action == "faut_ranker" :
                numP = infos[1]
                toks = infos[2]
                id_cand = str(numP) + ";" + str(toks)
                nf = infos[3]
                if nf in dicoNF2AMI :
                    ami = dicoNF2AMI[nf]
                    nb_cooc = dicoNF2nbcooc[nf]
                else : 
                    ami = "n_a"
                    nb_cooc = "n_a"                
                rank_R1 = infos[len(infos) -4]
                if "n_a" in str(ami) or "n_a" in str(rank_R1) or "n_a" in str(nb_cooc) :
                    rank_R2 = "n_a"
                    rank_R3 = "n_a"
                    f_out.write("\t".join(infos_new + ["n_a","n_a"] + derniere_cell + ["\t\n"]))
                elif float(ami) <= 0 or int(nb_cooc) < 10  :
                    rank_R2 = "n_a"
                    rank_R3 = "n_a"
                    f_out.write("\t".join(infos_new + ["n_a","n_a"] + derniere_cell + ["\t\n"]))
                else :                         
                    rank_R2 =  int(all_val_ami.index(float(ami))) + 1 # car indice démarre à zéro 
                    rank_R3 =  1/int(rank_R1) + 1/int(rank_R2)
                    if rank_min_pour_select != "n_a" :
                        if rank_R3 >= rank_min_pour_select :
                            f_out.write("\t".join(infos_new + [str(rank_R2), str(rank_R3)] + derniere_cell + ["FINAL:faut_annoter\n"]))
                            
                        else :
                            f_out.write("\t".join(infos_new + ["n_a","n_a"] + derniere_cell + ["\t\n"]))
                    else :
                        f_out.write("\t".join(infos_new + ["n_a","n_a"] + derniere_cell + ["\t\n"]))
            elif "faut_annoter" in str(action) :
                f_out.write("\t".join(infos_new + ["n_a","n_a"] + derniere_cell + ["FINAL:faut_annoter\n"]))
            else :
                f_out.write("\t".join(infos_new + [str(rank_R2), str(rank_R3)] + derniere_cell + ["\t\n"]))
    with open (os.path.join(repParent,"OUTPUT_unseen","CANDIDATES",str(langue),str(langue) + "_2020-07-02_intersection_l1_l2_l3_FR_l5.tsv"),"a") as f_out :
        f_out.write("Nb cands conservés après Ranking Combiné = " + str(nb_cand_conserves) + "\n")
        
    #===============================================================================================
    #            Annotation du TEST
    #     utilise le fichier "_test_extraction_V-N_IRVoption_FINAL.tsv"
    #   ET annote seulement les candidats dont le R3 permet d'annoter le nb adéquat de cands
    #===============================================================================================

    annot_unseen(langue, "test",valeurs_rank_Rfinal_utiles)

   
