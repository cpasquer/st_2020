#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys
import ast
import shutil
import pickle
import unicodedata as ud



search_verb = True
search_noun = False


if search_verb :
   pos = "V"

elif search_noun :
   pos = "Nc"


with open ("RESSOURCES/GLAFF-1.2.2/glaff-1.2.2.txt","r") as f_glaff, open("2020-05-15_traductions_vb_causatifs_all_langues_brut.tsv","r") as f_trads, open("2020-05-15_traductions_vb_causatifs_all_langues_brut_verifGlaff.tsv","a") as f_tradsverif:
    lignes_trad = f_trads.readlines()
    lignes_glaff = f_glaff.readlines()
    mots_traites = []
    mots_traduits = []
    compt = 1
    for l in lignes_trad[1:]:        
        trad = l.split("\t")[0].lower()  
        if "|" in str(trad) :
            trad = trad.split("|")[0]
        if len(l.split("\t")) > 1 :
            freq = l.split("\t")[1]
        mots_traduits.append(trad)   
        print(compt)
        compt += 1  
        for l2 in lignes_glaff :   #Açoriennes|Ncfp|Açorienne|a
            l2 = ud.normalize('NFC',l2) # normalisation car sinon non prise en compte ligatures
            if trad.lower() not in mots_traites :                
                if str(trad).lower().startswith("le "):
                    trad = trad.split("le ")[1]
                elif str(trad).lower().startswith("les "):
                    trad = trad.split("les ")[1]
                elif str(trad).lower().startswith("la "):
                    trad = trad.split("la ")[1]
                elif str(trad).lower().startswith("l'"):
                    trad = trad.split("l'")[1]
                if "|" + str(pos) in str(l2) and "|" + str(trad.lower()) + "|" in str(l2).lower():
				            # ie correspondance forme lemmatisée
                    a_ecrire = str(trad.lower()) +  "|lemme=" + str(trad.lower()) + "\t" + str(freq)
                    f_tradsverif.write(a_ecrire)
                    mots_traites.append(trad.lower())   
        for l2 in lignes_glaff :   #Açoriennes|Ncfp|Açorienne|a
            l2 = ud.normalize('NFC',l2) # normalisation car sinon non prise en compte ligatures
            if str(l2).lower().startswith(trad.lower()+ "|" + str(pos.lower()))  :
                lemme = l2.split("|")[2].lower()
                a_ecrire = str(trad.lower()) + "|lemme=" + str(lemme) +  "\t" + str(freq)
                # ie correspondance forme non lemmatisée (e.g. condoléances)
                f_tradsverif.write(a_ecrire)
                mots_traites.append(trad.lower())
               
    
