#!/usr/bin/python3
# -*- coding: utf-8 -*-


#==========================================
#             IMPORTS
#==========================================
import os
import sys
import time
import progressbar
import subprocess
import numpy as np
from collections import OrderedDict
from collections import Counter 
from tqdm import tqdm
from shutil import copyfile
import configparser
import ast
from threading import Thread
import math 
import shutil
import pickle
#-----------------------------
from util_utilitaires import *
from util_findPOScolumn import *
from util_cupt2Blind import *
from util_cupt2typo_POS_dep_morpho import * 
from util_distSyn import * 


             
#==========================================
#      FONCTIONS 
#==========================================
def choix_POSnormUnique_perNF(NF2POSnorm, freqPOSnorminTrain, NF2POSandLemme) : 
       obtention = ""
       freqPOSnormconservee = {}
       NF2POSnormUnique = {}
       for NF in NF2POSnorm :     
              listePOSnormFreqMAX = []
              POSnormConservee = ""
              if len(NF2POSnorm[NF]) == 1 :
                     """ 1 seule POSnorm pour cette NF => choix de cette POSnorm"""
                     POSnormConservee = getList(NF2POSnorm[NF])[0]
                     obtention = "POSnorm unique pour cette NF"                       
              else :
                     """plusieurs POSnorm pour cette NF"""      
                     listePOSnormFreqMAX = getList_KeysWithMaxValue(NF2POSnorm[NF])
                     if len(listePOSnormFreqMAX) == 1 :
                            obtention = "POSnorm de freq max pour cette NF"
                            POSnormConservee = listePOSnormFreqMAX[0]
                     else :
                            freqPOSnormNFdonnee = {}
                            for POSnorm in listePOSnormFreqMAX : 
                                   freqPOSnormNFdonnee[POSnorm] = freqPOSnorminTrain[POSnorm]
                            listePOSnormConservees = getList_KeysWithMaxValue(freqPOSnormNFdonnee)
                            POSnormConservee = sorted(listePOSnormConservees)[0] 
                            if len(listePOSnormConservees) > 1 :
                                   obtention = "POSnorm de freq max dans Train" #si plusieurs POSnorm de freq identique pour cette NF
                            else :
                                   obtention = "1ere POSnorm dans ordre lexico" #si plusieurs POSnorm de freq identique pour cette NF et de meme frequence dans Train => la premiere par ordre lexicographique
                 
              if POSnormConservee not in freqPOSnormconservee :
                     freqPOSnormconservee[POSnormConservee] = 1
              else :
                     freqPOSnormconservee[POSnormConservee] += 1
              NF2POSnormUnique[NF] = {}
              NF2POSnormUnique[NF][POSnormConservee] = (NF2POSandLemme[NF][POSnormConservee],obtention) 
       return NF2POSnormUnique, freqPOSnormconservee

def calcul_FreqPOSnorm_Train(POSnorm,POSseq,POSnorm2POSseq,freqPOSnorminTrain) :
       if POSnorm not in POSnorm2POSseq :
              POSnorm2POSseq[POSnorm] = {}
              POSnorm2POSseq[POSnorm][POSseq] = 1
       else :
              if POSseq not in POSnorm2POSseq[POSnorm] :
                     POSnorm2POSseq[POSnorm][POSseq] = 1
              else :
                     POSnorm2POSseq[POSnorm][POSseq] += 1
       if POSnorm in freqPOSnorminTrain :
              freqPOSnorminTrain[POSnorm] += 1
       else :
              freqPOSnorminTrain[POSnorm] = 1 
       return freqPOSnorminTrain,POSnorm2POSseq

def lemme_pos_2tuplePOSlemme(lemmes,pos):
       """
       input = "se|faire|remarquer"      et       "PRON|AUX|VERB"
       OUTPUT2 = [(PRON,'se'),(AUX,'faire'....]
       """
       l_out = []
       for elt in range(0,len(lemmes)):
              l_out.append((pos[elt],lemmes[elt]))
       return l_out

def synthese_Infos_perNF(NF, info,dico) :
       if NF not in dico : 
              dico[NF] = {}
              dico[NF][info] = 1
       else :
              if info in dico[NF] :
                     dico[NF][info] += 1 
              else :
                     dico[NF][info] = 1 
       return dico

def choix_CatUnique_perNF(NF2categ) :
       NF2categUnique = {}
       for NF in NF2categ : #{'LVC.full': 3, 'VID': 1}
              newNF = ""
              if len(NF2categ[NF]) > 1 :
                     allcatFreqMax = getList_KeysWithMaxValue(NF2categ[NF])   
                     if len(allcatFreqMax) == 1 :
                            # choix de la catégorie la plus fréquente pour cette NF
                            NF2categUnique[NF] = allcatFreqMax[0]
                     else :
                            # si catégories exaequo => choix arbitraire de la première par ordre lexicographique
                            NF2categUnique[NF] = sorted(allcatFreqMax)[0]  
              else : 
                     
                     NF2categUnique[NF] = list(NF2categ[NF].keys())[0]
       return NF2categUnique

def extractInfosDiscont(compt_mwes,col6_positionEltsMwe, lignes, nbMaxInsert, freqInsertPerCat) : 
       nbInsertperMWE = {}
       catMWEs = {}
       for MWE in col6_positionEltsMwe :           
              nbInsertperMWE[MWE] = -1
              catMWE = ""
              #---- Obtention de la catégorie MWE + calcul nb Insertions ---------
              idMWE = "MWE" + str(MWE)
              positionEltsMwe = col6_positionEltsMwe[MWE].split("|")  #['1', '2', '3', '4']
              positionDebut = positionEltsMwe[0]
              positionFin = positionEltsMwe[len(positionEltsMwe)-1]
              nbInsertions = 0    
              for ligne in lignes :                           
                     infos = ligne.split("\t")                       
                     if len(infos) > 10 and "-" not in infos[0]:  # e.g. amalgames 7-8	du    
                            # Infos insertions
                            for elt in range(int(positionDebut),int(positionFin) + 1) : 
                                   elt_lex = False 
                                   for num_eltLex in positionEltsMwe :	#['1', '2', '3', '4']
                                          if int(elt) == int(num_eltLex) :	
                                                 elt_lex = True     
                                   if int(elt) == int(infos[0]) : 
                                          if not elt_lex : 
                                                 nbInsertions += 1  
                                          else :                                                                      
                                                 #---- catégorie MWE ----  
                                                 if ";" not in infos[10] and ":" in infos[10] :
                                                        idMWE2 = "MWE" + str(infos[10].split(":")[0])
                                                        if idMWE2 not in catMWEs :
                                                               catMWEs[idMWE2] = infos[10].split(":")[1]                                                               
                                                 elif  ";" in infos[10] :                             
                                                        alltypes = infos[10].split(";")
                                                        for typeMWE in alltypes :
                                                               if ":" in typeMWE :                                                
                                                                      idMWE2 = "MWE" + str(typeMWE.split(":")[0])
                                                                      catMWEs[idMWE2] = typeMWE.split(":")[1]   
              

              if idMWE in catMWEs :
                     catMWE = catMWEs[idMWE]        
                     if catMWE not in freqInsertPerCat :
                            freqInsertPerCat[catMWE] = {}
                            freqInsertPerCat[catMWE][nbInsertions] = 1
                     else :
                            if nbInsertions not in freqInsertPerCat[catMWE] :
                                   freqInsertPerCat[catMWE][nbInsertions] = 1
                            else :
                                   freqInsertPerCat[catMWE][nbInsertions] += 1
                     nbInsertperMWE[int(MWE)-1] = nbInsertions
                     if nbInsertions > nbMaxInsert :
                            nbMaxInsert = nbInsertions 
              else :
                     num_mwe = idMWE.split("MWE")[1]
                     for ligne in lignes :                           
                            infos = ligne.split("\t")                       
                            if len(infos) > 10 :
                                   if ";" not in infos[10] and ":" in infos[10] :
                                          
                                          idMWE_encours = infos[10].split(":")[0]
                                          if num_mwe ==  idMWE_encours :
                                                 catMWEs[idMWE] = infos[10].split(":")[1]                                                               
                                   elif  ";" in infos[10] :                               
                                          alltypes = infos[10].split(";")
                                          for typeMWE in alltypes :
                                                 if ":" in typeMWE :                                                
                                                        idMWE_encours = typeMWE.split(":")[0]
                                                        if num_mwe ==  idMWE_encours :
                                                               catMWEs[idMWE] = typeMWE.split(":")[1]   
                     catMWE = catMWEs[idMWE] 
                     if catMWE not in freqInsertPerCat :
                            freqInsertPerCat[catMWE] = {}
                            freqInsertPerCat[catMWE][nbInsertions] = 1
                     else :
                            if nbInsertions not in freqInsertPerCat[catMWE] :
                                   freqInsertPerCat[catMWE][nbInsertions] = 1
                            else :
                                   freqInsertPerCat[catMWE][nbInsertions] += 1
                     nbInsertperMWE[int(MWE)-1] = nbInsertions
                     if nbInsertions > nbMaxInsert :
                            nbMaxInsert = nbInsertions 
                     compt_mwes += 1
       
       return catMWEs,nbMaxInsert, freqInsertPerCat,nbInsertperMWE,compt_mwes

def maxInsertion_parCat_horsHapax(freqInsertPerCat):
       """
       OUTPUT2 pour FR : {'VID': 6, 'LVC.full': 15, 'IRV': 3, 'LVC.cause': 6, 'MVC': 0}
       """
       nbMaxInsertPercat = {}
       for cat in freqInsertPerCat :
              nbMaxInsertPercat[cat] = 0
              for nbInsert in freqInsertPerCat[cat] :
                     if nbInsert > nbMaxInsertPercat[cat] and freqInsertPerCat[cat][nbInsert] > 1 :  
                            nbMaxInsertPercat[cat] = nbInsert   
       return nbMaxInsertPercat

def correctionIRV_FR(cat, lemmesEltLex) :
       new_lemmesEltLex = []
       for l in range(0,len(lemmesEltLex)) :
              if cat == "IRV" and lemmesEltLex[l] in ["me","te","le","lui","nous","vous"]:
                     newl = "se"
                     new_lemmesEltLex.append(newl)                            
              elif lemmesEltLex[l] in ["me","te","nous","vous"]:
                     newl = "se"
                     new_lemmesEltLex.append(newl)                            
              else :
                     newl = lemmesEltLex[l].lower()
                     new_lemmesEltLex.append(newl)
       NF = ";".join(sorted(new_lemmesEltLex))
       return [NF]

def correctionIRV_IT(cat, lemmesEltLex) :
       new_lemmesEltLex = []
       NFs = []
       for l in range(0,len(lemmesEltLex)) :
              if cat == "IRV" and lemmesEltLex[l] in ["ci","m'","me","mi","vi","s'","te","ti"]:
                     newl = "si"
                     new_lemmesEltLex.append(newl)                          
              else :
                     newl = lemmesEltLex[l].lower()
                     new_lemmesEltLex.append(newl)
       NF = ";".join(sorted(new_lemmesEltLex))
       NFs.append(NF)
       for l in range(0,len(lemmesEltLex)) :
              if cat == "IRV" and lemmesEltLex[l] in ["ci"]:
                     newl = "ci"
                     new_lemmesEltLex.append(newl)    
              else :
                     newl = lemmesEltLex[l].lower()
                     new_lemmesEltLex.append(newl)
       NF = ";".join(sorted(new_lemmesEltLex))
       NFs.append(NF)       
       return NFs
     
def calcul_DistanceSyntaxique(col9_EltLexPOS,col6_positionEltsMwe, phrase) : 
       col21_distSyn = {}
       col22_typeDistSyn = {}
       col23_distSyn2elts = {}            
       col24_typeDistSyn2elts = {}                             
       for MWE in range(0,len(col9_EltLexPOS)) :
              #--------------- init ---------------
              col21_distSyn[MWE] = -1
              col22_typeDistSyn[MWE] = ""   
              col23_distSyn2elts[MWE] = -1
              col24_typeDistSyn2elts[MWE] = ""                                     
              verifDistSyn = False
              eltLex_liste = col9_EltLexPOS[MWE][1].split("|")
              positionEltLex_liste = col6_positionEltsMwe[MWE][1].split("|")                            
              #--------------- calcul distance N-V ----------------
              comptVERB = 0
              comptNOUN = 0 
              for elt in eltLex_liste :
                     if elt == "VERB" :
                            comptVERB += 1
                     elif elt == "NOUN" :
                            comptNOUN += 1
              if comptNOUN == 1 and  comptVERB == 1 :
                     verifDistSyn = True
              if verifDistSyn :
                     for elt in range(0,len(eltLex_liste)) :
                            if eltLex_liste[elt] == "VERB" :
                                   positionVERB = int(positionEltLex_liste[elt])
                            elif eltLex_liste[elt] == "NOUN" :
                                   positionNOUN = int(positionEltLex_liste[elt])  
                     col21_distSyn[MWE],col22_typeDistSyn[MWE] = distanceSyntaxique(positionVERB,positionNOUN,phrase)                            
              #-------------- calcul distance MWE à 2 composants --------------
              if len(positionEltLex_liste) == 2 :                    
                     positionElt1 = int(positionEltLex_liste[0])
                     positionElt2 = int(positionEltLex_liste[1])  
                     col23_distSyn2elts[MWE],col24_typeDistSyn2elts[MWE] = distanceSyntaxique(positionElt1,positionElt2,phrase)   
       return col21_distSyn, col22_typeDistSyn, col23_distSyn2elts , col24_typeDistSyn2elts
  
def ordreVerbesMultiples(phrase,NF,MWE_lemmes,MWE_POS,NF2OrdreVerbes) :
       
       for MWE in MWE_POS :
              stats_POS = Counter(MWE_POS.split("|"))   
              nbverbes = stats_POS["VERB"] + stats_POS["AUX"]
              if nbverbes > 1 :                     
                     liste_POS = MWE_POS.split("|")
                     liste_lemmes = MWE_lemmes.split("|")
                     lemme_POS = lemme_pos_2tuplePOSlemme(liste_POS,liste_lemmes)
                     ordre = []
                     for elt in range(0,len(lemme_POS)) :
                            if lemme_POS[elt][1] in ["VERB","AUX"] :
                                   ordre.append(lemme_POS[elt][0]) 
                     if NF not in NF2OrdreVerbes :
                            NF2OrdreVerbes[NF] = ["|".join(ordre)]
                     else :
                            if "|".join(ordre) not in NF2OrdreVerbes[NF] : 
                                   NF2OrdreVerbes[NF] +=  ["|".join(ordre)]
       return NF2OrdreVerbes

def NEW_infosSup_Train(langue,pathsCorpus,colonnePOS, debug, dicoMorpho,listePOS,listeRelDep):  
       # Initialisations
       stats_morpho = {}
       dicoMWE = {}
       freqPOSnorminTrain = {}
       POSnorm2POSseq = {}
       NF2POSseq = {}
       NF2POSnorm = {}
       NF2categ = {}
       NF2flexion = {}
       NF2POSandLemme = {}
       topX_POSnorm = {}
       freqInsertPerCat = {}
       dicoNumPhrase2idPhrase = {}
       dicoFreqNF_inTrain = {}
       nbInsertperNF = {}
       nbInsertperMWE = {}
       NF2OrdreVerbes = {}
       stats_insertions = {}
       dico_ImbricationsMWEs = {}
       comptPhrase = 1
       nbMaxInsert = 0
       pronoms_VID_patron_PRON_PRON_VERB = {}
       dico_seqPOS_perCat_POSnorm = {}
       compt_mwes = 0
       dico_NF2_case_morphoNoun = {}
       #===================================================================
       # extract infos in train for annotated MWEs 
       #===================================================================       
       with open (pathsCorpus[0],"r") as f_cupt :
              infosCupt =  f_cupt.readlines()	
              corpusCupt = "".join(infosCupt[1:])					
              phrases = corpusCupt.split("\n\n")[:-1]
              comptPhrase = 1
              for phrase in phrases : 
                     mwe_in_phrase = []
                     col6_positionEltsMwe = {}
                     col7_EltLexFlex = {}						
                     col8_EltLexLemme = {}
                     col9_EltLexPOS = {}
                     col14_nbInsert =  {}
                     lignes = phrase.split("\n")
                     for ligne in lignes :
                            if "# source_sent_id" in ligne :
                                   sourcePhrase = ligne.split("# source_sent_id = ")[1]
                                   idPhrase = comptPhrase
                            elif ligne[0] != "#" :              
                                   infos = ligne.split("\t")      
                                   # ce token fait-il partie de une ou plusieurs MWE => extarction du numéro de MWE + catégorie
                                   idMWEs = []   
                                   typeMWEs = []
                                   if infos[10] not in ["*","_"]  :
                                          if "," not in infos[1] :
                                                 eltLex = True
                                                 #  Overlap => 1:VID;2:IAV   /  1;2      vs      pas d'overlap =>  1:VID /  1            
                                                 if ";" not in infos[10] and ":"  not in infos[10] :                                    
                                                        idMWEs.append(infos[10])
                                                 elif ";" not in infos[10] :
                                                        idMWEs.append(infos[10].split(":")[0])
                                                        typeMWEs.append(infos[10].split(":")[1])
                                                 elif ":" not in infos[10] :
                                                        idMWEs = infos[10].split(";")
                                                 else :                                    
                                                        temp_idMWEs = infos[10].split(";")
                                                        for temp in temp_idMWEs :
                                                               idMWEs.append(temp.split(":")[0])
                                                               typeMWEs.append(infos[10].split(":")[1])
                                          else :
                                                 eltLex = False 
                                   else:
                                          eltLex = False      
                                   # caractéristiques de l'Elt Lex   
                                   if eltLex and infos[colonnePOS] != "PUNCT" :  

                                          for idMWE in idMWEs :  
                                                 if "|" in infos[2] and langue == "DE":
                                                     new_infos = infos[2].replace("|","-")
                                                 else: 
                                                        if "_" in str(infos[2]) :
                                                               new_infos = infos[1] # si absence de lemme, on prend la forme fléchie
                                                        else :
                                                               new_infos = infos[2]
                                                 newId = "MWE" + str(idMWE)
                                                 if idMWE not in col6_positionEltsMwe :
                                                        col6_positionEltsMwe[idMWE] = infos[0]
                                                        col7_EltLexFlex[newId] = infos[1].lower()
                                                        col8_EltLexLemme[newId] = new_infos.lower()
                                                        col9_EltLexPOS[newId] = infos[colonnePOS]  
                                                 else :
                                                        col6_positionEltsMwe[idMWE] = col6_positionEltsMwe[idMWE] + "|" + infos[0] 
                                                        col7_EltLexFlex[newId] = col7_EltLexFlex[newId] + "|" +infos[1].lower()
                                                        col8_EltLexLemme[newId] =  col8_EltLexLemme[newId] + "|" +new_infos.lower()
                                                        col9_EltLexPOS[newId] = col9_EltLexPOS[newId] + "|" + infos[colonnePOS] 
                     comptPhrase += 1
                     # ================================================================================
                     #	Extraction des discontinuités pour chaque MWE d'une phrase donnée
                     # ================================================================================  
                     col4_catMWE,nbMaxInsert,stats_insertions,nbInsertperMWE, compt_mwes = extractInfosDiscont(compt_mwes,col6_positionEltsMwe, lignes, nbMaxInsert, stats_insertions)
                    
                     #---------- tri par ordre d'apparition des EP dans la phrase ------
                     col4_catMWE = sorted(col4_catMWE.items(), key=lambda t: t[0])    
                     col6_positionEltsMwe = sorted(col6_positionEltsMwe.items(), key=lambda t: t[0])     #{'1': '2|3', '2': '10|12'}
                     col7_EltLexFlex = sorted(col7_EltLexFlex.items(), key=lambda t: t[0])               #{'1': 'called|in', '2': 'pick|up'} 
                     col8_EltLexLemme = sorted(col8_EltLexLemme.items(), key=lambda t: t[0])             #{'1': 'call|in', '2': 'pick|up'}
                     col9_EltLexPOS = sorted(col9_EltLexPOS.items(), key=lambda t: t[0])                 #{'1': 'VERB|ADP', '2': 'VERB|ADP'}                             
                     col14_nbInsert = sorted(col14_nbInsert.items(), key=lambda t: t[0])                 #[('1', 2), ('2', 3)]
                     
                     
                     #=======================================================================================
                     #   séquences de POS autorisées par POSnorm + morpho LVC/VID
                     #=======================================================================================
                     for mwe in range(0,len(col6_positionEltsMwe)) :
                            compt_mwe = col6_positionEltsMwe[mwe][0]
                            str_mwe = "MWE" + str(compt_mwe)
                            cat_MWE = col4_catMWE[mwe][1]

                            tokens_MWE = listeStr2int(col6_positionEltsMwe[mwe][1].split("|"))  
                            lemmes_MWE = col8_EltLexLemme[mwe][1].split("|")
                            nf_MWE = ";".join(sorted(lemmes_MWE))
                            POSnorm_MWE =  ";".join(sorted(col9_EltLexPOS[mwe][1].split("|"))) 
                            #--------------------------------------------
                            pos_MWE = col9_EltLexPOS[mwe][1].split("|")
                            nb_verbes_inMWE = pos_MWE.count("VERB") + pos_MWE.count("V")
                            nb_noun_inMWE = pos_MWE.count("NOUN") + pos_MWE.count("S")
                            verbe_mwe_lemme = ""
                            nom_mwe_morpho = ""
                            if nb_verbes_inMWE == 1 and nb_noun_inMWE == 1 and cat_MWE in ["VID","LVC.full","LVC.cause"]:
                                   for elt in range(0,len(pos_MWE)) :
                                          if pos_MWE[elt] in ["VERB","V"] :
                                                 verbe_mwe_lemme = lemmes_MWE[elt]
                                          elif pos_MWE[elt] in ["NOUN","S"] :
                                                 tok_elt = tokens_MWE[elt]
                                                 for ligne in lignes :
                                                        if ligne[0] != "#" :              
                                                               infos = ligne.split("\t")     
                                                               if len(infos) > 4 :
                                                                      numTok = infos[0]
                                                                      if "-" not in numTok :
                                                                             pos = infos[3]    
                                                                             if int(numTok) == tok_elt :
                                                                                    nom_mwe_morpho = infos[5] 
                                   infos_morpho = nom_mwe_morpho.split("|")  #Gender=Fem|Number=Sing
                                   valeur_morpho_case = ""
                                   valeur_morpho_number = ""
                                   for i in infos_morpho :
                                          trait = i.split("=")[0]
                                          if trait == "Case" :
                                                 valeur_morpho_case = i.split("=")[1]
                                          elif trait == "Number" or trait == "num" :
                                                 valeur_morpho_number = i.split("=")[1]
                                   
                                   if nf_MWE not in dico_NF2_case_morphoNoun :
                                          dico_NF2_case_morphoNoun[nf_MWE] = {}
                                          dico_NF2_case_morphoNoun[nf_MWE]["numberNoun"] = [valeur_morpho_number]
                                          dico_NF2_case_morphoNoun[nf_MWE]["caseNoun"] = [valeur_morpho_case]
                                   else :
                                          if valeur_morpho_number not in  dico_NF2_case_morphoNoun[nf_MWE]["numberNoun"] :
                                                 dico_NF2_case_morphoNoun[nf_MWE]["numberNoun"] += [valeur_morpho_number]
                                          if valeur_morpho_case not in  dico_NF2_case_morphoNoun[nf_MWE]["caseNoun"] :
                                                 dico_NF2_case_morphoNoun[nf_MWE]["caseNoun"] += [valeur_morpho_case]
                            #--------------------------------------------
                            pos_seq = []
                            for t in range(min(tokens_MWE),max(tokens_MWE) + 1) :
                                   for ligne in lignes :
                                          if ligne[0] != "#" :              
                                                 infos = ligne.split("\t")     
                                                 if len(infos) > 4 :
                                                        numTok = infos[0]
                                                        if "-" not in numTok :
                                                               pos = infos[3]    
                                                               if int(numTok) == t :
                                                                      pos_seq.append(pos) 
                            pos_seq_str = "|".join(pos_seq)
                            if cat_MWE  not in dico_seqPOS_perCat_POSnorm :
                                   dico_seqPOS_perCat_POSnorm[cat_MWE] = {}
                                   if POSnorm_MWE not in dico_seqPOS_perCat_POSnorm[cat_MWE] :
                                          dico_seqPOS_perCat_POSnorm[cat_MWE][POSnorm_MWE] = []
                                          dico_seqPOS_perCat_POSnorm[cat_MWE][POSnorm_MWE] = [pos_seq_str]
                                   else :
                                          if pos_seq_str not in dico_seqPOS_perCat_POSnorm[cat_MWE][POSnorm_MWE] :
                                                 dico_seqPOS_perCat_POSnorm[cat_MWE][POSnorm_MWE] += [pos_seq_str]
                            else :
                                   if POSnorm_MWE not in dico_seqPOS_perCat_POSnorm[cat_MWE] :
                                          dico_seqPOS_perCat_POSnorm[cat_MWE][POSnorm_MWE] = []
                                          dico_seqPOS_perCat_POSnorm[cat_MWE][POSnorm_MWE] = [pos_seq_str]
                                   else :
                                          if pos_seq_str not in dico_seqPOS_perCat_POSnorm[cat_MWE][POSnorm_MWE] :
                                                 dico_seqPOS_perCat_POSnorm[cat_MWE][POSnorm_MWE] += [pos_seq_str] 
                     #============================================================
                     #     distance syntaxique 
                     #============================================================ 
                     col21_distSyn = {}
                     col22_typeDistSyn = {}
                     col23_distSyn2elts = {}            
                     col24_typeDistSyn2elts = {}                     
                     col21_distSyn, col22_typeDistSyn, col23_distSyn2elts, col24_typeDistSyn2elts = calcul_DistanceSyntaxique(col9_EltLexPOS,col6_positionEltsMwe, phrase)
                     
                     # =========================================================
                     #	OUTPUT2 (dans Fichier TSV si option debug)
                     # ========================================================
                     for MWE in range(0,len(col4_catMWE)): 
                            idMWE = col4_catMWE[MWE][0] 
                            col4 = col4_catMWE[MWE][1]             #VID          
                            col7 = col7_EltLexFlex[MWE][1]          #A|little|birdie|told
                            col8 = col8_EltLexLemme[MWE][1]         #a|little|birdie|tell    
                            tok_annot = sorted(listeStr_2int(col6_positionEltsMwe[MWE][1].split("|")))
                            
                            lemmesEltLex = col8.split("|")      
                            NF = ";".join(sorted(lemmesEltLex))
                            POSseq = col9_EltLexPOS[MWE][1]           
                            POSnorm = ";".join(sorted(POSseq.split("|")))  
                            # suppression cas possibles d'erreurs d'annotations : MWE à un composant non étiquetées comme "AUX/VERB"
                            erreur_annotation = False
                            if len(lemmesEltLex) == 1 and POSseq not in ["AUX","VERB"] :
                                   erreur_annotation = True                            
                            
                            if not erreur_annotation :   
                                   #==================================
                                   # cherche MWE imbriquées
                                   #==================================
                                   mwe_in_phrase.append((NF,tok_annot))                                      
                                   #==================================
                                   if comptPhrase not in dicoMWE :
                                          dicoMWE[comptPhrase] = {}
                                          if NF not in dicoMWE[comptPhrase] :
                                                 dicoMWE[comptPhrase][NF] = [col6_positionEltsMwe[MWE][1]]
                                          else :
                                                 dicoMWE[comptPhrase][NF] += [col6_positionEltsMwe[MWE][1]]    
                                   else :
                                          if NF not in dicoMWE[comptPhrase] :
                                                 dicoMWE[comptPhrase][NF] = [col6_positionEltsMwe[MWE][1]]
                                          else :
                                                 dicoMWE[comptPhrase][NF] += [col6_positionEltsMwe[MWE][1]]                                    
                                   
                                   #=======================================================================   
                                   # FR : Homogénéisation IRV avec pronoms lemmatisés différemment de SE
                                   #=======================================================================
                                   NFs = [NF]                              
                                   if langue == "FR" :
                                          NFs = correctionIRV_FR(col4,lemmesEltLex)  
                                   elif langue == "IT" :
                                          NFs = correctionIRV_IT(col4,lemmesEltLex)   
                                   
                                   for NF in NFs :
                                          NF2OrdreVerbes = ordreVerbesMultiples(phrase,NF,col8,POSseq,NF2OrdreVerbes)       
                                          #=================================================================   
                                          """ Fréquence des POSnorm dans TRAIN """
                                          freqPOSnorminTrain,POSnorm2POSseq = calcul_FreqPOSnorm_Train(POSnorm,POSseq,POSnorm2POSseq,freqPOSnorminTrain)
                                          """# Synthese par NF (= LemmNorm)  des :
                                          - Séquences de POS observées  
                                          - flexions observées 
                                          - POSnorm 
                                          - catégorie d'EP
                                          """
                                          NF2POSseq = synthese_Infos_perNF(NF,POSseq,NF2POSseq)      
                                          NF2POSnorm = synthese_Infos_perNF(NF,POSnorm,NF2POSnorm)  
                                          NF2flexion = synthese_Infos_perNF(NF,col7,NF2flexion)
                                          NF2categ = synthese_Infos_perNF(NF,col4,NF2categ)
                                          
                                          if NF not in dicoFreqNF_inTrain :
                                                 dicoFreqNF_inTrain[NF] = 1
                                          else :
                                                 dicoFreqNF_inTrain[NF] += 1
                                          
                                          #------------------------------------------
                                          # Nb d'insertions observées pour chaque NF
                                          n = nbInsertperMWE[MWE] 
                                          if NF not in nbInsertperNF:
                                                 nbInsertperNF[NF] = []                                   
                                                 nbInsertperNF[NF] = [n]
                                          else :
                                                 if n not in nbInsertperNF[NF] : 
                                                        nbInsertperNF[NF] += [n]                                   
                                                 
                                          #===============================================================================   
                                          # Associations lemme=POS pour chaque NF (= LemmNorm)  
                                          # NB : différentes POS pour un même lemme sont parfois observées e.g. (PART/PRON)
                                          # e.g. 'disputar;luta': {'disputar=VERB|luta=NOUN': 1}  
                                          #===============================================================================  
                                          if NF not in NF2POSandLemme :                                           
                                                 NF2POSandLemme[NF] = {}
                                                 NF2POSandLemme[NF][POSnorm] = tuple2string(list(zip(lemmesEltLex,POSseq.split("|"))))
                                          else :
                                                 if POSnorm not in NF2POSandLemme[NF] :
                                                        NF2POSandLemme[NF][POSnorm] = tuple2string(list(zip(lemmesEltLex,POSseq.split("|")))) 
                                                               
                            
                                          
                                          #========================================================
                                          #   Recherche VID impersonnels : il y a != elle y a
                                          #========================================================
                                          """
                                          pour FR cela donnerait ['il', 'cela', "s'", "l'", 'y', 'en', '-il', 'vous', 'ca', 'se', "t'", 'ça', "qu'"]
                                          """
                                          formeSurface_POS = list(zip(col7_EltLexFlex[MWE][1].split("|") ,POSseq.split("|")))
                                          if col4 == "VID" and POSnorm in ["PRON;VERB","PRON;PRON;VERB","AUX;PRON","AUX;PRON;PRON"] :
                                                 for elt in formeSurface_POS :
                                                        if elt[1] == "PRON" :
                                                               if elt[0]  not in pronoms_VID_patron_PRON_PRON_VERB :                                                        
                                                                      pronoms_VID_patron_PRON_PRON_VERB[elt[0]] = 1  
                                                               else:
                                                                      pronoms_VID_patron_PRON_PRON_VERB[elt[0]] += 1                                                                  
                                                             
                     #========================================
                     # recherche imbrications
                     #=========================================
                     
                     
                     all_tokens = []
                     all_NFs = []
                     tok2NF = {}
                     for mwe in range(0,len(mwe_in_phrase)) :
                            
                            all_tokens.append(mwe_in_phrase[mwe][1])
                            tok2NF["|".join(listeInt2Str(mwe_in_phrase[mwe][1]))] = mwe_in_phrase[mwe][0]
                            
                     
                     for tokMWE1 in all_tokens :
                            for tokMWE2 in all_tokens :
                                   if tokMWE1 != tokMWE2 :
                                          if len(recouvrement_listes([tokMWE1,tokMWE2])) == 1 : #ie listes avec recouvrement
                                                 MWE_imbriquante, MWE_imbriquee = renvoie_liste_plus_grande([tokMWE1,tokMWE2])
                                                 NFimbriquante = tok2NF[MWE_imbriquante]
                                                 NFimbriquee = tok2NF[MWE_imbriquee]
                                                 if NFimbriquante not in dico_ImbricationsMWEs :
                                                        dico_ImbricationsMWEs[NFimbriquante] = [NFimbriquee]
                                                 else :
                                                        if NFimbriquee not in dico_ImbricationsMWEs[NFimbriquante] :
                                                               dico_ImbricationsMWEs[NFimbriquante] = [NFimbriquee]                           
                     
                     
                     comptPhrase += 1
                     
              
              NF2categUnique = choix_CatUnique_perNF(NF2categ)   #Une seule catégorie par NF                     
              NF2POSnormUnique,freqPOSnormconservee = choix_POSnormUnique_perNF(NF2POSnorm, freqPOSnorminTrain, NF2POSandLemme) #Une seule POSnorm par NF

       info_insertionsMaxperCat = {}       
       info_insertionsMaxperCat = maxInsertion_parCat_horsHapax(stats_insertions)         
       return NF2OrdreVerbes, dico_NF2_case_morphoNoun,pronoms_VID_patron_PRON_PRON_VERB, NF2OrdreVerbes,info_insertionsMaxperCat,dico_ImbricationsMWEs, dico_seqPOS_perCat_POSnorm
        
def infos_Train2(langue,pathsCorpus,colonnePOS, topX, debug, dicoMorpho,listePOS,listeRelDep):  
       # Initialisations
       dicoMWE = {}
       freqPOSnorminTrain = {}
       POSnorm2POSseq = {}
       NF2POSseq = {}
       NF2POSnorm = {}
       NF2categ = {}
       NF2flexion = {}
       NF2POSandLemme = {}
       topX_POSnorm = {}
       nbMaxInsert = 0
       freqInsertPerCat = {}
       dicoNumPhrase2idPhrase = {}
       dicoFreqNF_inTrain = {}
       nbInsertperNF = {}
       nbInsertperMWE = {}
       comptPhrase = 1
       compt_mwes = 0
       #===================================================================
       # extract infos in train for annotated MWEs 
       #===================================================================       
       with open (pathsCorpus[0],"r") as f_cupt :
              infosCupt =  f_cupt.readlines()	
              corpusCupt = "".join(infosCupt[1:])					
              phrases = corpusCupt.split("\n\n")[:-1]
              comptPhrase = 1
              for phrase in phrases :                     
                     col6_positionEltsMwe = {}
                     col7_EltLexFlex = {}						
                     col8_EltLexLemme = {}
                     col9_EltLexPOS = {}
                     col14_nbInsert =  {}
                     lignes = phrase.split("\n")
                     for ligne in lignes :
                            if "# source_sent_id" in ligne :
                                   sourcePhrase = ligne.split("# source_sent_id = ")[1]
                                   idPhrase = comptPhrase 
                            elif ligne[0] != "#" :              
                                   infos = ligne.split("\t")              
                                   idMWEs = []   
                                   typeMWEs = []
                                   if infos[10] not in ["*","_"]  :
                                          if "," not in infos[1] :
                                                 eltLex = True
                                                 #  Overlap => 1:VID;2:IAV   /  1;2      vs      pas d'overlap =>  1:VID /  1            
                                                 if ";" not in infos[10] and ":"  not in infos[10] :                                    
                                                        idMWEs.append(infos[10])
                                                 elif ";" not in infos[10] :
                                                        idMWEs.append(infos[10].split(":")[0])
                                                        typeMWEs.append(infos[10].split(":")[1])
                                                 elif ":" not in infos[10] :
                                                        idMWEs = infos[10].split(";")
                                                 else :                                    
                                                        temp_idMWEs = infos[10].split(";")
                                                        for temp in temp_idMWEs :
                                                               idMWEs.append(temp.split(":")[0])
                                                               typeMWEs.append(infos[10].split(":")[1])
                                          else :
                                                 eltLex = False 
                                   else:
                                          eltLex = False      
                                   # caractéristiques de l'Elt Lex   
                                   if eltLex and infos[colonnePOS] != "PUNCT" :   
                                          for idMWE in idMWEs :  
                                                 newId = "MWE" + str(idMWE)
                                                 if "|" in infos[2] and langue == "DE":
                                                     new_infos = infos[2].replace("|","-")
                                                 else: 
                                                        if "_" in str(infos[2]) :
                                                               new_infos = infos[1]
                                                        else : 
                                                               new_infos = infos[2]
                                                 if idMWE not in col6_positionEltsMwe :
                                                        col6_positionEltsMwe[idMWE] = infos[0]
                                                        col7_EltLexFlex[newId] = infos[1].lower()
                                                        col8_EltLexLemme[newId] = new_infos.lower()
                                                        col9_EltLexPOS[newId] = infos[colonnePOS]  
                                                 else :
                                                        col6_positionEltsMwe[idMWE] = col6_positionEltsMwe[idMWE] + "|" + infos[0] 
                                                        col7_EltLexFlex[newId] = col7_EltLexFlex[newId] + "|" + infos[1].lower()
                                                        col8_EltLexLemme[newId] = col8_EltLexLemme[newId] + "|" + new_infos.lower()
                                                        col9_EltLexPOS[newId] = col9_EltLexPOS[newId] + "|" + infos[colonnePOS] 
                     comptPhrase += 1
                     # ================================================================================
                     #	Extraction des discontinuités pour chaque MWE d'une phrase donnée
                     # ================================================================================  
                     col4_catMWE,nbMaxInsert, freqInsertPerCat,nbInsertperMWE,compt_mwes = extractInfosDiscont(compt_mwes,col6_positionEltsMwe, lignes, nbMaxInsert, freqInsertPerCat)
                     #---------- tri par ordre d'apparition des EP dans la phrase ------
                     col4_catMWE = sorted(col4_catMWE.items(), key=lambda t: t[0])    
                     col6_positionEltsMwe = sorted(col6_positionEltsMwe.items(), key=lambda t: t[0])     #{'1': '2|3', '2': '10|12'}
                     col7_EltLexFlex = sorted(col7_EltLexFlex.items(), key=lambda t: t[0])               #{'1': 'called|in', '2': 'pick|up'} 
                     col8_EltLexLemme = sorted(col8_EltLexLemme.items(), key=lambda t: t[0])             #{'1': 'call|in', '2': 'pick|up'}
                     col9_EltLexPOS = sorted(col9_EltLexPOS.items(), key=lambda t: t[0])                 #{'1': 'VERB|ADP', '2': 'VERB|ADP'}                             
                     col14_nbInsert = sorted(col14_nbInsert.items(), key=lambda t: t[0])                 #[('1', 2), ('2', 3)]
                     
                    
                     #============================================================
                     #     distance syntaxique 
                     #============================================================ 
                     col21_distSyn = {}
                     col22_typeDistSyn = {}
                     col23_distSyn2elts = {}            
                     col24_typeDistSyn2elts = {}                     
                     col21_distSyn, col22_typeDistSyn, col23_distSyn2elts, col24_typeDistSyn2elts = calcul_DistanceSyntaxique(col9_EltLexPOS,col6_positionEltsMwe, phrase)
                     
                     # =========================================================
                     #	OUTPUT2 (dans Fichier TSV si option debug)
                     # ========================================================
                     for MWE in range(0,len(col4_catMWE)): 
                            idMWE = col4_catMWE[MWE][0] 
                            col4 = col4_catMWE[MWE][1]             #VID          
                            col7 = col7_EltLexFlex[MWE][1]          #A|little|birdie|told
                            col8 = col8_EltLexLemme[MWE][1]         #a|little|birdie|tell                            
                            lemmesEltLex = col8.split("|")      
                            NF = ";".join(sorted(lemmesEltLex))
                            POSseq = col9_EltLexPOS[MWE][1]           #DET|ADJ|NOUN|VERB
                            POSnorm = ";".join(sorted(POSseq.split("|")))  
                            
                            if comptPhrase not in dicoMWE :
                                   dicoMWE[comptPhrase] = {}
                                   if NF not in dicoMWE[comptPhrase] :
                                          dicoMWE[comptPhrase][NF] = [col6_positionEltsMwe[MWE][1]]
                                   else :
                                          dicoMWE[comptPhrase][NF] += [col6_positionEltsMwe[MWE][1]]    
                            else :
                                   if NF not in dicoMWE[comptPhrase] :
                                          dicoMWE[comptPhrase][NF] = [col6_positionEltsMwe[MWE][1]]
                                   else :
                                          dicoMWE[comptPhrase][NF] += [col6_positionEltsMwe[MWE][1]]                                    
                            
                            #=======================================================================   
                            # FR : Homogénéisation IRV avec pronoms lemmatisés différemment de SE
                            #=======================================================================                              
                            NFs = [NF]                              
                            if langue == "FR" :
                                   NFs = correctionIRV_FR(col4,lemmesEltLex)  
                            elif langue == "IT" :
                                   NFs = correctionIRV_IT(col4,lemmesEltLex)    
                                   
                            #=================================================================   
                            for NF in NFs : 
                                   """ Fréquence des POSnorm dans TRAIN """
                                   freqPOSnorminTrain,POSnorm2POSseq = calcul_FreqPOSnorm_Train(POSnorm,POSseq,POSnorm2POSseq,freqPOSnorminTrain)
                                   """# Synthese par NF (= LemmNorm)  des :
                                   - Séquences de POS observées  
                                   - flexions observées 
                                   - POSnorm 
                                   - catégorie d'EP
                                   """
                                   NF2POSseq = synthese_Infos_perNF(NF,POSseq,NF2POSseq)      
                                   NF2POSnorm = synthese_Infos_perNF(NF,POSnorm,NF2POSnorm)  
                                   NF2flexion = synthese_Infos_perNF(NF,col7,NF2flexion)
                                   NF2categ = synthese_Infos_perNF(NF,col4,NF2categ)
                                   
                                   if NF not in dicoFreqNF_inTrain :
                                          dicoFreqNF_inTrain[NF] = 1
                                   else :
                                          dicoFreqNF_inTrain[NF] += 1
                                   
                                   #------------------------------------------
                                   # Nb d'insertions observées pour chaque NF
                                   n = nbInsertperMWE[MWE] 
                                   if NF not in nbInsertperNF:
                                          nbInsertperNF[NF] = []                                   
                                          nbInsertperNF[NF] = [n]
                                   else :
                                          if n not in nbInsertperNF[NF] : 
                                                 nbInsertperNF[NF] += [n]                                   
                                          
                                   #===============================================================================   
                                   # Associations lemme=POS pour chaque NF (= LemmNorm)  
                                   # NB : différentes POS pour un même lemme sont parfois observées e.g. (PART/PRON)
                                   # e.g. 'disputar;luta': {'disputar=VERB|luta=NOUN': 1}  
                                   #===============================================================================  
                                   if NF not in NF2POSandLemme :                                           
                                          NF2POSandLemme[NF] = {}
                                          NF2POSandLemme[NF][POSnorm] = tuple2string(list(zip(lemmesEltLex,POSseq.split("|"))))
                                   else :
                                          if POSnorm not in NF2POSandLemme[NF] :
                                                 NF2POSandLemme[NF][POSnorm] = tuple2string(list(zip(lemmesEltLex,POSseq.split("|")))) 
                                                        
                     
                     comptPhrase += 1
                     
              
              NF2categUnique = choix_CatUnique_perNF(NF2categ)   #Une seule catégorie par NF                     
              NF2POSnormUnique,freqPOSnormconservee = choix_POSnormUnique_perNF(NF2POSnorm, freqPOSnorminTrain, NF2POSandLemme) #Une seule POSnorm par NF
              
              #  Obtention des X plus fréquentes POSnorm                  
              if topX > 0 : 
                     topX_POSnorm,couvertureTopX_POSnorm = getlist_couverture_TopValueDico(freqPOSnormconservee,topX)       
       return dicoMWE,NF2POSseq, POSnorm2POSseq, NF2categUnique, NF2flexion, NF2POSnormUnique, topX_POSnorm,freqInsertPerCat, dicoNumPhrase2idPhrase,dicoFreqNF_inTrain,nbInsertperNF
       
