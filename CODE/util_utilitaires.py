#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys
import ast
import shutil
import pickle
import itertools


def savepickle(dico,outfile):
    with open(outfile, 'wb') as f :
        pickle.dump(dico, f,protocol=pickle.HIGHEST_PROTOCOL)    

def loadpickle(infile):
    with open(infile, 'rb') as f :
        dico = pickle.load(f)   
    return dico

def removeKey(dico,key) :
    if str(key) in dico:
        del dico[str(key)]

def addHeader(nbCol):
    headers =[]
    for i in range(0,nbCol):
        headers += ["col" + str(i)]
    return headers        

def cupt2phrases(cupt):
    dicoPhrasesSansEntete = {}
    dicoPhrasesAvecEntete = {}
    if os.path.exists(cupt) :
        with open (cupt, "r") as f_cupt : 	
            infosCupt =  f_cupt.readlines()
            corpusCupt = "".join(infosCupt[1:])					
            phrases = corpusCupt.split("\n\n")[:-1]
            textPhrases = []
            indicesDoublons = []
            compteurPhrase = 1
            for phrase in phrases :
                idPhrase = ""
                lignes = phrase.split("\n")                        
                for ligne in lignes :
                    if "# source_sent_id" in ligne :
                        sourcePhrase = ligne.split("# source_sent_id = ")[1]
                        idPhrase = compteurPhrase   
                    elif "# text = " in ligne :
                        textPhrase =  ligne.split("# text = ")[1] #"A little birdie told me..."  
                if idPhrase != "" :
                    dicoPhrasesSansEntete[idPhrase] = lignes[2:]
                else : 
                    idPhrase = compteurPhrase
                    dicoPhrasesSansEntete[idPhrase] = lignes[1:]

                dicoPhrasesAvecEntete[compteurPhrase] = (idPhrase,lignes)
                compteurPhrase += 1
    return dicoPhrasesSansEntete,dicoPhrasesAvecEntete

def getList_KeysWithMaxValue(Dico) :
    return [k for k,v in Dico.items() if v == max(Dico.values())]
def infosTableur(tableur):
    with open (tableur,"r") as f_tab :
        lignes = f_tab.readlines()                
    return lignes
def getList(dict): 
    return list(dict.keys())

def initDico(listePOS):
    statsInsertions = {}
    for POS in listePOS :
        statsInsertions[POS] = 0
    return statsInsertions

def updateDir(cheminDir):
    # si le répertoire existe : efface le répertoire et son contenu 
    # sinon : crée le répertoire
    if os.path.exists(cheminDir):
        shutil.rmtree(cheminDir)
        os.makedirs(cheminDir) 
    else :
        os.makedirs(cheminDir)

def getlist_couverture_TopValueDico(dico,topX):
    """
    input = dictionnaire clé-valeur (e.g. clé = POSnorm, valeur = fréquence en corpus)
    output = liste des topX clés par valeur décroissante
    """
    dicoTrie = {} 
    listeTrie = []
    nbTotal = 0
    nbSelection = 0
    for k in dico:
        nbTotal += dico[k]
    dicoTrie =  {key: value for key, value in dico.items() if value in sorted(set(dico.values()), reverse=True)[:topX]}
    for k in dicoTrie :
        listeTrie.append(k)
        nbSelection += dicoTrie[k] 
    couverture = nbSelection / nbTotal 
    return listeTrie, couverture

def tupleOrdreCroissant (liste):
    # input = [(['36', '37'], 'IDIOMAT'), (['2', '19'], 'LITERAL'), (['1', '19'], 'LITERAL'), 
    # output = [(['1', '19'], 'LITERAL'), (['2', '19'], 'LITERAL') (['36', '37'], 'IDIOMAT'), , ,  
    newListe = []
    allelt0Tuple = []  
    for elt in liste :
        elt0Tuple = list(map(int, elt[0]))
        allelt0Tuple.append(elt0Tuple)
    allelt0Tuple = sorted(allelt0Tuple)
    for p in allelt0Tuple :
        for elt in liste :
            if list(map(int, elt[0])) == p :
                newListe.append((elt[0],elt[1],elt[2]))
    return newListe

def isTupleCroissant(tupleString):    
    tupleInt = list(map(int, tupleString))
    tupleOrdon = sorted(tupleInt)
    if tupleOrdon == tupleInt :
        croissant = True
    else :
        croissant = False
    return croissant

def listeInt2Str(l1):
    l2 = []
    for e in l1 :
        l2.append(str(e))
    return l2

def efface_fichier(fichier):
    with open (fichier,"a") as f_out :
        f_out.write("")

def prod_cartesien(l1,l2) :
    l = [[l1],l2]
    result =  list(itertools.product(*l))    
    return result

def produit_cartesien(dico):
    """
    INPUT : dictionnaire de lemmes avec leur position dans la phrase 
             e.g. {'de': [11], 'le': [1, 8, 19], 'partie': [14], 'être': [10]}
    OUTPUT : dictionnaire de produit cartésien ie toutes les combinaisons possibles de lemmes
             e.g. {7605: {'de;le;partie;être': [(11, 1, 14, 10), (11, 8, 14, 10), (11, 19, 14, 10)]}}
    """

    listeOUT = []
    for lemme in dico :
        listeOUT += [dico[lemme]]  
    result = list(itertools.product(*listeOUT))
    return result


def listeStr2int(l):
    l_out = []
    for elt in l :
        l_out.append(int(elt))
    return l_out


def tri_liste_ordre_croissant(l) :
    l_int = []
    for elt in l :
        l_int.append(int(elt))
    return sorted(l_int)

def second_smallest(numbers): 
    m1, m2 = float('inf'), float('inf')
    for x in numbers:
        if x <= m1:
            m1, m2 = x, m1
        elif x < m2:
            m2 = x
    return m2

def sort_dico_by_value(sorting_choice,dico) :
    """
    sorting_choice = par valeurs décroissantes ou croissantes
    """
    if sorting_choice == "decroissant" :
        return {k: v for k, v in sorted(dico.items(), key=lambda item: item[1], reverse=True)}
    else :
        return {k: v for k, v in sorted(dico.items(), key=lambda item: item[1])}

def suppr_eltcontigus_identiques(l):
    element_precedent = ""
    l_out = []
    for elt in l :
        if elt != element_precedent :
            l_out.append(elt)
        element_precedent = elt
    return l_out


def liste2TSV(l,fichier):
    with open(fichier,"a") as f_out :
        for elt in l :
            f_out.write(str(elt) + "\n")

def dico2TSV_k1(dico,fichier):
    with open(fichier,"a") as f_out :
        for k in dico :
            f_out.write(str(k) + "\t" + str(dico[k]) + "\n")


def liste_tuples_sorted_list(l):
    """
    input :  [(13, 4), (13, 12)]
    output : ['4|13', '12|13']
    """
    l_int = []
    l_out = []
    # tuples triés par ordre croissant de valeurs
    for t in l :
        l_int.append(sorted(t))
    for combi in l_int :
        combi_str = []
        for elt in combi:
            combi_str.append(str(elt))
        l_out.append("|".join(combi_str))        
    return l_out


def liste_string2int(l):
    l_out = []
    for elt in l:
        l_out.append(int(elt))
    return l_out

def listeStr_2int(l_str):
    l_out = []
    for elt in l_str:
        l_out.append(int(elt))
    return l_out

def liste2str(l) :
    return ";".join(l)

def ordonne_iterable(tokens_composants):
    liste = []
    l = []
    for elt in tokens_composants :
        liste.append(int(elt))       
    return sorted(liste)

def tuple2str(tup):
    """
    Input = tuple de nombres e.g. (4,3)
    Output = string avec nombres par ordre croissant : "3|4"
    """
    listeOut = []
    if len(tup) > 1 :
        for tok in tup :
            listeOut.append(int(tok))
    else :
        listeOut.append(int(tup[0]))
    return "|".join(listeInt2Str(sorted(listeOut)))

def updateDico2keys_liste(dico, k1,k2):
    if k1 not in dico :
        dico[k1] = {}
    if k2 not in dico[k1] :
        dico[k1][k2] = []
    return dico

def renvoie_liste_plus_grande(listes) :
    l1 = listes[0]
    l2 = listes[1]
    if len(l1) > len(l2) :
        return "|".join(listeInt2Str(l1)), "|".join(listeInt2Str(l2))
    else :
        return "|".join(listeInt2Str(l2)), "|".join(listeInt2Str(l1))



def recouvrement_listes(listes) :
    if len(listes) > 1 :
        for l1 in listes :
            for l2 in listes :
                if l2 != l1 :
                    intersection = list(set(l1) & set(l2))
                    if l1 == intersection and l1 in listes:
                        listes.remove(l1)
                    elif l2 == intersection and l2 in listes :
                        listes.remove(l2)
    return listes

def cherche_listes_imbriquees(listes):
    """
    input = liste avec tous candidats e.g. [[1,2,3,5],[1,2,3,5,8,9],[1,2,3,4],[10,11],[1,5]]
    output : cle = liste imbirquante, valeurs = listes imbriquées eg {'1|2|3|5|8|9': [[1, 2, 3, 5], [1, 5]], '1|2|3|5': [[1, 5]]}
    """
    liste_imbriquante = {}    
    """
    candsInPhrase = [['1', '2', '3'], ['2', '3']]
    NFsinPhrase = ['agir;il;se', 'agir;se']
    cands_imbriquants2imbriques = {}
    """
    if len(listes) > 1 :
        for l1 in listes :
            for l2 in listes :
                if l2 != l1 :
                    intersection =  listeInt2Str(sorted(listeStr2int(list(set(l1) & set(l2)))))
                    if l1 == intersection or l2 == intersection :
                       if len(l1) > len(l2) :
                           # ie l1 imbriquante
                           l1_string = "|".join(listeInt2Str(l1)) 
                           #print(l1_string)
                           if l1_string not in liste_imbriquante :
                               liste_imbriquante[l1_string] = [l2]
                           else :
                               if l2 not in liste_imbriquante[l1_string] :
                                   liste_imbriquante[l1_string] += [l2]  
                       else :
                           l2_string = "|".join(listeInt2Str(l2))
                           #print(l2_string)
                           if l2_string not in liste_imbriquante :
                               liste_imbriquante[l2_string] = [l1]
                           else :
                               if l1 not in liste_imbriquante[l2_string] :
                                    liste_imbriquante[l2_string] += [l1]
    return liste_imbriquante



def updateDico2keys_liste(dico, k1,k2):
  if k1 not in dico :
    dico[k1] = {}
  if k2 not in dico[k1] :
    dico[k1][k2] = []
  return dico
 
def produit_cartesien(dico):
  """
  INPUT : dictionnaire de lemmes avec leur position dans la phrase 
           e.g. {'de': [11], 'le': [1, 8, 19], 'partie': [14], 'être': [10]}
  OUTPUT2 : dictionnaire de produit cartésien ie toutes les combinaisons possibles de lemmes
           e.g. {7605: {'de;le;partie;être': [(11, 1, 14, 10), (11, 8, 14, 10), (11, 19, 14, 10)]}}
  """
 
  listeOUT = []
  for lemme in dico :
    listeOUT += [dico[lemme]]  
  result = list(itertools.product(*listeOUT))
  return result

def tri_liste_ordre_croissant(l) :
  l_int = []
  for elt in l :
    l_int.append(int(elt))
  return sorted(l_int)

def listeStr2int(l):
  l_out = []
  for elt in l :
    l_out.append(int(elt))
  return l_out


def ordonne_iterable(tokens_composants):
    liste = []
    l = []
    for elt in tokens_composants :
        liste.append(int(elt))       
    return sorted(liste)

def listeStr_2int(l_str):
    l_out = []
    for elt in l_str:
        l_out.append(int(elt))
    return l_out


def tuple2str(tup):
    """
    Input = tuple de nombres e.g. (4,3)
    Output = string avec nombres par ordre croissant : "3|4"
    """
    listeOut = []
    if len(tup) > 1 :
        for tok in tup :
            listeOut.append(int(tok))
    else :
        listeOut.append(int(tup[0]))
    return "|".join(listeInt2Str(sorted(listeOut)))

def tuple2string(liste) :
       """
       input  = liste de tuples lemmes-POS : [('dar', 'VERB'), ('espetáculo', 'NOUN')]
       OUTPUT2 = liste de string lemme = POS : ['dar=VERB', 'espetáculo=NOUN')]
       """     
       listeOUt = []
       for tupl in liste :
              string = str(tupl[0]) + "=" + str(tupl[1])
              listeOUt.append(string)
       return "|".join(sorted(listeOUt))


