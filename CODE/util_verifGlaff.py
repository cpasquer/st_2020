#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sys
import ast
import shutil
import pickle
import unicodedata as ud
import itertools
from itertools import product
import codecs

"""
cherche si un mot donné appartient au glaff avec la POS choisie et donne son lemme
gère les ambiguités : table = NOM / verbe
gère ligatures
"""
#===================================================
def savepickle(dico,outfile):
    with open(outfile, 'wb') as f :
        pickle.dump(dico, f,protocol=pickle.HIGHEST_PROTOCOL)    

def loadpickle(infile):
    with open(infile, 'rb') as f :
        dico = pickle.load(f)   
    return dico
#===================================================
def forme2lemme(langue, forme_cherchee): 
    tous_mots_dans_glaff = False
    lemmes_pos_possibles = []
    existe_dico_glaff = False
    if os.path.exists (str(repParent) + "/CODE/dico_glaff.pickle") :
        dico_glaff = loadpickle(str(repParent) + "/CODE/" + str(langue) + "_dico_glaff_trad_pickle")
        existe_dico_glaff = True    
    chemin_glaff = str(repParent) + "/CODE/glaff-1.2.2.txt"
    with codecs.open (chemin_glaff,"r","utf8") as f_glaff:
        lignes_glaff = f_glaff.readlines()
        if existe_dico_glaff : 
            if forme_cherchee in dico_glaff :
                lemmes_pos_possibles = dico_glaff[forme_cherchee]
                tous_mots_dans_glaff = True
        if not existe_dico_glaff  :
            dico_glaff = {}   
        if not tous_mots_dans_glaff :
            for l in lignes_glaff :   #Açoriennes|Ncfp|Açorienne|a
                forme_glaff = l.split("|")[0]
                if forme_glaff == forme_cherchee or ud.normalize('NFC', forme_glaff) == ud.normalize('NFC', forme_cherchee):
                    tous_mots_dans_glaff = True
                    pos = l.split("|")[1][0]
                    pos_flexion = l.split("|")[1]
                    lemme = l.split("|")[2]     
                    if (pos,lemme) not in lemmes_pos_possibles :
                        lemmes_pos_possibles.append((pos,lemme,pos_flexion))
            dico_glaff[forme_cherchee] = lemmes_pos_possibles
            savepickle(dico_glaff,str(repParent) + "/CODE/" + str(langue) + "_dico_glaff_trad_pickle")
    return [lemmes_pos_possibles, tous_mots_dans_glaff]

def ep2motscherches(langue, aux, ep_cherchees):
    """
    ep_lemmatisee = [[('A', 'fait'), ('V', 'faire')], [('N', 'place'), ('V', 'placer')]]
    # output =    [(('A', 'fait'), ('N', 'place')), (('A', 'fait'), ('V', 'placer')), (('V', 'faire'), ('N', 'place')), (('V', 'faire'), ('V', 'placer'))]

    """
    ep_lemmatisees = []   
    ep_lemmatisee = []
    mots = ep_cherchees.split("|")
    commence_par_aux = False
    termine_par_verbe = False # eg. être tordu / avoir engagé
    supprime_cand = False # suppression passé composé
    termine_forme_passive = False
    commence_par_nom = False
    termine_par_verbe = False
    termine_par_nom = False
    termine_par_adj = False
    commence_par_verbe  =False
    ordre_nom_verbe_autorise = True
    compt_mot = 1    
    for m in mots :
        pos_lemme = forme2lemme(langue,m)
        #====================================================================================
        #             Pour EP à 2 éléments :
        #  nettoyage : "avoir généré", "être impliqué"
        # choix lemmes : "recherche conduite" => conduire recherche et pas rechercher conduite
        #=======================================================================================
        if len(mots) == 2 :
            for lemme_possible in pos_lemme[0]:  #[('N', 'être', 'Ncms'), ('V', 'être', 'Vmn----')]
                #------------------ 1er mot -----------------------
                if lemme_possible[1] in aux and compt_mot == 1:
                    commence_par_aux = True
                if lemme_possible[0] == "N" and compt_mot == 1:
                    commence_par_nom = True
                elif lemme_possible[0] == "V" and compt_mot == 1:
                    commence_par_verbe = True
                #------------------ 2e mot -----------------------
                if lemme_possible[2][0] == "V" and compt_mot == 2:
                    termine_par_verbe = True
                    if "Vmps-" in str(lemme_possible[2]) :
                        termine_forme_passive = True
                if lemme_possible[2][0] == "A" and compt_mot == 2:
                    termine_par_adj = True
                if lemme_possible[2][0] == "N" and compt_mot == 2:
                    termine_par_nom = True  
                
        ep_lemmatisee += [pos_lemme[0]]
        tous_mots_dans_glaff = pos_lemme[1]
        compt_mot += 1    
    if len(mots) == 2 and commence_par_aux and termine_par_verbe and termine_forme_passive:
        #============================================================================================
        #     cas 1 : être mis / avoir mis   => SUPPR
        #============================================================================================
        supprime_cand = True        
    elif len(mots) == 2 and  commence_par_nom and termine_forme_passive :
        #============================================================================================
        #     cas 2 : analyse faite  => faire analyse 
        #============================================================================================
        new_ep_lemmatisee = []
        for mot in range(0,len(ep_lemmatisee)) :
            new_ep_lemmatisee_mot = []
            for pos_lemme_possible in ep_lemmatisee[mot] :
                if mot == 0 and pos_lemme_possible[0] == "N" :
                    new_ep_lemmatisee_mot.append(pos_lemme_possible)
                elif mot == 1 and pos_lemme_possible[0] == "V" :
                    new_ep_lemmatisee_mot.append(pos_lemme_possible)
            new_ep_lemmatisee.append(new_ep_lemmatisee_mot)
        # inversion nom-verbe
        new_ep_lemmatisee_temp = []
        new_ep_lemmatisee_temp = [new_ep_lemmatisee[1],new_ep_lemmatisee[0]]
        ep_lemmatisee = new_ep_lemmatisee_temp
    elif len(mots) == 2 and  commence_par_nom and not commence_par_verbe and termine_par_verbe and not termine_forme_passive : # e.g. "changement subissant"
        #============================================================================================
        #     cas 2 : introspection faire
        #============================================================================================
        supprime_cand = True
        ep_lemmatisees = ["forme verbale invalide"]
    elif len(mots) == 2 and commence_par_nom and commence_par_verbe  and termine_par_verbe and  not termine_forme_passive :
        #============================================================================================
        #      ambiguité du 1er mot qui peut être V ou N => ordre N-V NON autorisé
        #       cas 3 : étude faisant  : ne sera pas conservé
        #               faire étude : sera conservé
        #============================================================================================
        ordre_nom_verbe_autorise = False
    if not supprime_cand : 
        if tous_mots_dans_glaff :
            ep_lemmatisees += [ep_lemmatisee]
        else :
            ep_lemmatisees = ["invalide (absent Glaff)"]
    else :
        ep_lemmatisees = ["forme verbale invalide (pp)"]
    return ep_lemmatisees, ordre_nom_verbe_autorise


def filtre_ep_lemmatisee_pos_LVCorVID(ep_lemmatisees,aux,patrons_MWE_langue):
    """
    input = [[('A', 'fait'), ('V', 'faire')], [('N', 'place'), ('V', 'placer')]]
    output = [(('A', 'fait'), ('N', 'place')), (('A', 'fait'), ('V', 'placer')), (('V', 'faire'), ('N', 'place')), (('V', 'faire'), ('V', 'placer'))]
    """

    for ep in ep_lemmatisees :
        lemmes_possibles_posLVCorVID = []
        lemme_ep_possibles = list(itertools.product(*ep))
        for combi in lemme_ep_possibles :
            combi_pos  = []
            combi_pos_flex = []
            for m in combi :
                combi_pos += [m[0][0]]
                combi_pos_flex += [m[2]]
            combi_pos = "|".join(combi_pos)
            if combi_pos in patrons_MWE_langue :
                lemme = []
                for m in combi :
                    lemme += [m[1]]
                lemme = "|".join(lemme)
                if(lemme,combi_pos) not in lemmes_possibles_posLVCorVID :
                    lemmes_possibles_posLVCorVID += [(lemme,combi_pos)]
        if lemmes_possibles_posLVCorVID == []:
            """
            on fait une seconde passe pour supprimer les cas où on a un verbe en plus e.g." avoir pris une décision"
            """
            premier_elt = ep[0]
            second_elt = ep[1]
            nb_mots_cand = len(ep)
            auxiliaire_a_supprimer = False
            cand_a_supprimer = False
            for lemme_possible_mot1 in premier_elt :
                if lemme_possible_mot1[1] in aux and lemme_possible_mot1[0] == "V" :
                    if nb_mots_cand > 2 :
                        auxiliaire_a_supprimer = True     
            if auxiliaire_a_supprimer :     
                return "faire seconde passe en supprimant auxiliaire"
            else :
                return "invalide seq pos"
        else :            
            return str(lemmes_possibles_posLVCorVID)


#========================================
repParent = "/".join(os.path.abspath(os.path.dirname(sys.argv[0])).split("/")[:-1])


