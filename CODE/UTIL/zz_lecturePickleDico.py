#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pickle
import sys

"""
Ecrit le contenu du dico picke dans fichier TSV : sur chaque ligne => clé \t valeur
"""
with open(str(sys.argv[1]), "rb") as f_in:
    d = {}
    d = pickle.load(f_in) 
    
    nameFilePickle = sys.argv[1]
    f_out = "contenuExtrait_" + str(nameFilePickle) + ".tsv"
    with open(f_out,"a") as f_out :
        for k in d :
            f_out.write(str(k) + "\t" + str(d[k]) + "\n")
    print(d)

